@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row">
        <div id="content" class="col-sm-12 full">
        <div class="row">
            <div class="login-box">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                    {{ csrf_field() }}
                    <fieldset class="col-sm-11">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="controls row">
                                <div class="input-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="email">ID</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"/>
                                         @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="controls row">
                                <div class="input-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="password">PASS</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password"/>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls row">
                                <label class="col-sm-3 control-label" for="remember"></label>
                                <div class="col-sm-8 resetpassword">
                                    <a href="{{ url('/admin/password/reset') }}">パスワードを忘れた方はコチラ</a>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="controls row">
                                <label class="col-sm-3 control-label" for="remember"></label>
                                <div class="col-sm-8">
                                    <input type="checkbox" name="remember"/>
                                    <label for="remember">私を覚えてますか</label>
                                </div>
                            </div>
                        </div>   -->

                        <div class="row">



                        </div>

                        <div class="form-group buttonbox">
                            <div class="controls row">
                                <label class="col-sm-3 control-label" for="remember"></label>
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-lg btn-primary col-xs-10">ログイン</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls row">
                                <label class="col-sm-3 control-label" for="remember"></label>
                                <div class="col-sm-8">
                                    <a href="{{ url('/admin/register') }}" class="btn btn-lg btn-primary-2 col-xs-10">新規登録</a>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>

                <div class="clearfix"></div>

            </div>
        </div><!--/row-->

    </div>

    </div><!--/row-->

</div><!--/container-->


<!-- start: JavaScript-->
<!--[if !IE]>-->


<!--<![endif]-->

<!--[if IE]>

    <script src="assets/js/jquery-1.11.0.min.js"></script>

<![endif]-->

<!--[if !IE]>-->

    <script type="text/javascript">

    </script>

<!--<![endif]-->

<!--[if IE]>

    <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

<![endif]-->
<script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


<!-- page scripts -->
<script src="{{ URL::to('assets/js/jquery.icheck.min.js') }}"></script>

<!-- theme scripts -->
<script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
<script src="{{ URL::to('assets/js/core.min.js') }}"></script>

<!-- inline scripts related to this page -->
<script src="{{ URL::to('assets/js/pages/login.js') }}"></script>

<!-- end: JavaScript-->
@endsection
