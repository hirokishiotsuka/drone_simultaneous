@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row">
        <div id="content" class="col-sm-12 full">
            
            <div class="row box-error">
                    
                <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
                
                    <h1>404</h1>
                    <h2>Oops! You're lost.</h2>
                    <p>The page you are looking for was not found.</p>
                
                </div><!--/col-->
                    
            </div><!--/row-->
            
        </div><!--/content-->   
            
    </div><!--/row-->       
        
</div><!--/container-->
@endsection