<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drone extends Model
{
    protected $table = 'drones';

    protected $fillable = ['name','manufacture','serial','insurance','renovation','post_on_homepage','user_id','status'];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
