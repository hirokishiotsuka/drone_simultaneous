<?php
namespace App\Http\Controllers;

use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\FlightLogRepository;
use App\Repositories\FlightRepository;
use App\Repositories\UserRepository;
use App\Flight;
use App\Point;
use App\HomePosition;
use App\FlightLog;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

class FlightLogController extends Controller {
    public function __construct(FlightLogRepository $flightlogs, UserRepository $users, FlightRepository $flights) {
        $this->flightlogs = $flightlogs;
        $this->flights = $flights;
        $this->users = $users;
        $this->currentUser = auth()->user();
    }

    public function index(){
        $flights = $this->flights->findBy('user_id',$this->currentUser->id)->all()->sortByDesc('updated_at');
        $flights = $this->addUser($flights);
        $flights = $this->addLastFlight($flights);
        return view('logs.lists', ['flights' => $flights]);
    }

    public function datelist($id){
        $user_id = $this->currentUser->id;
        $flight = $this->flights->find($id);
        if ($flight !== null && $user_id == $flight->user_id) {
            $logs = FlightLog::addSelect(\DB::raw('id, flight_id,fstarted_at, SUM(flight_time) AS total_flight_time, COUNT(id) AS total_sendlog'))->where('flight_id', $id)->groupBy('fstarted_at')->orderBy('fstarted_at', 'DESC')->get();
            $logs = $this->addStatus($logs);
            $logs->flightName = $flight->name;
            return view('logs.datelist', ['logs' => $logs]);
        }
    }

    public function addStatus($collection){
        return $collection->map(function($value) {
            $getLogs = FlightLog::addSelect('plan_status')->where('flight_id', $value->flight_id)->where('fstarted_at', $value->fstarted_at)->get();
            if(count($getLogs) == 0){
                $value->status = '-';
                return $value;
            } else {
                foreach ($getLogs as $keyLogs => $Log) {
                    if($Log->plan_status != 0){
                        $allLogIsZero = false;
                        break;
                    }
                    $allLogIsZero = true;
                }
                if($allLogIsZero){
                    $value->status = '異常'; //Fail
                    return $value;
                }else{
                    if($getLogs->last()->plan_status == 0){
                        $value->status = '正常'; //Success
                        return $value;
                    }else{
                        $value->status = '異常'; //Fail
                        return $value;
                    }
                }
            }
        });
    }

    public function delete($log_id){
        $user_id = $this->currentUser->id;
        $log = $this->flightlogs->find($id);
        if ($log !== null && $user_id == $log->user_id) {
            $this->flightlogs->delete($id);
            $logs = $this->flightlogs->findAllWith('user_id',$this->currentUser->id, ['flight']);
            return view('logs.lists', ['logs' => $logs])->with('message', 'Delete log succeed!');
        }
        return view('404');
    }

    public function deleteMore(Request $request) {
        $user_id = $this->currentUser->id;
        try {
            $data = $request->all();
            $rules = [
                'id.*' => 'required|numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                foreach ($data['id'] as $key => $id) {
                    $logs = $this->flightlogs->find($id);
                    if ($logs !== null && $user_id == $logs->user_id) {
                        $this->flightlogs->delete($id);
                    } else
                        return response()->error('You don\'t have permission', 404);
                }
                return response()->success($data['id']);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function addUser($collection) {
        return $collection->map(function($value) {
                    $user = $this->users->find($value->user_id);
                    if ($user != null)
                        $value->user_name = $user->name;
                    return $value;
                });
    }

    public function addLastFlight($collection) {
        return $collection->map(function($value){
            $value->lastFlight = FlightLog::where('flight_id', $value->id)->groupBy('fstarted_at')->get()->last();
            return $value;
        });
    }

    public function replay($id){
        $log = $this->flightlogs->find($id);
        if($log !== null) {
            $flight = $this->flights->find($log->flight_id);
            if ($flight !== null) {
                $points = $flight->points;
                $homeposition = Point::getHomePosition($flight->id, $flight->user_id);
                $geofilters = $flight->geofilters;
                $geofilters = $this->convertGeoFilterArray($geofilters);
                return view('flights.replays.detail', ['flight' => $flight, 'nodes' => $points, 'homeposition' => $homeposition, 'idlogreplay' => $id, 'geofilters' => $geofilters]);
            }
             return view('404');
        }
        return view('404');
    }

    public function convertGeoFilterArray($collection){
        return $collection->map(function($value){
            $pointsArray = unserialize($value->array_points);
            if(count($pointsArray) > 0) $value->array_points_convert = implode(";",$pointsArray);
            // unset($value->array_points);
            return $value;
        });
    }
}
