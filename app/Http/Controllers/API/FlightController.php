<?php

namespace App\Http\Controllers\API;

use JWTAuth;
use App\Http\Middleware\authJWT;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\FlightRepository;
use App\Repositories\FlightMediaRepository;
use App\Flight;
use App\Point;
use App\HomePosition;
use App\FlightLog;
use App\FlightMedia;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class FlightController extends Controller
{
    public function __construct(FlightRepository $flights, FlightLog $logs, FlightMediaRepository $flightmedias, Request $request)
	{
		parent::__construct();
		$this->flights = $flights;
		$this->logs = $logs;
		$this->medias = $flightmedias;
        $this->currentUserAPI = $request->get('currentUser');
	}

	public function edit(Request $request)
	{
		try{
			$data	= $request->all();
			$rules 	= [
				'id'			=> 'required|numeric',
	            'name'      	=> 'string|max:255',
	            'status'		=> 'numeric',
	            'plan_status'	=> 'numeric',
			];
			$validator = Validator::make($data, $rules);
			if ($validator->passes()) {
				$flight = $this->flights->find($data['id']);
				if($flight !== null && $this->currentUserAPI->id == $flight->user_id){ // Set temp id
					$flight = $this->flights->update($data, $data['id']);
					return response()->success(compact('data'));
				}
				return response()->error($flight, 404);
			}
			return response()->error($validator->errors()->all(), 300);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function getLogs($id)
	{
		try {
			$flight = $this->flights->find($id, null, [
				'logs',
			]);
			// $currentUser = auth()->user();
			// if ($flight !== null && $this->currentUserAPI->id == $flight->user_id) { // Set temp id
			if ($flight !== null) { // Set temp id
				$flight->lastFlight = $flight->logs->groupBy('fstarted_at')->last();
				unset($flight->logs);
				return response()->success(compact('flight'));
			}
			return response()->error(null, 404);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function getLogsReplay($id)
	{
		try {
			$log = $this->logs->find($id);
			$flight = $this->flights->find($log->flight_id);
			$currentUser = auth()->user();
			if ($log !== null && $flight !== null && $this->currentUserAPI->id == $log->user_id) { // Set temp id
				$logList = FlightLog::where('flight_id', $log->flight_id)->where('fstarted_at', $log->fstarted_at)->orderBy('id', 'ASC')->get();
				$flight->lastFlight = $logList;
				return response()->success(compact('flight'));
			}
			return response()->error(null, 404);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function dateList($id)
	{
		try {
			$flight = $this->flights->find($id);
			if($flight !== null) {
				// $flight = $this->flights->find($id, null, ['logs' => function($query) {
				// 	$query->select(['id' ,'flight_id', 'fstarted_at'])->groupBy('fstarted_at');
				// }]);
				// $dateList = $flight->logs;
				$logs = $this->logs->where('flight_id', $flight->id)
					->select(['id' ,'flight_id', 'fstarted_at'])->groupBy('fstarted_at')->paginate(10);
				return response()->success(compact('logs'));
			}
			return response()->error(null, 404);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function deleteMore(Request $request)
	{
		try {
    		$data     = $request->all();
    		$rules    = [
    			'id.*'			=> 'required|numeric',
    		];
    		$validator = Validator::make($data, $rules);
    		if ($validator->passes()) {
    			foreach ($data['id'] as $key => $id) {
    				$flight = $this->flights->find($id);
					if($flight !== null && $this->currentUserAPI->id == $flight->user_id){ // Set temp ID
						$flight->allPoints()->delete();
						$flight->logs()->delete();
						$flight->medias()->delete();
						$this->flights->delete($id);
						return response()->success('Flight has been deleted');
					}
					else return response()->error('You don\'t have permission', 404);
    			}
    			return response()->success($data['id']);
    		}
    		return response()->error($validator->errors()->all(), 300);
    	} catch(\Exception $e) {
    		return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    	}
	}

	public function deleteDates(Request $request, $id)
	{
		try {
			$data = $request->all();
			$rules = [
				'id.*' => 'required|numeric',
			];
			$validator = Validator::make($data, $rules);
			if ($validator->passes()) {
				$flight = $this->flights->find($id);
				if ($flight != null) {
					foreach ($data['id'] as $key => $date) {
						$findLog = $flight->logs->where('id', (int)$date)->first();
						if($findLog->user_id == $this->currentUserAPI->id) { // Set temp ID
							$dateList = $flight->logs->where('fstarted_at', $findLog['fstarted_at'])->all();

							$idsToDelete = array_map(function($item){
							 	return $item['id'];
							}, $dateList);

							FlightLog::whereIn('id', $idsToDelete)->delete();
						}
						else return response()->error('You don\'t have permission', 404);
					}
					return response()->success('Date Lists has been deleted');
				}
				return response()->error($flight, 404);
			}
			return response()->error($validator->errors()->all(), 300);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function deleteMedias(Request $request)
	{
		try {
			$data = $request->all();
			$rules = [
				'id.*' => 'required|numeric',
			];
			$validator = Validator::make($data, $rules);
			if ($validator->passes()) {
				$idsToDelete = [];
				foreach ($data['id'] as $key => $idMedia) {
					$media = $this->medias->find($idMedia);
					if ($media != null) {
						if ($media->user_id == $this->currentUserAPI->id) // Set temp ID
							$idsToDelete[] = $idMedia;
						else
							return response()->error('You don\'t have permission', 404);
					}
				}
				FlightMedia::whereIn('id', $idsToDelete)->delete();
				return response()->success('Medias has been deleted');
			}
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

  public function goHome(Request $request)
  {
    try {
      	$data	= $request->all();
      	DB::table('flights')
        	->where('id', $data['id'])
        	->update(['go_home' => $data['go_home']]);
      	return response()->success('go_home has been updated');
    } catch (\Exception $e) {
      return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function startFlight(Request $request)
  {
    try {
      	$data	= $request->all();
      	DB::table('flights')
        	->where('id', $data['id'])
        	->update(['plan_status' => $data['plan_status']]);

      return response()->success('plan_status has been updated');
    } catch (\Exception $e) {
      return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
}
