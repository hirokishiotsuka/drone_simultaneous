<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\NodeRepository;
use App\Node;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class NodeController extends Controller
{
    public function __construct(NodeRepository $nodes)
	{
		parent::__construct();
		$this->nodes = $nodes;
	}

	public function createAPI(Request $request)
	{
		try{
			$data	= $request->all();
			$rules 	= [
				'id'				=> 'required|numeric',
				'nodes.*.lat'		=> 'numeric',
				'nodes.*.lng'		=> 'numeric',
				'nodes.*.height'	=> 'numeric',
			];
			$validator = Validator::make($data, $rules);
			if ($validator->passes()) {
				$flight = $this->nodes->find($data['id']);
				if($flight !== null){
					$this->attachNodeToFlight();
					return response()->success(compact('images'));
				}
				return response()->error($flight, 300); 
			}
			return response()->error($validator->errors()->all(), 300);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function attachNodeToFlight($model, $nodeArr)
	{
		if(empty($nodeArr)) return null;
	}
}
