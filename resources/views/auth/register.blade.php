@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row">
                <div id="content" class="col-sm-12 full">
        <div class="row">
            <div class="register-box">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/register') }}">
                        {{ csrf_field() }}
                        <fieldset class="col-sm-11">
                            <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="company_name">企業名（※）</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}"/>
                                            @if ($errors->has('company_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('company_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="company_address">住所</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="company_address" type="text" class="form-control" name="company_address" value="{{ old('company_address') }}"/>
                                            @if ($errors->has('company_address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('company_address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-with-hr">
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">名前</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"/>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">メールアドレス</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="email" type="text" class="form-control" name="email" value="{{ old('email') }}"/>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">パスワード</label>
                                        <div class="col-sm-8">
                                            <input id="password" class="form-control" id="password" type="password" class="form-control" name="password"/>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">パスワード（確認）</label>
                                        <div class="col-sm-8">
                                            <input id="passwordconf" class="form-control" id="password-confirm" type="password" class="form-control" name="password_confirmation"/>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">電話番号</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="tel" type="text" class="form-control" name="tel" value="{{ old('tel') }}"/>
                                            @if ($errors->has('tel'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('tel') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group buttonbox">
                                <a href="{{ url('/admin/login') }}" class="btn btn-default btn-lg col-xs-4 pull-left">キャンセル</a> <button type="submit" class="btn btn-primary btn-lg col-xs-7 pull-right">登録</button>
                            </div>
                            <div class="metabox">
                                <a href="#" id="terms">利用規約</a> |
                                <a href="#" id="policies">プライバシーポリシー</a>
                            </div>
                        </fieldset>
                    </form>

                <div class="clearfix"></div>

            </div>
        </div><!--/row-->

    </div>

    </div><!--/row-->

</div><!--/container-->
<div class="modal fade" id="termsModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>利用規約</h3>
            </div>
            <div class="modal-body">
                利用規約<br>
                利用規約<br>
                利用規約<br>
                利用規約
            </div>
            <div class="modal-footer">
                <a id="btnYes" class="btn btn-sm btn-primary" data-dismiss="modal" href="#">Yes</a>
                <a aria-hidden="true" data-dismiss="modal" href="#" class="btn btn-sm btn-danger">No</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="policiesModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>プライバシーポリシー</h3>
            </div>
            <div class="modal-body">
                プライバシーポリシー<br>
                プライバシーポリシー<br>
                プライバシーポリシー<br>
                プライバシーポリシー
            </div>
            <div class="modal-footer">
                <a id="btnYes" class="btn btn-sm btn-primary" data-dismiss="modal" href="#">Yes</a>
                <a aria-hidden="true" data-dismiss="modal" href="#" class="btn btn-sm btn-danger">No</a>
            </div>
        </div>
    </div>
</div>
<!-- start: JavaScript-->
<!--[if !IE]>-->


<!--<![endif]-->

<!--[if IE]>

    <script src="assets/js/jquery-1.11.0.min.js"></script>

<![endif]-->

<!--[if !IE]>-->

    <script type="text/javascript">

    </script>

<!--<![endif]-->

<!--[if IE]>

    <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

<![endif]-->
<script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


<!-- page scripts -->
<script src="{{ URL::to('assets/js/jquery.icheck.min.js') }}"></script>

<!-- theme scripts -->
<script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
<script src="{{ URL::to('assets/js/core.min.js') }}"></script>

<!-- inline scripts related to this page -->
<script src="{{ URL::to('assets/js/pages/login.js') }}"></script>

<!-- end: JavaScript-->
<script type="text/javascript">
    $('#terms').click(function() {
        $('#termsModal').modal('show');
    });
    $('#policies').click(function() {
        $('#policiesModal').modal('show');
    });
</script>
@endsection
