<p>をご利用いただきありがとうございます。</p>
<p>以下の、ユーザのパスワード変更を受け付けました。</p>
<p>Email: {{ $user->email }}</p>
<p>Password reset: <a href="{{ $link = url('admin/password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a></p>
