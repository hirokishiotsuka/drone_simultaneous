<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('assets/ico/apple-touch-icon-144-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('assets/ico/apple-touch-icon-114-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('assets/ico/apple-touch-icon-72-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::to('assets/ico/apple-touch-icon-57-precomposed.png') }}">
        <!-- <link rel="shortcut icon" href="{{ URL::to('assets/ico/favicon.png') }}"> -->
        <title>Drone</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
        <!-- Bootstrap core CSS -->
        <link href="{{ URL::to('assets/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- page css files -->
        <link href="{{ URL::to('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/jquery-ui.min.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/fullcalendar.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/morris.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/climacons-font.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/common.css') }}" rel="stylesheet">
        <link href="{{ URL::to('assets/css/bootstrap-switch.min.css') }}" rel="stylesheet">


        <!-- Custom styles for this template -->
        <!-- <link href="{{ URL::to('assets/css/style.min.css') }}" rel="stylesheet"> -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="{{ URL::to('assets/js/jquery-2.1.0.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/bootstrap-switch.js') }}"></script>
        
        <style>
            html, body {
              height: 100%;
              margin: 0;
              padding: 0;
              font-family: 'Lato';
            }
            #map {
              height: 100%;
            }
            .fa-btn {
                margin-right: 6px;
            }
            .navbar-inverse{
                background-color: rgba(34, 34, 34, 0.8);
            }
            * {
              border-radius: 0 !important;
            }
            * {
              -webkit-border-radius: 0 !important;
                 -moz-border-radius: 0 !important;
                      border-radius: 0 !important;
            }
        </style>
    </head>
    <body id="app-layout">
        <!--Content-->
        @yield('content')

        <!--[if IE]>
            <script src="{{ URL::to('assets/js/jquery-1.11.0.min.js') }}"></script>
        <![endif]-->
        <!--[if !IE]>-->
            <script type="text/javascript">

            </script>
        <!--<![endif]-->
        <!--[if IE]>
            <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
            </script>
        <![endif]-->
        
        <script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
        <!-- page scripts -->
        <script src="{{ URL::to('assets/js/jquery-ui.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.ui.touch-punch.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.sparkline.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/fullcalendar.min.js') }}"></script>
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/js/excanvas.min.js"></script><![endif]-->

        <script src="{{ URL::to('assets/js/jquery.autosize.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.placeholder.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/moment.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/daterangepicker.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.easy-pie-chart.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/raphael.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/morris.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/uncompressed/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ URL::to('assets/js/uncompressed/gdp-data.js') }}"></script>
        <script src="{{ URL::to('assets/js/gauge.min.js') }}"></script>

        <!-- theme scripts -->
        <script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/core.min.js') }}"></script>

        @yield('scripts')
    </body>
</html>
