<?php
namespace App\Repositories;

use App\Flight;

class FlightRepository extends AbstractRepository
{

    public function __construct(Flight $flights)
    {
        $this->model         = $flights;
    }

}
