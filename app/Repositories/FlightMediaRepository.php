<?php
/**
 * @author Quynh Nguyen	<quynh.nn@neo-lab.vn>
 */
namespace App\Repositories;

use App\FlightMedia;

class FlightMediaRepository extends AbstractRepository
{

    public function __construct(FlightMedia $flightmedias)
    {
        $this->model         = $flightmedias;
    }

}
