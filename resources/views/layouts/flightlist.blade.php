<!DOCTYPE html>
<html>
<head>
    <title>Drone Control Panel</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::to('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <link rel="stylesheet" href="{{ URL::to('assets/bootstrap-table/src/bootstrap-table.css') }}">
    <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
    <!-- page css files -->
    <link href="{{ URL::to('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/morris.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/climacons-font.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::to('assets/css/style.min.css') }}" rel="stylesheet">

    <script src="{{ URL::to('assets/js/jquery-2.1.0.min.js') }}"></script>
    <script src="{{ URL::to('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
    <![endif]-->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        .fixed-table-header {
            display: none !important;
        }
    </style>
</head>
<body>
    @if (!Auth::guest())
        <div class="navbar" role="navigation">
            <div class="container-fluid" style="background: #2d2d2d none repeat scroll 0 0;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar">a</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="{{ URL::to('assets/img/logo.png') }}"> Drone Admin</a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-comments-o"></i><span class="badge">4</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-menu-header">
                                <strong>Messages</strong>
                                <div class="progress thin">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                  </div>
                                </div>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                    <span class="label label-info">NEW</span>
                                </a>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                    <span class="label label-info">NEW</span>
                                </a>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                </a>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                </a>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                </a>
                            </li>
                            <li class="avatar">
                                <a href="#">
                                    <img src="{{ URL::to('assets/img/avatar.jpg') }}">
                                    <div>New message</div>
                                    <small>1 minute ago</small>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer text-center">
                                <a href="#">View all messages</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-cog"></i></a></li>
                    <li><a href="#"><i class="fa fa-power-off"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><img src="{{ URL::to('assets/img/avatar.jpg') }}"><span class="badge">9</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-menu-header text-center">
                                <strong>Account</strong>
                            </li>
                            <li><a href="#"><i class="fa fa-bell-o"></i> Updates <span class="label label-info">42</span></a></li>
                            <li><a href="#"><i class="fa fa-envelope-o"></i> Messages <span class="label label-success">42</span></a></li>
                            <li><a href="#"><i class="fa fa-tasks"></i> Tasks <span class="label label-danger">42</span></a></li>
                            <li><a href="#"><i class="fa fa-comments"></i> Comments <span class="label label-warning">42</span></a></li>
                            <li class="dropdown-menu-header text-center">
                                <strong>Settings</strong>
                            </li>
                            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="#"><i class="fa fa-wrench"></i> Settings</a></li>
                            <li><a href="#"><i class="fa fa-usd"></i> Payments <span class="label label-default">42</span></a></li>
                            <li><a href="#"><i class="fa fa-file"></i> Projects <span class="label label-primary">42</span></a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fa fa-shield"></i> Lock Profile</a></li>
                            <li><a href="#"><i class="fa fa-lock"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-bars"></i></a></li>
                </ul>
            </div>
        </div>
        @endif
        <div class="container-fluid content">
        <div class="row">
            @if (!Auth::guest())
            <!-- start: Main Menu -->
            <div class="sidebar col-md-2 col-sm-1 ">

                <div class="sidebar-collapse collapse">

                    <ul class="nav nav-sidebar">

                        <li><a href="#"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm text"> トップ </span></a></li>
                        <li><a href="#"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> アカウント設定</span></a></li>
                        <li><a href="#"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> リアルタイムフライト </span></a></li>
                        <li><a href="{{ url('/admin/flight/list') }}"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm text"> メディア </span></a></li>
                        <li><a href="#"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> ログアウト</span></a></li>

                    </ul>

                    <div class="nav-sidebar title"><span>Admin</span></div>

                    <ul class="nav nav-sidebar">

                        <li><a href="{{ url('/admin/home') }}"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm text"> Admin設定 </span></a></li>
                        <li><a href="{{ url('/admin/flight/listsss') }}"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm text"> ユーザー一覧</span></a></li>

                    </ul>

                </div>
                                    <a href="#" id="main-menu-min" class="full visible-md visible-lg"><i class="fa fa-angle-double-left"></i></a>
                            </div>
            <!-- end: Main Menu -->
            @endif
            @yield('content')
        </div>
    </div>

<script>
    var $table = $('#table'),
        $remove = $('#remove'),
        selections = [];

    function initTable() {
        $table.bootstrapTable({
            height: getHeight(),
            columns: [
                [
                    {
                        field: 'state',
                        checkbox: true,
                        rowspan: 2,
                        align: 'center',
                        valign: 'middle'
                    }, {
                        title: 'Item ID',
                        field: 'id',
                        rowspan: 2,
                        align: 'center',
                        valign: 'middle',
                        sortable: true,
                        footerFormatter: totalTextFormatter
                    }, {
                        title: 'Item Detail',
                        colspan: 3,
                        align: 'center'
                    }
                ],
                [
                    {
                        field: 'name',
                        title: 'Item Name',
                        sortable: true,
                        editable: true,
                        footerFormatter: totalNameFormatter,
                        align: 'center'
                    }, {
                        field: 'price',
                        title: 'Item Price',
                        sortable: true,
                        align: 'center',
                        editable: {
                            type: 'text',
                            title: 'Item Price',
                            validate: function (value) {
                                value = $.trim(value);
                                if (!value) {
                                    return 'This field is required';
                                }
                                if (!/^\$/.test(value)) {
                                    return 'This field needs to start width $.'
                                }
                                var data = $table.bootstrapTable('getData'),
                                    index = $(this).parents('tr').data('index');
                                console.log(data[index]);
                                return '';
                            }
                        },
                        footerFormatter: totalPriceFormatter
                    }, {
                        field: 'operate',
                        title: 'Item Operate',
                        align: 'center',
                        events: operateEvents,
                        formatter: operateFormatter
                    }
                ]
            ]
        });
        // sometimes footer render error.
        setTimeout(function () {
            $table.bootstrapTable('resetView');
        }, 200);
        $table.on('check.bs.table uncheck.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);

            // save your data, here just save the current page
            selections = getIdSelections();
            // push or splice the selections if you want to save all data selections
        });
        $table.on('expand-row.bs.table', function (e, index, row, $detail) {
            if (index % 2 == 1) {
                $detail.html('Loading from ajax request...');
                $.get('LICENSE', function (res) {
                    $detail.html(res.replace(/\n/g, '<br>'));
                });
            }
        });
        $table.on('all.bs.table', function (e, name, args) {
            console.log(name, args);
        });
        $remove.click(function () {
            var ids = getIdSelections();
            $table.bootstrapTable('remove', {
                field: 'id',
                values: ids
            });
            $remove.prop('disabled', true);
        });
        $(window).resize(function () {
            $table.bootstrapTable('resetView', {
                height: getHeight()
            });
        });
    }

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }

    function responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, selections) !== -1;
        });
        return res;
    }

    function detailFormatter(index, row) {
        var html = [];
        $.each(row, function (key, value) {
            html.push('<p><b>' + key + ':</b> ' + value + '</p>');
        });
        return html.join('');
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="like" href="javascript:void(0)" title="Like">',
            '<i class="glyphicon glyphicon-heart"></i>',
            '</a>  ',
            '<a class="remove" href="javascript:void(0)" title="Remove">',
            '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('');
    }

    window.operateEvents = {
        'click .like': function (e, value, row, index) {
            alert('You click like action, row: ' + JSON.stringify(row));
        },
        'click .remove': function (e, value, row, index) {
            $table.bootstrapTable('remove', {
                field: 'id',
                values: [row.id]
            });
        }
    };

    function totalTextFormatter(data) {
        return 'Total';
    }

    function totalNameFormatter(data) {
        return data.length;
    }

    function totalPriceFormatter(data) {
        var total = 0;
        $.each(data, function (i, row) {
            total += +(row.price.substring(1));
        });
        return '$' + total;
    }

    function getHeight() {
        return $(window).height() - $('h1').outerHeight(true);
    }

    $(function () {
        var scripts = [
                location.search.substring(1) || 'http://localhost:3000/assets/bootstrap-table/src/bootstrap-table.js',
                'http://localhost:3000/assets/bootstrap-table/src/extensions/export/bootstrap-table-export.js',
                'http://rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js',
                'http://localhost:3000/assets/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js',
                'http://rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/js/bootstrap-editable.js'
            ],
            eachSeries = function (arr, iterator, callback) {
                callback = callback || function () {};
                if (!arr.length) {
                    return callback();
                }
                var completed = 0;
                var iterate = function () {
                    iterator(arr[completed], function (err) {
                        if (err) {
                            callback(err);
                            callback = function () {};
                        }
                        else {
                            completed += 1;
                            if (completed >= arr.length) {
                                callback(null);
                            }
                            else {
                                iterate();
                            }
                        }
                    });
                };
                iterate();
            };

        eachSeries(scripts, getScript, initTable);
    });

    function getScript(url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.src = url;

        var done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState ||
                    this.readyState == 'loaded' || this.readyState == 'complete')) {
                done = true;
                if (callback)
                    callback();

                // Handle memory leak in IE
                script.onload = script.onreadystatechange = null;
            }
        };

        head.appendChild(script);

        // We handle everything using the script element injection
        return undefined;
    }
</script>
</body>
</html>
