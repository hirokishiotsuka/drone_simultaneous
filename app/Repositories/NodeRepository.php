<?php
namespace App\Repositories;

use App\Node;

class NodeRepository extends AbstractRepository
{

    public function __construct(Node $nodes)
    {
        $this->model         = $nodes;
    }

}
