@extends('layouts.app')

@section('content')
<div class="col-md-10 col-sm-11 main">
    <div class="row">     
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Edit Flight: {{ $flight->name }}</h2>
                </div>
                <div class="panel-body">
                    @include('auth.partials.errors')
                    <form action="{{ URL::to('/admin/flight/proEdit') }}" method="post">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="text" id="id" class="form-control" value="{{ $flight->id }}" disabled="disabled">
                                <input type="hidden" name="id" value="{{ $flight->id }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" id="user_id" class="form-control" value="{{ $flight->user_id }}" disabled="disabled">
                                <input type="hidden" name="user_id" value="{{ $flight->user_id }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                                <input type="text" id="name" name="name" class="form-control" placeholder="{{ $flight->name }}">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <button type="submit" class="btn btn-sm btn-success"> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
