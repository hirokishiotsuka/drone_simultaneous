@extends('layouts.app')

@section('content')
<div class="col-md-10 col-sm-11 main">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>アカウント設定</h2>
                </div>
                <div class="panel-body">
                    @include('auth.partials.errors')
                    <table class="tb-detail">
                        <tr>
                            <td class="option">名前</td>
                            <td class="value">{{ $user->name }}</td>
                            <td class="action"><a data-toggle="modal" href="#editName"><img src="{{ URL::to('/assets/img/editbtn.png') }}"></a></td>
                        </tr>
                        <tr>
                            <td class="option">メールアドレス</td>
                            <td class="value">{{ $user->email }}</td>
                            <td class="action"><a data-toggle="modal" href="#editEmail"><img src="{{ URL::to('/assets/img/editbtn.png') }}"></a></td>
                        </tr>
                        <tr>
                            <td class="option">パスワード</td>
                            <td class="value">**********</td>
                            <td class="action"><a data-toggle="modal" href="#editPassword"><img src="{{ URL::to('/assets/img/editbtn.png') }}"></a></td>
                        </tr>
                        <tr>
                            <td class="option">電話番号</td>
                            <td class="value">{{ $user->telephone }}</td>
                            <td class="action"><a data-toggle="modal" href="#editPhone"><img src="{{ URL::to('/assets/img/editbtn.png') }}"></a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css" rel="stylesheet">
<!-- page scripts -->
<!-- start: JavaScript-->
<!--[if !IE]>-->

        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> -->
        <!-- <script src="{{ URL::to('assets/js/jquery.confirm.js') }}"></script> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script> -->

<!--<![endif]-->

<!--[if IE]>

    <script src="{{ URL::to('assets/js/jquery-1.11.0.min.js') }}"></script>

<![endif]-->

<!--[if !IE]>-->

    <script type="text/javascript">

    </script>

<!--<![endif]-->

<!--[if IE]>

    <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

<![endif]-->
<div class="modal fade" id="editName" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>アカウント編集</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route'=> ['user.edit', $user->id], 'id'=>'frmName','class'=>'form-horizontal', 'files'=>true)) !!}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">名前 :</label>
                        <div class="col-md-9">
                            <input id="name" class="form-control" type="text" name="name" placeholder="{{ $user->name }}">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <a id="btnYesName" onclick="checkName(event)" href="javascript:;"><img src="{{ URL::to('assets/img/icSave.png') }}"></a>
                <a aria-hidden="true" data-dismiss="modal" href="#"><img src="{{ URL::to('assets/img/icCancel.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editEmail" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>アカウント編集</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route'=> ['user.edit', $user->id], 'id'=>'frmEmail','class'=>'form-horizontal', 'files'=>true)) !!}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">メールアドレス :</label>
                        <div class="col-md-9">
                            <input id="email" class="form-control" type="text" name="email" placeholder="{{ $user->email }}">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <a id="btnYesEmail" onclick="checkEmail(event)" href="javascript:;"><img src="{{ URL::to('assets/img/icSave.png') }}"></a>
                <a aria-hidden="true" data-dismiss="modal" href="#"><img src="{{ URL::to('assets/img/icCancel.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editPassword" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>アカウント編集</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route'=> ['user.edit', $user->id], 'id'=>'frmPassword','class'=>'form-horizontal', 'files'=>true)) !!}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="password">パスワード :</label>
                        <div class="col-md-9">
                            <input id="password" class="form-control" type="password" name="password">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <a id="btnYesPassword" onclick="checkPassword(event)" href="javascript:;"><img src="{{ URL::to('assets/img/icSave.png') }}"></a>
                <a aria-hidden="true" data-dismiss="modal" href="#"><img src="{{ URL::to('assets/img/icCancel.png') }}"></a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="editPhone" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>アカウント編集</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route'=> ['user.edit', $user->id], 'id'=>'frmPhone','class'=>'form-horizontal', 'files'=>true)) !!}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="telephone">電話番号 :</label>
                        <div class="col-md-9">
                            <input id="telephone" class="form-control" type="text" name="telephone" placeholder="{{ $user->telephone }}">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <a id="btnYesPhone" onclick="checkPhone(event)" href="javascript:;"><img src="{{ URL::to('assets/img/icSave.png') }}"></a>
                <a aria-hidden="true" data-dismiss="modal" href="#"><img src="{{ URL::to('assets/img/icCancel.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#editName').on('shown.bs.modal', function() {
        if($('#frmName .col-md-9').find('.help-block').length != 0){
            $('#frmName .help-block').remove();
            var name = $('#name');
            name.closest('.form-group').removeClass('has-error');
        }
    });
    $('#editEmail').on('shown.bs.modal', function() {
        if($('#frmEmail .col-md-9').find('.help-block').length != 0){
            $('#frmEmail .help-block').remove();
            var email = $('#email');
            email.closest('.form-group').removeClass('has-error');
        }
    });
    $('#editPassword').on('shown.bs.modal', function() {
        if($('#frmPassword .col-md-9').find('.help-block').length != 0){
            $('#frmPassword .help-block').remove();
            var password = $('#password');
            password.closest('.form-group').removeClass('has-error');
        }
    });
    $('#editPhone').on('shown.bs.modal', function() {
        if($('#frmPhone .col-md-9').find('.help-block').length != 0){
            $('#frmPhone .help-block').remove();
            var telephone = $('#telephone');
            telephone.closest('.form-group').removeClass('has-error');
        }
    });
    function checkName(e) {
        var name = $('#name');
        var nameUser = $.trim(name.val());
        if(!name.val() || nameUser.length == 0) {
            name.closest('.form-group').removeClass('has-success').addClass('has-error');
            if($('#frmName .col-md-9').find('.help-block').length == 0){
                $("#frmName .col-md-9").append( '<span class="help-block"><strong>名前を入力してください</strong></span>');
            }
            e.preventDefault();
        } else {
            var callAction = $.post( "{{ url('/admin/user/edit').'/'.Auth::user()->id }}", { name: nameUser }, function() {
                // alert( "success" );
            })
            .done(function(data) {
                if(data.status == false){
                    name.closest('.form-group').removeClass('has-success').addClass('has-error');
                    if($('#frmName .col-md-9').find('.help-block').length == 0){
                        $("#frmName .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    else {
                        $('#frmName .help-block').remove();
                        $("#frmName .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    e.preventDefault();
                }
                else location.reload();
            });
        }
    };
    function checkEmail(e) {
        var email = $('#email');
        if(!email.val()) {
            email.closest('.form-group').removeClass('has-success').addClass('has-error');
            if($('#frmEmail .col-md-9').find('.help-block').length == 0){
                $("#frmEmail .col-md-9").append( '<span class="help-block"><strong>正しいメールアドレスを入力してください。</strong></span>');
            }
            e.preventDefault();
        } else {
            var callAction = $.post( "{{ url('/admin/user/edit').'/'.Auth::user()->id }}", $( "#frmEmail" ).serialize(), function() {
                // alert( "success" );
            })
            .done(function(data) {
                if(data.status == false){
                    email.closest('.form-group').removeClass('has-success').addClass('has-error');
                    if($('#frmEmail .col-md-9').find('.help-block').length == 0){
                        $("#frmEmail .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    else {
                        $('#frmEmail .help-block').remove();
                        $("#frmEmail .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    e.preventDefault();
                }
                else location.reload();
            })
        }
    };
    function checkPassword(e) {
        var password = $('#password');
        if(!password.val()) {
            password.closest('.form-group').removeClass('has-success').addClass('has-error');
            if($('#frmPassword .col-md-9').find('.help-block').length == 0){
                $("#frmPassword .col-md-9").append( '<span class="help-block"><strong>Passwordを入力してください</strong></span>');
            }
            e.preventDefault();
        } else {
            var callAction = $.post( "{{ url('/admin/user/edit').'/'.Auth::user()->id }}", $( "#frmPassword" ).serialize(), function() {
                // alert( "success" );
            })
            .done(function(data) {
                if(data.status == false){
                    password.closest('.form-group').removeClass('has-success').addClass('has-error');
                    if($('#frmPassword .col-md-9').find('.help-block').length == 0){
                        $("#frmPassword .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    else {
                        $('#frmPassword .help-block').remove();
                        $("#frmPassword .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    e.preventDefault();
                }
                else location.reload();
            })
        }
    };
    function checkPhone(e) {
        var telephone = $('#telephone');
        if(!telephone.val()) {
            telephone.closest('.form-group').removeClass('has-success').addClass('has-error');
            if($('#frmPhone .col-md-9').find('.help-block').length == 0){
                $("#frmPhone .col-md-9").append( '<span class="help-block"><strong>電話番号を入力してください</strong></span>');
            }
            e.preventDefault();
        } else {
            var callAction = $.post( "{{ url('/admin/user/edit').'/'.Auth::user()->id }}", $( "#frmPhone" ).serialize(), function() {
                // alert( "success" );
            })
            .done(function(data) {
                if(data.status == false){
                    telephone.closest('.form-group').removeClass('has-success').addClass('has-error');
                    if($('#frmPhone .col-md-9').find('.help-block').length == 0){
                        $("#frmPhone .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    else {
                        $('#frmPhone .help-block').remove();
                        $("#frmPhone .col-md-9").append( '<span class="help-block"><strong>'+data.error.message+'</strong></span>');
                    }
                    e.preventDefault();
                }
                else location.reload();
            })
        }
    };
</script>
@endsection
