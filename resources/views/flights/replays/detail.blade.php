@extends('layouts.node')
@section('content')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
    $(function () {
    $('#chartContainer').highcharts({
        chart: {
            backgroundColor:'rgba(44, 44, 44, 0.6)',
            marginRight: 40,
            marginTop: 40,
            width: null
        },
        title: {
            text: ''
        },
        xAxis: {
            labels: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                style: {
                    color: '#fff'
                },
                text: 'Height'
            },
            labels: {
                style: {
                    color: '#fff'
                },
                formatter: function () {
                    return this.value + 'm';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        series: [{
            name: 'Height',
            data: [
                @if(!empty($homeposition))
                    {
                        y: {{$homeposition->height}},
                        marker: {symbol: 'url(/assets/img/icon00.png)'}
                    },
                @endif
                @if(!empty($nodes) && count($nodes) > 0)
                    @foreach($nodes as $key => $node)
                        {
                            // x: {{$key}}, 
                            y: {{$node->height}},
                            marker: {symbol: 'url(/assets/img/icon{{$key+1}}.png)'}
                        },
                    @endforeach
                @endif
            ]
        },{
            name: 'Drone',
            data: [
                @if(!empty($homeposition))
                    {
                        key: {{$homeposition->id}},
                        y: {{$homeposition->height}},
                        marker: {symbol: 'url(/assets/img/blank.png)'}
                    },
                @endif
                @if(!empty($nodes) && count($nodes) > 0)
                    @foreach($nodes as $key => $node)
                        {
                            key: {{$node->id}},
                            y: {{$node->height}},
                            marker: {symbol: 'url(/assets/img/blank.png)'}
                        },
                    @endforeach
                @endif
            ]
        }]
    });
});
</script>
<style type="text/css">
    #iw-container  .iw-title {
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 22px;
        font-weight: 400;
        padding: 10px;
        background-color: #48b5e9;
        color: white;
        margin: 1px;
        border-radius: 2px 2px 0 0; /* In accordance with the rounding of the default infowindow corners. */
    }
    .gm-style-iw {
        min-width: 210px !important;
        top: 20px !important;
        left: 0 !important;
        background-color: #fff;
        background-image: url("{{ URL::to('assets/img/popDrone.png') }}");
        background-repeat: no-repeat;
        background-position: 130px -50px;
        background-size: 80% auto;
        box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
        border: 1px solid #bbb;
        border-radius: 5px !important;
        text-align: center;
        min-height: 60px;
    }
    #frmCurrentNode {
        min-width: 260px !important;
    }
    #frmEditNode label.col-md-3{
        text-align: left;
        padding-left: 5px;
        font-weight: 300;
        padding-right: 10px;
    }
    #frmCurrentNode h4, #frmEditNode h4{
        margin-bottom: 5px;
        margin-top: 15px;
    }
    #frmCurrentNode hr, #frmEditNode hr{
        margin-top: 0px;
        border-top: 1px solid #25a6ef;
    }
    #frmCurrentNode h4.w-90p, #frmEditNode h4.w-90p{
        text-align: left;
    }
    #frmCurrentNode label.col-md-4,#frmEditNode label.col-md-4{
        text-align: left;
        padding-left: 0px;
    }
    #frmCurrentNode .form-group, #frmEditNode .form-group{
        text-align: left;
    }
    .labelForInput {
        top: 6px;
    }
    #height{
        border-top-left-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
    }
    #frmCurrentNode .input-group-addon, #frmEditNode .input-group-addon{
        border-top-right-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
    }
    #frmCurrentNode .form-group.center, #frmEditNode .form-group.center{
        text-align: center;
    }
    .labels {
      color: white;
      font-family: "Lucida Grande", "Arial", sans-serif;
      font-size: 14px;
      text-align: left;
      font-weight: bold;
    }
    .right-area {
      position: absolute;
      right: 25px;
      top: 75px;
    }
    #player {
      position: relative;
    }
    .button-wrapper {
      position: absolute;
      right: 2px;
    }
    #goHome {
      background: transparent none repeat scroll 0 0;
      font-size: 12px;
      z-index: 0;
      vertical-align: middle;
      display: table-cell;
      padding-right: 15px;
      padding-bottom: 4px;
    }
    #goHome .inner {
      position: absolute;
      font-size: .85rem;
      color: #f00;
      font-weight: bold;
      margin: .8rem .2rem;
    }
    #goHome .jsClose.disabled {
      pointer-events: none;
      opacity: .6;
      user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
    }
    #map #drone_weather_toolbar{
        display: block;
    }
    #chartContainer{
        background: transparent none repeat scroll 0 0;
        font-size: 12px;
        position: absolute;
        left: 35px;
        bottom: 70px;
        z-index: 0;
    }
    #drone_weather_toolbar {
        background: transparent none repeat scroll 0 0;
        font-size: 12px;
        vertical-align: middle;
        display: table-cell;
        z-index: 0;
    }
    #drone_weather_toolbar .arrow{
        left: 88% !important;
    }
    .smooth-tab.attach-right {
        border-radius: 1.4em 0 0 1.4em / 1em 0 0 1em;
    }
    .smooth-tab {
        background-color: transparent;
        border: 0 solid;
        font-size: 1.1em;
        min-height: 1.6em;
        min-width: 1.6em;
    }
    #drone_weather_toolbar h4{
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 5px;
    }
    #drone_weather_toolbar hr{
        border-top: 1px solid #25a6ef;
        margin: 0px;
    }
    #drone_weather_toolbar > ul {
        margin-bottom: 3px;
    }
    #drone_weather_toolbar .form-group{
        margin-bottom: 0;
    }
    #drone_weatherIcon{
        clear: left;
        float: left;
        height: 4em;
        margin-bottom: 2px;
    }
    #drone_weather_toolbar ul li > a.dropdown-toggle {
        background: rgba(0, 0, 0, 0.45) none repeat scroll 0 0;
        display: inline-block;
        line-height: 0;
        padding: 0;
    }
    #drone_weather_toolbar a {
      outline: 0;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;

    }
    #drone_weather_toolbar a #weatherIcon_wrapper{
        width: auto;
        height: 45px;
    }
    #drone_weather_toolbar #weatherIcon_1{
        float: right;
        width: 38px;
        height: 38px;
        background-image: url(/assets/img/weatherDefault.png);
        background-repeat: no-repeat;
        background-position: center;
        background-size: 70% 70%;
        background-color: #28b8fb;
        border-radius: 50% !important;
        border: 2px solid #28b8fb;
        cursor: pointer;
    }
    #drone_weather_toolbar #weatherIcon_2{
        height: 70px;
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar .popover.fade.bottom.in{
        left: -220px !important;
        width: 300px !important;
        border-radius: 5px !important;
        background-repeat: no-repeat;
        background-position: center;
        background-image: url(/assets/img/weatherLoading.gif);
        background-size: 25px 25px;
        background-color: #eee;
    }
    #drone_weather_toolbar fieldset .col-sm-12, #drone_weather_toolbar fieldset .col-sm-5, #drone_weather_toolbar fieldset .col-sm-4, #drone_weather_toolbar fieldset .col-sm-7{
        padding-left: 0px;
    }
    #drone_weather_toolbar .boxWeather .col-md-8{
        padding-left: 5px;
    }
    #drone_weather_toolbar fieldset .col-sm-12, #drone_weather_toolbar fieldset .col-sm-5, #drone_weather_toolbar fieldset .col-sm-7{
        padding-right: 0px;
    }
    #drone_weather_toolbar .boxWeather.form-group {
        margin-left: 0;
    }
    #drone_weather_toolbar .backgroundDirection{
        width: 86px;
        height: 86px;
        background-image: url(/assets/img/weatherDirectionBG.png);
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar #windDirection{
        width: 86px;
        height: 86px;
        background-image: url(/assets/img/windDirection.png);
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar label{
        font-weight: 300;
        padding-left: 0px;
        padding-right: 0px;
    }
    #drone_weather_toolbar .form-group.weatherTitle{
        margin-bottom: 0px;
    }
    #drone_weather_toolbar .dateWeather{
        font-size: 11px;
    }
    #drone_weather_toolbar .nameWeather{
        font-weight: 600;
    }
    #drone_weather_toolbar .temperatureInfo{
        font-size: 20px;
    }
    #drone_weather_toolbar .temperatureInfo #temperatureWeather{
        font-size: 38px;
    }
    #drone_weather_toolbar .kccs_api{
        height: 75px;
    }
    #drone_weather_toolbar .kccs_api #reload_btn{
        margin-top: 5px;
        text-align: center;
    }
    #drone_weather_toolbar .kccs_api #reload_btn img{
        cursor: pointer;
    }
    #drone_weather_toolbar .kccs_api.form-group a img {
        margin-top: 10px;
        margin-bottom: 5px;
    }
    #app-layout .popover{
        width: 300px;
        transform : translateY(0px) !important;
        transition-duration: 0ms;
        transition-property: transform !important;
    }
    #app-layout .popover-tip{
        transition: -webkit-transform 0ms ease-in !important;
    }
    #drone_weather_toolbar .colon{
        float: right;
    }
    #jsNodes .bootstrap-switch{
        border-radius: 20px !important;
        border: 2px solid #fff !important;
        margin: 0 5px !important;
    }
    #jsNodes .bootstrap-switch.bootstrap-switch-inverse.bootstrap-switch-on .bootstrap-switch-label,#jsNodes .bootstrap-switch.bootstrap-switch-off .bootstrap-switch-label, #jsNodes .bootstrap-switch.bootstrap-switch-inverse.bootstrap-switch-off .bootstrap-switch-label, #jsNodes .bootstrap-switch.bootstrap-switch-on .bootstrap-switch-label {
        border-radius: 40% !important;
    }
    #jsNodes .bootstrap-switch .bootstrap-switch-handle-off, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on, #jsNodes .bootstrap-switch .bootstrap-switch-label{
        padding: 1px 12px !important;
    }
    #jsNodes .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default, #jsNodes .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-primary, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary{
        background-color: #222 !important;
    }
    #jsNodes .noFlyZone-switcher{
        display: none;
        /*line-height: 1;
        margin-top: 15px;
        margin-right: 20px;
        color: #fff;
        font-weight: 600;*/
    }
    /*#jsNodes .noFlyZone-switcher label.title{
        font-weight: 300;
        margin-right: 5px;
    }*/
    #jsNodes .geoFilter-switcher label.title{
        margin-right: 5px;
        font-size: 15px;
    }
    #jsNodes .geoFilter-switcher {
        line-height: 1;
        color: #fff;
        font-weight: 600;
        margin-right: 20px;
        margin-top: 15px;
    }
    .highcharts-legend{display: none;}
    .highcharts-contextbutton {display: none}
    .highcharts-credits{display: none}
    .highcharts-series.highcharts-series-1.highcharts-line-series.highcharts-color-1{display: none} 
</style>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/admin/logs/').'/'.$flight->id }}"><img src="/assets/img/back.png" alt="back" /> BACK</a>
        </div>
        <input type="hidden" id="flight" value="{!! $flight->id !!}" />
        <input type="hidden" id="idlogreplay" value="{!! $idlogreplay !!}" />
        {!! Form::open(array('id'=>'jsNodes','class'=>'form-horizontal', 'files'=>true)) !!}
        <ul class="nav navbar-nav navbar-right">
            <li class="geoFilter-switcher">
                <label class="title">Geo Filter:</label> ON <input id="geoFilterZone-checkbox" type="checkbox" name="geoFilterZone-checkbox" onchange="geoFilterSwicher()" data-on-text=""
                   data-off-text=""
                   data-handle-width="1px"
                   data-label-width="1px"
                   data-size="mini" checked> OFF
            </li>
            <li class="noFlyZone-switcher">
                <label class="title">飛行禁止区域表示:</label> ON <input id="noFlyZone-checkbox" type="checkbox" name="noFlyZone-checkbox" onchange="noFlyZoneSwicher()" data-on-text=""
                   data-off-text=""
                   data-handle-width="1px"
                   data-label-width="1px"
                   data-size="mini" checked> OFF
            </li>
            <li><a href="javascript:;" class="p-10-3 jsReplay unactive"><img src="/assets/img/replay.png" alt="保存" /></a></li>
        </ul>
        @if(!empty($homeposition))
            <input type="hidden" id="is_home" name="is_home" value ="1" />
             <input type="hidden" attr-home="{!!  $homeposition->is_home ? '1' : '0' !!}" attr-point-id="{!!  $homeposition->id !!}" id="old_node" name="node[]" value ="{!! $homeposition->id.','. $homeposition->lat.','.$homeposition->lng.','.$homeposition->height !!}" />
        @endif
        @if(!empty($nodes) && count($nodes) > 0)
            @foreach($nodes as $node)
                <input type="hidden" attr-home="{!!  $node->is_home ? '1' : '0' !!}" attr-point-id="{!!  $node->id !!}" id="old_node" name="node[]" value ="{!! $node->id.','. $node->lat.','.$node->lng.','.$node->height !!}" />
            @endforeach
        @endif
        @if(!empty($geofilters) && count($geofilters) > 0)
            @foreach($geofilters as $geofilter)
                <input type="hidden" attr-geofilter-id="{!!  $geofilter->id !!}" id="geo_filters" name="geo_filters[]" value ="{!! $geofilter->id.'-'. $geofilter->array_points_convert !!}" />
            @endforeach
        @endif
        {!! Form::close() !!}
    </div>
</nav>
<!--Map -->

<div id="map">
</div>
<div class="right-area">
  <div id="player">
    <!-- <iframe width="476" height="268" src="https://www.youtube.com/embed/UMDiZEMU5e8" frameborder="0" allowfullscreen id="live_view"></iframe> -->
  </div>
  <div class="button-wrapper bootstrap">
      <!-- <input type="text" style="width:200px;display:none;" value="XdlmoLAbbiQ" id="live_view_key">
      <input type="button" value="URL 更新" id="change_live_view_key" style="display:none;"> -->
      <!-- <div id="drone_weather_toolbar" class="smooth-tab attach-right dark">
        <a id="drone_weather_icon"
           tabindex="0"
           role="button"
           data-placement="bottom">
           <div id="weatherIcon_wrapper"><div id="weatherIcon_1">&nbsp;</div></div>
        </a>
      </div> -->
  </div>

  <div id="weather-content" class="hidden">
      <fieldset class="col-sm-12 form-horizontal">
          <div class="form-group weatherTitle">
              <h4 class="w-90p"><span id="addressWeather"></span>気象情報 </h4>
              <hr>
          </div>
          <div class="dateWeather form-group">
              <span id="dateForecast"></span> 時点の気象情報
          </div>
          <div class="iconWeather form-group">
              <div class="col-sm-5" id="weatherIcon_2">

              </div>
              <div class="col-sm-7 temperatureInfo">
                  <span id="temperatureWeather"></span>°C
              </div>
          </div>
          <div class="nameWeather form-group">
              <span id="nameWeather"></span>
          </div>
          <div class="boxWeather form-group">
              <div class="col-sm-7">
                  <div class="form-group">
                      <label class="col-md-4">湿度 <span class="colon">:</span></label>
                      <div class="col-md-8"><span id="humidityForecast"></span>%</div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">降水確率 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="precipitationWeather"></span>%
                          (<span id="timeWeather"></span>)
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">風速 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="windVelocityForecast"></span>m/s
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">風向 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="directStringForecast"></span> (<span id="directAngleForecast"></span>°)
                      </div>
                  </div>
              </div>
              <div class="col-sm-5 backgroundDirection">
                  <div id="windDirection">

                  </div>
              </div>
          </div>
          <div class="kccs_api form-group">
              <a href="http://www.energy-cloud.jp"><img src="/assets/img/logo_kccs.png" title="KCCS API サービス"></a>
              <hr>
              <div id="reload_btn" class="reloadHidden"></div>
          </div>
      </fieldset>
  </div>
</div>
<div id="chartContainer" style="min-width: 500px; height: 250px; margin: 0 auto"></div>
<div class="modal fade" id="alertModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                ドローンからの応答が途切れました。
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

<script src="{{ URL::to('assets/js/mc_category_a.js') }}"></script>
<script src="{{ URL::to('assets/js/mc_category_b.js') }}"></script>
<script src="{{ URL::to('assets/js/mc_category_city_limit.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $player = $("#player");
        $player.hide();
        $('.jsReplay').on('click', function(){
            var _this = $(this);
            if(_this.hasClass('active')) {
                for (var i = 0; i < timeOutLoadNodeArray.length; i++) {
                    clearTimeout(timeOutLoadNodeArray[i]);
                }
                removeAllNodesForReplay();
                _this.addClass('unactive').removeClass('active');
                _this.find('img').attr('src','/assets/img/replay.png');
                $player.hide();
                var droneChart = $('#chartContainer').highcharts();
                droneChart.series[1].data.forEach(function(chart, key){
                    chart.update({
                        marker: {symbol: 'url(/assets/img/blank.png)'}
                    });
                });
            }else{
                _this.addClass('active').removeClass('unactive');
                _this.find('img').attr('src','/assets/img/stopReplay.png');
                // Start Get Log And Process Replay
                ajaxLoadNodesForReplay();
                // Show video stream
                $player.show();
            }
        });
    });
    var map;
    var latLngBounds;
    var gmarkers = [];
    var glocations = [];
    var marker = [];
    var id = [];
    var hight = [];
    var flight;
    var idlogreplay;
    var object =[];
    var infoWindows = [];
    var center;
    var c_lat = 35.65325;
    var c_long = 139.772049;
    var point;
    var polylines = [];
    var init = false;
    var is_home = false;
    var logNodes = [];
    var currentNode;
    var currentNodeMarker;
    var flightInfo;

    var red_image = '/assets/img/pin-red.png';
    var blue_image = '/assets/img/pin-blue.png';
    var category_a_name = 'Category A';
    var category_b_name = 'Category B';
    var category_city_limit_name = 'NO Fly';
    var opened_info = null;
    var inc = 0;

    var initialBounds;
    var aircrafts = [];
    var noFlyZones = [];
    var geoFilterZones = [];
    var logsNodeArray = [];
    var weatherLoaded = false;
    var initialCenter;
    var weatherIconClicked = false;

    var radiusEarth=6372.7976;
    var lastTimeGetLog = new Date().getTime();
    var lastLogID;
    var getLogInterval;
    var modalAlertShowed = false;
    var timeOutLoadNodeArray = [];
    var timeOutLoadNode;

    function initialize() {
        flight = $('#flight').val();
        idlogreplay = $('#idlogreplay').val();
        var location = new Array();
        center = new google.maps.LatLng(c_lat, c_long);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center:center,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeId: 'satellite',
            disableDefaultUI: true,
        });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                c_lat = position.coords.latitude;
                c_long = position.coords.longitude;
                center = new google.maps.LatLng(c_lat, c_long);
                if($('#jsNodes #old_node').length == 0){
                    map.setCenter(new google.maps.LatLng(c_lat, c_long));
                }
                setHomeLocation(center);
            }, function(error) {
            });
        }else {
            center = new google.maps.LatLng(c_lat, c_long);
            setHomeLocation(center);
        }


        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zoomControlDiv);

        var markers = [];
        latLngBounds = new google.maps.LatLngBounds();
        $('#jsNodes #old_node').each (function () {
            var _val = $(this).val().split(",");
            markers.push(_val[1]+','+_val[2]);
            object.push(_val);
            if($(this).attr('attr-home') ==1) {
                is_home = true;
                inc = -1;
            }
        });



        if(object.length >0 ) {
            loadMarkers(markers);
        }else{
            //total = 0;
            //var center = 1;
            /*Get location*/
            /*navigator.geolocation.getCurrentPosition(function(position) {
                var _lat = position.coords.latitude;
                var _long = position.coords.longitude;
                var pos = {
                  lat: _lat,
                  lng: _long
                };
                map.setCenter(pos);
                var latlng = new google.maps.LatLng(_lat,_long);
                var center = 1;
                createNode(flight,_lat, _long, latlng, center);
                glocations.push(_lat+','+_long);
            }, function() {
                var _lat = c_lat;
                var _long = c_long;
                var latlng = new google.maps.LatLng(_lat,_long);
                createNode(flight,_lat, _long, latlng, center);
                glocations.push(_lat+','+_long);
            });*/
            init = true;
        }

        google.maps.event.addListener(map, 'click', function (e) {
            var max = 10;
            if(!is_home || init) {
                max = 9;
            }
            console.log(inc+'create');
            if(inc <= max && $('.jsCreatNode').hasClass('active')) {
                if(glocations.length == 0) {
                    var lat = e.latLng.lat().toFixed(7);
                    var lgn = e.latLng.lng().toFixed(7);
                    var latlng = new google.maps.LatLng(lat,lgn);
                    if(is_home)  {
                        var center = 1;
                    }else{
                        var center = 0;
                    }
                    createNode(flight, lat, lgn, latlng, center, inc);
                }else{
                    var lat = e.latLng.lat().toFixed(7);
                    var lgn = e.latLng.lng().toFixed(7);
                    var latlng = new google.maps.LatLng(lat,lgn);
                    var center = 0;
                    createNode(flight, lat, lgn, latlng, center, inc);

                    var length = glocations.length -1;
                    var endCoords = glocations[length].split(",");
                    var lat_end = endCoords[0];
                    var lgn_end = endCoords[1];

                    drawLine(lat, lgn, lat_end, lgn_end)
                }
                glocations.push(lat+','+lgn);
            }
        });

        map.setZoom(18);

        // google.maps.event.addListener(map, 'bounds_changed', scheduleDelayedCallback(map));
        google.maps.event.addListener(map, 'idle', function() {
            if (this.isMapDragging) {
                this.idleSkipped = true;
                return;
            }
            this.idleSkipped = false;
            this._respondToMapChange(map);
        }.bind(this));
        google.maps.event.addListener(map, 'dragstart', function() {
            this.isMapDragging = true;
        }.bind(this));
        google.maps.event.addListener(map, 'dragend', function() {
            this.isMapDragging = false;
            if (this.idleSkipped == true) {
                this.idleSkipped = false;
            }
        }.bind(this));
        google.maps.event.addListener(map, 'bounds_changed', function() {
            this.idleSkipped = false;
        }.bind(this));
        /**
         * Draw exist geo filter zone
         */
        $('#jsNodes #geo_filters').each (function () {
            var flyAreaWrapperPath = [];
            var _val = $(this).val().split("-");
            var arrayPointsOfFilterZone = _val[1].split(";");
            arrayPointsOfFilterZone.forEach(function (value, key){
                coordinate = value.split(',');
                flyAreaWrapperPath.push({lat: parseFloat(coordinate[0]), lng: parseFloat(coordinate[1])});
            });
            var oldFlyAreaWrapper = new google.maps.Polygon({
                paths: flyAreaWrapperPath,
                strokeColor: '#052eff',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#052eff',
                fillOpacity: 0.2
            });
            oldFlyAreaWrapper.setMap(map);
            geoFilterZones.push(oldFlyAreaWrapper);
        });
        setMapClearGeoFilterZone(null);
    }
    // google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script src="{{ URL::to('assets/js/replay-flight-1.0.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwv_DIo4nEWCHNnqpDm5f5-VGkmxjYLN8&libraries=drawing,geometry&callback=initialize"
    async defer></script>
@endsection
