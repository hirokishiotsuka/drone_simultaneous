@extends('layouts.app')

@section('content')
<div class="col-md-10 col-sm-11 main">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>アカウント設定</h2>
                </div>
                <div class="panel-body">
                    @include('auth.partials.errors')
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/user/create') }}">
                        {{ csrf_field() }}
                        <fieldset class="col-sm-6 admin-panel">
                            <div class="form-group">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="company_name">企業名（※）</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="company_name" type="text" class="form-control" placeholder="{{ $company->name }}" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="company_address">住所</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="company_address" type="text" class="form-control" placeholder="{{ $company->address }}" disabled="disabled""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-with-hr">
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">名前</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"/>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">メールアドレス</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="email" type="text" class="form-control" name="email" value="{{ old('email') }}"/>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">パスワード</label>
                                        <div class="col-sm-8">
                                            <input id="password" class="form-control" id="password" type="password" class="form-control" name="password"/>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">パスワード（確認）</label>
                                        <div class="col-sm-8">
                                            <input id="passwordconf" class="form-control" id="password-confirm" type="password" class="form-control" name="password_confirmation"/>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                                <div class="controls row">
                                    <div class="input-group col-sm-12">
                                        <label class="col-sm-4 control-label" for="email">電話番号</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="tel" type="text" class="form-control" name="tel" value="{{ old('tel') }}"/>
                                            @if ($errors->has('tel'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('tel') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group buttonbox">
                                <a href="{{ url('/admin/login') }}" class="btn btn-default btn-lg col-xs-4 pull-left">キャンセル</a> <button type="submit" class="btn btn-primary btn-lg col-xs-7 pull-right">登録</button>
                            </div>
                        </fieldset>
                    </form>

                <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- start: JavaScript-->
<!--[if !IE]>-->


<!--<![endif]-->

<!--[if IE]>

    <script src="assets/js/jquery-1.11.0.min.js"></script>

<![endif]-->

<!--[if !IE]>-->

    <script type="text/javascript">

    </script>

<!--<![endif]-->

<!--[if IE]>

    <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

<![endif]-->
<script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


<!-- page scripts -->
<script src="{{ URL::to('assets/js/jquery.icheck.min.js') }}"></script>

<!-- theme scripts -->
<script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
<script src="{{ URL::to('assets/js/core.min.js') }}"></script>

<!-- inline scripts related to this page -->
<script src="{{ URL::to('assets/js/pages/login.js') }}"></script>

<!-- end: JavaScript-->
<script type="text/javascript">
    $('#terms').click(function() {
        $('#termsModal').modal('show');
    });
    $('#policies').click(function() {
        $('#policiesModal').modal('show');
    });
</script>
@endsection