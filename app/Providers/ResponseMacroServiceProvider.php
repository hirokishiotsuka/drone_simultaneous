<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    
    /**
     * Bootrap the application service
     */
    public function boot() {
        Response::macro('success', function ($data) {
            return Response::json([
                'status' => true,
                'data'   => $data,
                'error' => [
                    'code' => 0 ,
                    'message' => []
                ]
            ]);
        });


        Response::macro('error', function ($message, $status = 400) {
            return Response::json([
                'status' => false,
                'data' => null,
                'error' => [
                    'code' => $status,
                    'message' => $message
                ]
            ]);
        });
    }

    /**
     * Register application service
     * @override
     */
    public function register() {

    }

}