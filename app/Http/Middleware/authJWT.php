<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Exception;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     */
    public $currentUser;

    public function handle($request, Closure $next)
    {
        try {
            $currentUser = JWTAuth::toUser($request->input('token'));
            $request->attributes->add(['currentUser' => $currentUser]);
        }
        catch(Exception $e){
            try{
                $authHeader = $request->hasHeader('authorization');
                list($jwt) = sscanf( $request->header('authorization'), 'Bearer %s');
                $currentUser = JWTAuth::toUser($jwt);
                $request->attributes->add(['currentUser' => $currentUser]);
            } catch(Exception $e) {
                if($e instanceof TokenInvalidException){
                    return response()->error(['error' => 'Token is invalid.'], 500);
                }
                else if($e instanceof TokenExpiredException){
                    return response()->error(['error' => 'Token is expried.'], 500);
                }
                else {
                    return response()->error(['error' => 'Token not provide.'], 500);
                }
            }
        }
        return $next($request);
    }
}
