@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-sm-11 main">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">

                    <div class="panel-heading" data-original-title>
                        <h2><span class="break"></span><a href="{{ URL::to('/admin/drone/') }}">機体管理</a>&nbsp;&nbsp;>&nbsp;&nbsp;登録</h2>
                        <div class="panel-actions">

                        </div>
                    </div>

                    <div class="panel-body">
                        {{--@include('auth.partials.errors')--}}
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/drone/create') }}">
                            {{ csrf_field() }}
                            <fieldset class="col-sm-6 admin-panel">

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="name">機体名</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Phantom 3" />
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('manufacture') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="manufacture">製造者名</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" id="manufacture" type="text" class="form-control" name="manufacture" value="{{ old('manufacture') }}" placeholder="DJI" />
                                                @if ($errors->has('manufacture'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('manufacture') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('serial') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="serial">製造番号</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" id="serial" type="text" class="form-control" name="serial" value="{{ old('serial') }}" placeholder="xxxxxxxxx000000000" />
                                                @if ($errors->has('serial'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('serial') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('insurance') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="insurance">無人航空機保険加入済</label>
                                            <div class="col-sm-8">
                                                <div class="custom-checkbox">
                                                    <input name="insurance" value="1" id="insurance" type="checkbox" >
                                                    <label for="insurance">&nbsp;</label>
                                                </div>
                                                @if ($errors->has('insurance'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('insurance') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('renovation') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="renovation">改造有り</label>
                                            <div class="col-sm-8">
                                                <div class="custom-checkbox">
                                                    <input name="renovation" value="1" id="renovation" type="checkbox" >
                                                    <label for="renovation">&nbsp;</label>
                                                </div>
                                                @if ($errors->has('renovation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('renovation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('post_on_homepage') ? ' has-error' : '' }}">
                                    <div class="controls row">
                                        <div class="input-group col-sm-12">
                                            <label class="col-sm-4 control-label" for="post_on_homepage">ホームページ掲載有り</label>
                                            <div class="col-sm-8">
                                                <div class="custom-checkbox">
                                                    <input name="post_on_homepage" value="1" id="post_on_homepage" type="checkbox" >
                                                    <label for="post_on_homepage">&nbsp;</label>
                                                </div>
                                                @if ($errors->has('post_on_homepage'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('post_on_homepage') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group buttonbox">
                                    <a href="{{ url('/admin/drone') }}" class="btn btn-default btn-lg col-xs-4 pull-left">キャンセル</a> <button type="submit" class="btn btn-primary btn-lg col-xs-7 pull-right">登録</button>
                                </div>
                            </fieldset>
                        </form>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- start: JavaScript-->
    <!--[if !IE]>-->


    <!--<![endif]-->

    <!--[if IE]>

    <script src="assets/js/jquery-1.11.0.min.js"></script>

    <![endif]-->

    <!--[if !IE]>-->

    <script type="text/javascript">

    </script>

    <!--<![endif]-->

    <!--[if IE]>

    <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

    <![endif]-->
    <script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


    <!-- page scripts -->
    <script src="{{ URL::to('assets/js/jquery.icheck.min.js') }}"></script>

    <!-- theme scripts -->
    <script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/core.min.js') }}"></script>

    <!-- end: JavaScript-->
@endsection