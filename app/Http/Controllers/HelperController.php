<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Http\Middleware\authJWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Flight;
use App\Repositories\FlightRepository;
// use App\Helpers\ImageHelpers;
use App\FlightLog;
use App\Point;
use Storage;
use App\FlightMedia;
use Illuminate\Routing\UrlGenerator;

class HelperController extends Controller
{
    protected $url;
    public function __construct(FlightRepository $flights, UrlGenerator $url, FlightMedia $medias, FlightLog $logs, Point $points, Request $request)
    {
    	parent::__construct();
    	$this->flights = $flights;
    	$this->config = config('file_uploads');
        $this->url = $url;
        $this->medias = $medias;
        $this->logs = $logs;
        $this->points = $points;
        $this->currentUserAPI = $request->get('currentUser');
    }

    /**
     * API Upload for Flight
     * @param $request      Illuminate\Http\UploadedFile      Get form request
     * @param $id           $id                               Id of toilet                    
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     **/
    public function uploadAPI(Request $request)
    {
        try {
            $data   = $request->all();
            $rules  = [
                'paths.*' 		=> 'required|string',
                'id' 			=> 'required|numeric',
                'point_id'      => 'numeric',
                'name'			=> 'required|string|max:255',
				'lat'			=> 'required|numeric',
				'lng'			=> 'required|numeric',
				'height'		=> 'required|numeric',
				'recorded_at'	=> 'required|date_format:Y-m-d H:i:s',
            ];

            $validator = Validator::make($data, $rules);
            if($validator->passes()) {
            	$flight = $this->flights->find($data['id']);
				// $user = JWTAuth::toUser($data['token']);
				if ($flight != null && $flight->user_id == $this->currentUserAPI->id) {
					if ($data['paths'] === null) return response()->error('Path Array Null', 404);
					foreach ($data['paths'] as $key => $value) {
						$moreInfo = [
							'name'				=> $data['name'],
							'link'				=> $value,
							'user_id'			=> $this->currentUserAPI->id,
							'lat'				=> $data['lat'],
							'lng'				=> $data['lng'],
							'height'			=> $data['height'],
							'recorded_at'		=> $data['recorded_at'],
						];
                        if ($request->has('point_id')) {
                            $point = $this->points->find($data['point_id']);
                            if($point->flight_id === $flight->id) {
                                $moreInfo['point_id'] = $data['point_id'];
                            }
                        }
						$this->attachMediaToModel($flight, $moreInfo);
					}
					$flight = $this->flights->find($data['id']);
					$flight->medias = $flight->medias;
	                return response()->success(compact('flight'));
				}
				return response()->error($flight, 404);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function mediaAPI(Request $request)
    {
    	try {
            $data     = $request->all();
            $rules    = [
            	'id'		=> 'required|numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
            	$log = $this->logs->find($data['id']);
            	// $user = JWTAuth::toUser($data['token']);
            	if ($log != null && $log->user_id == $this->currentUserAPI->id) {
                    $firstLog = $this->logs->where('flight_id', $log->flight_id)->where('fstarted_at', $log->fstarted_at)->orderBy('created_at', 'asc')->first();
                    $lastLog = $this->logs->where('flight_id', $log->flight_id)->where('fstarted_at', $log->fstarted_at)->orderBy('created_at', 'desc')->first();
                    // dd($lastLog);
                    $medias = $this->medias->where('flight_id', $log->flight_id)->where('recorded_at','>=', $firstLog->created_at)->where('recorded_at','<=', $lastLog->created_at)->select(['*'])->paginate(15);
                    // $medias = $this->addBaseURL($medias);
                    // $medias->data->thumbnail = 'test'.$medias->data->name;
                    // dd($medias.isEmpty());
                    // dd($medias);
                    $log->medias = $medias;

            		return response()->success(compact('log'));
				}
				return response()->error($log, 404); 
			}
			return response()->error($validator->errors()->all(), 300);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
    }

    protected function addBaseURL($collection)
    {
        return $collection->map(function($value) {
            $value->link = $this->url->to('/').'/'. $value->link;
            return $value;
        });
    }

    protected function createPath($paths)
    {
        if (!is_array($paths)) {
            if (!\File::exists($paths)) {
                \File::makeDirectory($paths, $mode = 0777, true, true);
            }
        } else {
            foreach ($paths as $path) {
                if (!\File::exists($path)) {
                    \File::makeDirectory($path, $mode = 0777, true, true);
                }
            }
        }
        return $paths;
    }

    public function getConfig($option = null)
    {
        return $option === null ? $this->config : (isset($this->config[$option]) ? $this->config[$option] : null);
    }

    /**
     * Create file name.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file File
     *
     * @return string
     * @author Quynh Nguyen Add String General
     */
    public function getFileName($file)
    {
        $stringGeneral = md5(uniqid($file->getClientOriginalExtension(), true));
        $fileNameOri = preg_replace("/[^a-zA-Z0-9_\-.]/", "", $file->getClientOriginalName());
        $fileName = substr($stringGeneral, 0, 16) . '-' . $fileNameOri;
        return $fileName;
    }

    /**
     * Function Attach Relation Image for Model
     *
     * @param $model      	$model     		Model attach relation with Image
     * @param $imageArr 	$imageArr      	Images Array                    
     * 
     * @return Illuminate\Database\Eloquent\Model
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     **/
    public function attachMediaToModel($model, $moreInfo = array())
    {
        $attachImg = new FlightMedia();
        foreach ($moreInfo as $key => $value) {
        	$attachImg->$key = $value;
        }
        $model->medias()->save($attachImg);
        return $model;
    }
}
