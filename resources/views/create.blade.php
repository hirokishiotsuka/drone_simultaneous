<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::to('assets/ico/apple-touch-icon-57-precomposed.png') }}">
    <!-- <link rel="shortcut icon" href="{{ URL::to('assets/ico/favicon.png') }}"> -->

    <title>Drone Control Panel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
    <!-- Bootstrap core CSS -->
    <link href="{{ URL::to('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- page css files -->
    <link href="{{ URL::to('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/morris.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/climacons-font.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="{{ URL::to('assets/css/style.min.css') }}" rel="stylesheet"> -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        * {
          border-radius: 0 !important;
        }
        * {
          -webkit-border-radius: 0 !important;
             -moz-border-radius: 0 !important;
                  border-radius: 0 !important;
        }
    </style>
</head>
<body id="app-layout">
    @if (!Auth::guest())
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('/admin/flight/list') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> BACK</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="btn btn-default btn-" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Add new Point</a></li>

                <li><a href="#"><i class="fa fa-bookmark" aria-hidden="true"></i> Save</a></li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">

    <div class="col-md-10 col-sm-11 main">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5>Add New Flight</h5>
                    </div>
                    <div class="panel-body">
                        @include('auth.partials.errors')
                        <form action="{{ URL::to('/admin/flight/proCreate') }}" method="post">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" id="user_id" class="form-control" value="{{ $user_id }}" disabled="disabled">
                                    <input type="hidden" name="user_id" value="{{ $user_id }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name of Flight">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <button type="submit" class="btn btn-sm btn-success"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endif
    <!--[if !IE]>-->


    <!--<![endif]-->

    <!--[if IE]>

        <script src="{{ URL::to('assets/js/jquery-1.11.0.min.js') }}"></script>

    <![endif]-->

    <!--[if !IE]>-->

        <script type="text/javascript">

        </script>

    <!--<![endif]-->

    <!--[if IE]>

        <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
        </script>

    <![endif]-->
    <script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


    <!-- page scripts -->
    <script src="{{ URL::to('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/fullcalendar.min.js') }}"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/js/excanvas.min.js"></script><![endif]-->
    <script src="{{ URL::to('assets/js/jquery.flot.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.time.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.autosize.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.placeholder.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/moment.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/daterangepicker.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/raphael.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/morris.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/uncompressed/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ URL::to('assets/js/uncompressed/gdp-data.js') }}"></script>
    <script src="{{ URL::to('assets/js/gauge.min.js') }}"></script>

    <!-- theme scripts -->
    <script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/core.min.js') }}"></script>

    <!-- inline scripts related to this page -->
    <script src="{{ URL::to('assets/js/pages/index.js') }}"></script>

    <!-- end: JavaScript-->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
