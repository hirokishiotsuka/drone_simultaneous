<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    protected $loginPath = 'admin/home';

    protected $redirectPath = 'admin/home';

    protected $redirectAfterLogout = 'admin/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'tel' => 'required|numeric',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
            'password_confirmation' => 'required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
            'company_name' => 'required|max:255',
            'company_address' => 'required|max:255',
        ];
        $messages = [
            'name.required'             => '名前を入力してください',
            'name.max'                  => '255文字以内で入力してください',
            'email.email'               => '正しいメールアドレスの形式で入力してください',
            'email.unique'              => '電子メールは既に受け取られました',
            'email.required'            => 'メールアドレスを入力してください',
            'email.max'                 => '255文字以内で入力してください',
            'tel.required'              => '電話番号を入力してください',
            'tel.numeric'               => '数字を入力してください',
            'password.required'         => 'パスワードを入力してください',
            'password.min'              => '8文字以上入力してください',
            'password.regex'            => 'パスワードが文字と数字を合わせて入力してくだ',
            'password_confirmation.required' => 'パスワード確認を入力してください',
            'password_confirmation.min' => '8文字以上入力してください',
            'password_confirmation.regex' => 'パスワードが文字と数字を合わせて入力してくだ',
            'password.confirmed'        => 'パスワード確認が間違っています',
            'company_name.required'     => '企業名を入力してください',
            'company_name.max'          => '255文字以内で入力してください',
            'company_address.required'  => '住所を入力してください',
            'company_address.max'       => '255文字以内で入力してください',
        ];
        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $company = Company::create([
            'name' => $data['company_name'],
            'address' => $data['company_address'],
        ]);
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'telephone' => $data['tel'],
            'password' => bcrypt($data['password']),
            'company_id' => $company->id,
        ]);
    }

    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return redirect()->route('login')->with('status', trans($response));
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ];
        $messages = [
            'email.email'           => 'メールアドレスの形式が間違っています',
            'email.required'        => 'メールアドレスを入力してください。',
            'password.required'     => 'パスワードを入力してください。',
            'password.min'          => '8文字以上入力してください',
        ];
        $this->validate($request, $rules, $messages);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }
}
