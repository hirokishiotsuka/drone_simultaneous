function ZoomControl(controlDiv, map) {
    // Creating divs & styles for custom zoom control
    controlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '70px';
    controlWrapper.style.height = '150px';
    controlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '50px';
    zoomInButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/assets/img/icZoomOut.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '50px';
    zoomOutButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/assets/img/icZoomin.png")';
    controlWrapper.appendChild(zoomOutButton);

    var locationButton = document.createElement('div');
    locationButton.style.width = '50px';
    locationButton.style.height = '60px';
    /* Change this to be the .png image you want to use */
    locationButton.style.backgroundImage = 'url("/assets/img/icLocation.png")';
    controlWrapper.appendChild(locationButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    google.maps.event.addDomListener(locationButton, 'click', function() {
        var l_latlng = new google.maps.LatLng(c_lat, c_long);
        map.setCenter(l_latlng);
        setHomeLocation(l_latlng);
    });
}

function setHomeLocation(c_latlng) {
    var marker = new google.maps.Marker({
        position: c_latlng,
        map: map,
        label: {
            text: 'A',
            color: "#2396e5",

        },
        draggable:true,
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            strokeColor: '#ECFCF8',
            fillColor: '#2396e5',
            fillOpacity: 1,
            strokeWeight: 5,
        }
    });
    marker.setMap(map);
}

function createNode(flight, _lat, _long, latlng, center, pos, is_subuser, user_id) {
    var subuser = is_subuser || false;
    var _user_id = user_id || 1;
    var _url = '/admin/flight/createNode?id=' + flight + '&user_id=' + user_id + '&lat=' + _lat + '&long=' + _long;
    $.ajax({
        url: _url,
        type: "POST",
        dataType: "JSON",
        success: function(res){
            if(res.status) {
              var str = res.data.id + ',' + res.data.lat + ','+ res.data.lng + ',' + res.data.height+',0,0,0,'+ res.data.user_id;
              subuser ? object_2.push(str.split(',')) : object_1.push(str.split(','));
              var obj = str.split(',');
              var type = 'news';
              addMarker(latlng, map, obj, center, pos, type, subuser, _user_id);
            } else {
            }
        }
    });
}

function loadMarkers(markers, object, is_subuser) {
    var locations = new Array;
    var length = markers.length;
    var _is_subuser = is_subuser || false;
    if(length > 0){
        if(is_home && length >= 11 ) {
            length = 11;
        }else if(length > 10) {
            length = 10;
        }
        for (var i=0; i < length; i++){
            locations.push(markers[i]);
            j = i + 1;
            var start = markers[i];
            var end = markers[j];

            /*Start*/
            var startCoords = start.split(",");
            var lat_start = startCoords[0];
            var lgn_start = startCoords[1];
            var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
            var obj = object[i];
            if(!is_home) {
                addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);
            }else{
                if(i== 0) {
                  center = 1;
                  addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);

                }else{
                    center = 0;
                    addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);
                }
            }

            if(length == 1) {
                latLngBounds = drawLine(lat_start, lgn_start, lat_start, lgn_start);
            }
            if(j != length) {
                /*End*/
                var endCoords = end.split(",");
                var lat_end = endCoords[0];
                var lgn_end = endCoords[1];
                latLngBounds = drawLine(lat_start, lgn_start, lat_end, lgn_end);
            }

        }
        map.fitBounds(latLngBounds);
        if (_is_subuser) {
          gmarkers_2 = locations;
          glocations_2 = locations;
        } else {
          gmarkers_1 = locations;
          glocations_1 = locations;
        }
    }
}

function moveMarkerDrawLine(arg_marker_1, arg_marker_2, arg_marker_3,pos_node) {
    _marker_2 = arg_marker_2.split(",");
    lat_2 = _marker_2[0];
    lgn_2 = _marker_2[1];

    if(arg_marker_1 != null){
        _marker_1 = arg_marker_1.split(",");
        lat_1 = _marker_1[0];
        lgn_1 = _marker_1[1];
        drawLine(lat_1, lgn_1, lat_2, lgn_2,pos_node-1);
    }
    if(arg_marker_3 != null){
        _marker_3 = arg_marker_3.split(",");
        lat_3 = _marker_3[0];
        lgn_3 = _marker_3[1];
        drawLine(lat_2, lgn_2, lat_3, lgn_3,pos_node);
    }
}

function drawLine(lat_start, lgn_start, lat_end, lgn_end, pos_node) {
    var startPt = new google.maps.LatLng(lat_start,lgn_start);
    var endPt = new google.maps.LatLng(lat_end,lgn_end);
    calcRoute(startPt, endPt, pos_node);
    latLngBounds.extend(startPt);
    latLngBounds.extend(endPt);
    return latLngBounds;
}

function calcRoute(source,destination,pos_node){
    var polyline = new google.maps.Polyline({
        path: [source, destination],
        strokeColor: '#FFF',
        strokeWeight: 2,
        strokeOpacity: 1
    });
    polyline.setMap(map);
    if(pos_node!=null){
        polylines[pos_node] =  polyline;
    }else{
        polylines.push(polyline);
    }
}
function addMarker(latlng, map, obj, center, pos, type, subuser, user_id) {
    var _subuser = subuser || false;
    var _user_id = user_id || 1;
    var operator = '';
    if (_subuser) {
      if(center == 1) {
          title = 'H';
          color = '#f7255c';
          operator = 'drone-admin2';
      }else{
          title = (++inc_2).toString();
          color = '#00ff84';
          operator = '';
      }
    } else {
      if(center == 1) {
          title = 'H';
          color = '#f7255c';
          operator = 'drone-admin1';
      }else{
          title = (++inc_1).toString();
          color = '#28b8fb';
          operator = '';
      }
    }
    // var title = '';
    // if(center != 1) {
    //   if (_subuser) {
    //     title = (++inc_2).toString();
    //   } else {
    //     title = (++inc_1).toString();
    //   }
    // } else {
    //   title = 'H';
    // }
    var image = {
      url: '/assets/img/icon10_v2.png',
      anchor: new google.maps.Point(16,16),
      origin: new google.maps.Point(0,0),
      size: new google.maps.Size(32, 32),
      scale: 1,
    };
    // if(is_home && (inc_1 == 0 || inc_2 == 0)) {
    //     title = 'H';
    // }
    if(title == 10){
        var markerPoint = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable: true,
          icon: image,
          zIndex: 2,
          optimized: false
        });
    }else{
        var markerPoint = new google.maps.Marker({
            position: latlng,
            map: map,
            label: {
                text: title,
                color: "#FFF",
                fontWeight: "bold",
            },
            draggable:true,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 15,
                strokeColor: '#fff',
                strokeWeight: 2,
                fillColor: color,
                fillOpacity: 1
            },
            zIndex: 1,
            optimized: false
        });
    }
    var lat = latlng.lat().toFixed(7);
    var lgn = latlng.lng().toFixed(7);
    markerPoint.setMap(map);
    // var _homeTop = $('#map .gm-style > div > div > div .gmnoprint:first-child').css("top");
    // var _homeLeft = $('#map .gm-style > div > div > div .gmnoprint:first-child').css("left");
    if(is_home && inc_1 == 0) {
      var markerHome = new google.maps.Marker({
          position: latlng,
          map: map,
          label: {
              text: operator + "\n",
              color: "#FFF",
              fontWeight: "bold"
          },
          icon: {
            url: "http://maps.gstatic.com/mapfiles/markers2/arrowtransparent.png",
            size: new google.maps.Size(50, 50)
          },
          draggable:true,
          zIndex: 1,
          optimized: false
      });
      markerHome.setMap(map);
      // $('#map').append("<span style='position:relative; left:calc(" + _homeLeft + " - 2rem); top:calc(" + _homeTop + " - 2rem); color: #fff; text-align: center; text-shadow: 0 0 3px #000; font-weight: bold; font-size: 14px;'>" + operator + "</span>");
    }
    if(is_home && inc_2 == 0) {
      var markerHome = new google.maps.Marker({
          position: latlng,
          map: map,
          label: {
              text: operator + "\n",
              color: "#FFF",
              fontWeight: "bold"
          },
          icon: {
            url: "http://maps.gstatic.com/mapfiles/markers2/arrowtransparent.png",
            size: new google.maps.Size(50, 50)
          },
          draggable:true,
          zIndex: 1,
          optimized: false
      });
      markerHome.setMap(map);
      // $('#map').append("<span style='position:relative; left:calc(" + _homeLeft + " - 2rem); top:calc(" + _homeTop + " - 2rem); color: #fff; text-align: center; text-shadow: 0 0 3px #000; font-weight: bold; font-size: 14px;'>" + operator + "</span>");
    }

    var _id = obj[0];
    var _height = obj[3];
    var _capture = obj[4];
    var _rightAngle = obj[5];
    var _timeStay = obj[6];
    var _user_id = obj[7];
    if(_capture == 0){
        var _capture_convert = "off";
    }else var _capture_convert = "on";
    if(_rightAngle == 0){
        var _rightAngle_convert = "off";
    }else var _rightAngle_convert = "on";

    if (_subuser) {
      $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes_sub[]" value ="'+ lat + ',' + lgn +',' + _height + ','+_capture+','+_rightAngle+','+_timeStay+','+_user_id+'" />');
    } else {
      $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes[]" value ="'+ lat + ',' + lgn +',' + _height + ','+_capture+','+_rightAngle+','+_timeStay+','+_user_id+'" />');
    }
    var _delete = '<a class="jsDeleteNode" onClick="deleteMarker('+_id+');"><img src="/assets/img/icTrash.png" alt="" style="float: right;"/></a>';
    if(title == 'H') {
        _delete = '';
    }

    var container = document.createElement("FORM");
    container.action = '/admin/flight/'+flight + '/' + _user_id + '/realtime-edit/'+_id;
    container.method = "POST";
    container.setAttribute("id", "frmEditNode");

    var titleNode = document.createElement("DIV");
    titleNode.setAttribute("class", "form-group");
    titleNode.innerHTML = '<h4 class="w-90p">ポイント '+title+'</h4>' + '<hr>';

    var latGroup = document.createElement("DIV");
    latGroup.setAttribute("class", "form-group");
    latGroup.innerHTML =   '<label class="col-md-3 labelForInput">経度: </label>' +
                            '<div class="input-group col-md-8">' +
                                '<input type="number" step="0.0000001" class="height-form-control" name ="lat" min="-90" max="90" id="lat" placeholder="0" value="'+lat+'">' +
                                '<span class="input-group-addon" style="width: 0;">°</span>' +
                            '</div>';

    var lngGroup = document.createElement("DIV");
    lngGroup.setAttribute("class", "form-group");
    lngGroup.innerHTML =    '<label class="col-md-3 labelForInput">緯度: </label>' +
                            '<div class="input-group col-md-8">' +
                                '<input type="number" step="0.0000001" class="height-form-control" name ="lgn" min="-180" max="180" id="lgn" placeholder="0" value="'+lgn+'">' +
                                '<span class="input-group-addon" style="width: 0;">°</span>' +
                            '</div>';

    var heightGroup = document.createElement("DIV");
    heightGroup.setAttribute("class", "form-group");
    heightGroup.innerHTML = '<label class="col-md-3 labelForInput">高度</label>' +
                            '<div class="input-group col-md-8">' +
                                '<input type="number" class="height-form-control" name ="height" min="0" max="150" id="height" placeholder="0" value="'+_height+'">' +
                                '<span class="input-group-addon" style="width: 0;">m</span>' +
                            '</div>';

    var timeStayGroup = document.createElement("DIV");
    timeStayGroup.setAttribute("class", "form-group");
    timeStayGroup.innerHTML = '<label class="col-md-3 labelForInput">滞空</label>' +
                            '<div class="input-group col-md-8">' +
                                '<input type="number" class="height-form-control" name ="timeStay" min="0" max="60" id="timeStay" placeholder="0" value="'+_timeStay+'">' +
                                '<span class="input-group-addon" style="width: 0;">s</span>' +
                            '</div>';

    var captureGroup = document.createElement("DIV");
    captureGroup.setAttribute("class", "form-group");
    captureGroup.innerHTML = '<label class="col-md-3">撮影: </label><strong class="onOffSwitcher">ON <span class="wrapCaptureImage capture_'+_capture_convert+'" onclick="captureImageSwitcher(this)"><img src="/assets/img/'+_capture_convert+'_switcher.png" class="captureImageSwitcher"/><input type="hidden" name="capture" value="'+_capture+'"/></span> OFF</strong>';

    var rightAngleGroup = document.createElement("DIV");
    rightAngleGroup.setAttribute("class", "form-group");
    rightAngleGroup.innerHTML = '<label class="col-md-3">直角<img class="icon_right_angle" src="/assets/img/arrow.png">: </label><strong class="onOffSwitcher">ON <span class="wrapCaptureImage capture_'+_rightAngle_convert+'" onclick="rightAngleSwitcher(this)"><img src="/assets/img/'+_rightAngle_convert+'_switcher.png" class="captureImageSwitcher"/><input type="hidden" name="rightAngle" value="'+_rightAngle+'"/></span> OFF</strong>';

    var buttonGroup = document.createElement("DIV");
    buttonGroup.setAttribute("class", "form-group center");
    buttonGroup.innerHTML = '<a href="javascript:;" class="jsClose"  onclick="return checkform();"><img src="/assets/img/save.png" alt="キャンセル" /></a> ' + '<a href="javascript:;" class="jsClose"  onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>';

    container.appendChild(titleNode);
    container.appendChild(latGroup);
    container.appendChild(lngGroup);
    container.appendChild(heightGroup);
    container.appendChild(timeStayGroup);
    container.appendChild(captureGroup);
    container.appendChild(rightAngleGroup);
    container.appendChild(buttonGroup);

    var infowindow = new google.maps.InfoWindow({
        content: container,
        maxWidth: 500
    });

    google.maps.event.addListener(marker, 'click', function() {
        closeAllInfoWindows();
        infowindow.open(map,marker);
        infoWindows.push(infowindow);
    });

    /*Move marker*/
    // google.maps.event.addListener(marker, 'dragend', function() {
    //     if(is_home) {
    //         title = pos;
    //     }else{
    //         title = pos;
    //     }
    //     if(title == 'H') {
    //         var number = 0;
    //         polylines[0].setMap(null);
    //     }else{
    //         var number = parseInt(pos);
    //         for(i= number - 1; i <= number ; i++) {
    //           if(polylines[i] != null)
    //               polylines[i].setMap(null);
    //       }
    //     }

    //     var lat = marker.position.lat().toFixed(7);
    //     var lgn = marker.position.lng().toFixed(7);
    //     marker_2 = lat+','+lgn;

    //     marker_1 = gmarkers[number - 1];
    //     gmarkers[title] = marker_2;
    //     marker_3 = gmarkers[number + 1];
    //     moveMarkerDrawLine(marker_1, marker_2, marker_3, number);
    //     $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height);
    //     if(init){
    //         $('.jsSave').trigger('click');
    //     }
    // });

    /*Move marker*/
    google.maps.event.addListener(marker, 'dragend', function() {
        var infoNodeUpdate = $('#jsNodes #node[attr-point-id="'+_id+'"]').attr("value");

        if(is_home) {
            title = pos;
        }else{
            title = pos;
        }
        if(title == 'H') {
            var number = 0;
            polylines[0].setMap(null);
        }else{
            var number = parseInt(pos);
            for(i= number - 1; i <= number ; i++) {
              if(polylines[i] != null)
                  polylines[i].setMap(null);
          }
        }

        var lat = marker.position.lat().toFixed(7);
        var lgn = marker.position.lng().toFixed(7);

        var container = document.createElement("FORM");
        container.action = '/admin/flight/'+flight+ '/' + _user_id + '/edit/'+_id;
        container.method = "POST";
        container.setAttribute("id", "frmEditNode");
        var latGroup = document.createElement("DIV");
        latGroup.setAttribute("class", "form-group");
        latGroup.innerHTML =   '<label class="col-md-3 labelForInput">経度: </label>' +
                                '<div class="input-group col-md-8">' +
                                    '<input type="number" step="0.0000001" class="height-form-control" name ="lat" min="-90" max="90" id="lat" placeholder="0" value="'+lat+'">' +
                                    '<span class="input-group-addon" style="width: 0;">°</span>' +
                                '</div>';

        var lngGroup = document.createElement("DIV");
        lngGroup.setAttribute("class", "form-group");
        lngGroup.innerHTML =    '<label class="col-md-3 labelForInput">緯度: </label>' +
                                '<div class="input-group col-md-8">' +
                                    '<input type="number" step="0.0000001" class="height-form-control" name ="lgn" min="-180" max="180" id="lgn" placeholder="0" value="'+lgn+'">' +
                                    '<span class="input-group-addon" style="width: 0;">°</span>' +
                                '</div>';

        container.appendChild(titleNode);
        container.appendChild(latGroup);
        container.appendChild(lngGroup);
        container.appendChild(heightGroup);
        container.appendChild(timeStayGroup);
        container.appendChild(captureGroup);
        container.appendChild(rightAngleGroup);
        container.appendChild(buttonGroup);

        infowindow.setContent(container);

        args_marker_2 = lat+','+lgn;

        if (_subuser) {
          args_marker_1 = gmarkers_2[number - 1];
          gmarkers_2[title] = args_marker_2;
          args_marker_3 = gmarkers_2[number + 1];
        } else {
          args_marker_1 = gmarkers_1[number - 1];
          gmarkers_1[title] = args_marker_2;
          args_marker_3 = gmarkers_1[number + 1];
        }
        moveMarkerDrawLine(args_marker_1, args_marker_2, args_marker_3, number);
        $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height+','+_capture+','+_rightAngle+','+_timeStay+','+_user_id);
        if(init || type == 'news'){
            $('.jsSave').trigger('click');
        }
    });

    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}

function ajaxLoadNodes(object, is_subuser) {
    var _is_subuser = is_subuser;
    var _url = '/api/flight/log/'+flight;
    $.ajax({
        url: _url,
        type: "GET",
        dataType: "JSON",
        success: function(res, status, jqXHR){
            var newLogNodes_1 = [];
            var newLogNodes_2 = [];
            if(res.status) {
                if(res.data.flight.lastFlight!=null) {
                  // if(jqXHR.data != null && jqXHR.data.flight.lastFlight!=null) processAlertForRealtime(jqXHR);
                    // console.log("★★★★★" + res.data.flight.lastFlight.length + "★★★★★");
                    var LogIndex = res.data.flight.lastFlight.length - 1;
                    _is_subuser = res.data.flight.lastFlight[LogIndex].user_id === 2 ? true : false;
                    // console.log("★★★★★" + _is_subuser + "★★★★★");
                    res.data.flight.lastFlight.forEach(function(log, id, array) {
                      _currentTime = new Date().getTime();
                      _lastestTimeLog = new Date(log.fstarted_at.replace(/-/g, "/")).getTime();
                      var _timeDiff = (_currentTime - _lastestTimeLog);
                      if(_timeDiff > 300000) {
                        // console.log("Too old.");
                        return;
                      }
                      if (id !== array.length - 1){
                        if (log.user_id == 1) {
                          var nodeExist_1 = logNodes_1.indexOf(log.lat+','+log.lng);
                          if (nodeExist_1 > -1) {
                          }else{
                              // logNodes_1.push(log.lat+','+log.lng);
                              newLogNodes_1.push(log.lat+','+log.lng);
                          }
                        } else {
                          var nodeExist_2 = logNodes_2.indexOf(log.lat+','+log.lng);
                          if (nodeExist_2 > -1) {
                          }else{
                              // logNodes_2.push(log.lat+','+log.lng);
                              newLogNodes_2.push(log.lat+','+log.lng);
                          }
                        }
                      }
                    });
                    flightInfo = res.data.flight.name;
                    if(res.data.flight.lastFlight[res.data.flight.lastFlight.length-1].stream_key !== null && res.data.flight.lastFlight[res.data.flight.lastFlight.length-1].stream_key !== stream_key) {
                      stream_key = res.data.flight.lastFlight[res.data.flight.lastFlight.length-1].stream_key;
                      stream_url = "{{getenv('URL_STREAM')}}"+stream_key;;
                    }
                    var color_switch = false;
                    // if (!color_switch) {
                      color_switch = false;
                      loadLogNodes(newLogNodes_1, object, color_switch);
                    // } else {
                      color_switch = true;
                      loadLogNodes(newLogNodes_2, object, color_switch);
                    // }
                }
                if (!_is_subuser && currentNode_1 == null) {
                          if(res.data.flight.lastFlight!=null && res.data.flight.lastFlight[res.data.flight.lastFlight.length - 1].plan_status == 1){
                                currentNode_1 = res.data.flight.lastFlight[res.data.flight.lastFlight.length-1];
                                var latlng_start = new google.maps.LatLng(currentNode_1.lat,currentNode_1.lng);
                                console.log("MAINのaddCurrentNodeを呼びます。");
                                addCurrentNode(latlng_start,map, _is_subuser);
                                var breakForEachGeoFilterZone = {};
                                //Get Current Point From Logs
                                var droneChart = $('#chartContainer').highcharts();
                                droneChart.series[1].data.forEach(function(chart, key){
                                    if (chart.key == currentNode_1.current_point) {
                                        chart.update({
                                            marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                        });
                                    } else {
                                        chart.update({
                                            marker: {symbol: 'url(/assets/img/blank.png)'}
                                        });
                                    }

                                });
                                //End Process Point with second board
                                try {
                                    geoFilterZones.forEach(function(object, key){
                                        var statusInsideGeoFilter = google.maps.geometry.poly.containsLocation(latlng_start, object);
                                        if(statusInsideGeoFilter == true){
                                            throw breakForEachGeoFilterZone;
                                        }
                                        if(key === (geoFilterZones.length-1) && statusInsideGeoFilter == false && goHomeCalled == false) {
                                            //Call Go Home API
                                            // var _url = '/api/flight/gohome';
                                            // $.ajax({
                                            //     type: "POST",
                                            //     url: _url,
                                            //     data: {
                                            //     id: flight,
                                            //     go_home: 1
                                            // },
                                            // success: function(res){
                                            //     goHomeCalled = true;
                                            // }
                                          // });
                                        }
                                    });
                                } catch (e) {
                                    if (e !== breakForEachGeoFilterZone) throw e;
                                }
                            }
                } else if (!_is_subuser && currentNode_1 != null) {
                    if(res.data.flight.lastFlight!=null){
                        currentNode_1 = res.data.flight.lastFlight[res.data.flight.lastFlight.length-1];
                        var latlng_start = new google.maps.LatLng(currentNode_1.lat,currentNode_1.lng);
                        currentNodeMarker_1.setPosition( latlng_start );
                        var breakForEachGeoFilterZone = {};
                        //Get Current Point From Logs
                        var droneChart = $('#chartContainer').highcharts();
                        droneChart.series[1].data.forEach(function(chart, key){
                            if (chart.key == currentNode_1.current_point) {
                                chart.update({
                                    marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                });
                            } else {
                                chart.update({
                                    marker: {symbol: 'url(/assets/img/blank.png)'}
                                });
                            }

                        });
                        //End Process Point with second board
                        try {
                            geoFilterZones.forEach(function(object, key){
                                var statusInsideGeoFilter = google.maps.geometry.poly.containsLocation(latlng_start, object);
                                if(statusInsideGeoFilter == true){
                                    throw breakForEachGeoFilterZone;
                                }
                                if(key === (geoFilterZones.length-1) && statusInsideGeoFilter == false && goHomeCalled == false) {
                                    //Call Go Home API
                                  //   var _url = '/api/flight/gohome';
                                  //   $.ajax({
                                  //       type: "POST",
                                  //       url: _url,
                                  //       data: {
                                  //       id: flight,
                                  //       go_home: 1
                                  //   },
                                  //   success: function(res){
                                  //       goHomeCalled = true;
                                  //   }
                                  // });
                                }
                            });
                        } catch (e) {
                          if (e !== breakForEachGeoFilterZone) throw e;
                        }
                    }
                }

                if (_is_subuser && currentNode_2 == null) {
                    if(res.data.flight.lastFlight!=null && res.data.flight.lastFlight[res.data.flight.lastFlight.length - 1].plan_status == 1){
                        currentNode_2 = res.data.flight.lastFlight[res.data.flight.lastFlight.length-1];
                        var latlng_start = new google.maps.LatLng(currentNode_2.lat,currentNode_2.lng);
                        console.log("SUBのaddCurrentNodeを呼びます。");
                        addCurrentNode(latlng_start,map,_is_subuser);
                        var breakForEachGeoFilterZone = {};
                        //Get Current Point From Logs
                        var droneChart = $('#chartContainer').highcharts();
                        droneChart.series[1].data.forEach(function(chart, key){
                            if (chart.key == currentNode_2.current_point) {
                                chart.update({
                                    marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                });
                            } else {
                                chart.update({
                                    marker: {symbol: 'url(/assets/img/blank.png)'}
                                });
                            }

                        });
                        //End Process Point with second board
                        try {
                            geoFilterZones.forEach(function(object, key){
                                var statusInsideGeoFilter = google.maps.geometry.poly.containsLocation(latlng_start, object);
                                if(statusInsideGeoFilter == true){
                                    throw breakForEachGeoFilterZone;
                                }
                                if(key === (geoFilterZones.length-1) && statusInsideGeoFilter == false && goHomeCalled == false) {
                                    //Call Go Home API
                                    // var _url = '/api/flight/gohome';
                                    // $.ajax({
                                    //     type: "POST",
                                    //     url: _url,
                                    //     data: {
                                    //     id: flight,
                                    //     go_home: 1
                                    // },
                                    // success: function(res){
                                    //     goHomeCalled = true;
                                    // }
                                  // });
                                }
                            });
                        } catch (e) {
                            if (e !== breakForEachGeoFilterZone) throw e;
                        }
                    }
                } else if (_is_subuser && currentNode_2 != null) {
                      if(res.data.flight.lastFlight!=null){
                          currentNode_2 = res.data.flight.lastFlight[res.data.flight.lastFlight.length-1];
                          var latlng_start = new google.maps.LatLng(currentNode_2.lat,currentNode_2.lng);
                          currentNodeMarker_2.setPosition( latlng_start );
                          var breakForEachGeoFilterZone = {};
                          //Get Current Point From Logs
                          var droneChart = $('#chartContainer').highcharts();
                          droneChart.series[1].data.forEach(function(chart, key){
                              if (chart.key == currentNode_2.current_point) {
                                  chart.update({
                                      marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                  });
                              } else {
                                  chart.update({
                                      marker: {symbol: 'url(/assets/img/blank.png)'}
                                  });
                              }

                          });
                          //End Process Point with second board
                          try {
                              geoFilterZones.forEach(function(object, key){
                                  var statusInsideGeoFilter = google.maps.geometry.poly.containsLocation(latlng_start, object);
                                  if(statusInsideGeoFilter == true){
                                      throw breakForEachGeoFilterZone;
                                  }
                                  if(key === (geoFilterZones.length-1) && statusInsideGeoFilter == false && goHomeCalled == false) {
                                      //Call Go Home API
                                    //   var _url = '/api/flight/gohome';
                                    //   $.ajax({
                                    //       type: "POST",
                                    //       url: _url,
                                    //       data: {
                                    //       id: flight,
                                    //       go_home: 1
                                    //   },
                                    //   success: function(res){
                                    //       goHomeCalled = true;
                                    //   }
                                    // });
                                  }
                              });
                          } catch (e) {
                            if (e !== breakForEachGeoFilterZone) throw e;
                          }
                      }
                  }
            }else {
            }
        },
    }).always(function(jqXHR, textStatus) {
        // if(jqXHR.data != null && jqXHR.data.flight.lastFlight!=null) processAlertForRealtime(jqXHR);
    });
}

// Load Logs of Drone
function loadLogNodes(logNodes, object, is_subuser) {
    var length = logNodes.length;
    if(length > 0){
        for (var i=0; i < length; i++){
            j = i + 1;
            var start = logNodes[i];
            var end = logNodes[j];

            /*Start*/
            var startCoords = start.split(",");
            var lat_start = startCoords[0];
            var lgn_start = startCoords[1];
            var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
            var obj = object[i];

            center = 0;
            addLogNode(latlng_start,map,obj,center,i, is_subuser);

        }
        map.fitBounds(latLngBounds);
    }
}

function addLogNode(latlng,map,obj,center,pos, is_subuser) {
    // var title = (++inc).toString();
    var color = "";
    if (is_subuser) {
      // console.log("SUBのaddLogNodeが呼ばれた");
      marker_2 = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:false,
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
          scale: 5,
          strokeColor: '#fff',
          strokeWeight: 2,
          fillColor: '#0021ff',
          fillOpacity: 1
        },
        zIndex: 1,
        optimized: false
      });
      marker_2.setMap(map);
      logsNodeArray_2.push(marker_2);
    } else {
      // console.log("フツーのaddLogNodeが呼ばれた");
      marker_1 = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable:false,
          icon: {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 5,
              strokeColor: '#fff',
              strokeWeight: 2,
              fillColor: '#ffde00',
              fillOpacity: 1
          },
          zIndex: 1,
          optimized: false
      });
      marker_1.setMap(map);
      logsNodeArray_1.push(marker_1);

    }

    // var lat = latlng.lat().toFixed(7) - 0.000050;
    // var lgn = latlng.lng().toFixed(7);
    // if (is_subuser) {
      // marker_2.setMap(map);
      // logsNodeArray_2.push(marker_2);
    // } else {
      // marker_1.setMap(map);
      // logsNodeArray_1.push(marker_1);
    // }
}

function addCurrentNode(latlng,map,is_subuser) {
    // var title = (++inc).toString();
    // var color = "#ffde00";
    var drone_img = '';
    var currentNode = '';
    var image;

    if (is_subuser) {
      drone_img = '/assets/img/drone_v2_blue.png';
      image = {
        url: drone_img,
        anchor: new google.maps.Point(19,19),
        origin: new google.maps.Point(0,0),
        size: new google.maps.Size(40, 40),
        scale: 1,
      };
      currentNode = currentNode_2;
      // console.log("DRONE: SUBのaddCurrentNodeが呼ばれた");
      currentNodeMarker_2 = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable:false,
          icon: image,
          zIndex: 4,
          optimized: false
      });
      currentNodeMarker_2.setMap(map);
      logsNodeArray_2.push(currentNodeMarker_2);

    } else {
      drone_img = '/assets/img/drone_v2.png';
      image = {
        url: drone_img,
        anchor: new google.maps.Point(19,19),
        origin: new google.maps.Point(0,0),
        size: new google.maps.Size(40, 40),
        scale: 1,
      };
      currentNode = currentNode_1;
      // console.log("DRONE: フツーのaddCurrentNodeが呼ばれた");
      currentNodeMarker_1 = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:false,
        icon: image,
        zIndex: 4,
        optimized: false
      });
      currentNodeMarker_1.setMap(map);
      logsNodeArray_1.push(currentNodeMarker_1);
    }

    // var lat = latlng.lat().toFixed(7) - 0.000075;
    // var lng = latlng.lng().toFixed(7);
    // var latlng = new google.maps.LatLng(lat,lng);



    var html = '<form id="frmCurrentNode"  method="POST" action="#" >' +
            '<div class="form-group">' +
                '<h4 class="w-90p">'+flightInfo+'</h4>' +
                '<hr>'+
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">経度: </label><strong>'+ currentNode.lat + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">緯度: </label><strong>' + currentNode.lng + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">高度:</label><strong>' + currentNode.height + ' m</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">バッテリー:</label><strong>' + currentNode.battery + ' %</strong>' +
            '</div>' +
            '<div class="form-group center">' +
                '<a href="javascript:;" class="jsClose" onclick="document.getElementById(\'frmEditNode\').submit();"><img src="/assets/img/save.png" alt="保存" /></a> ' +
                '<a href="javascript:;" class="jsClose" onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>' +
            '</div>' +
        '</form>';

    var infowindow = new google.maps.InfoWindow({
        content: html,
        maxWidth: 500
    });

    if (is_subuser) {
      google.maps.event.addListener(currentNodeMarker_2, 'click', function() {
          closeAllInfoWindows();
          infowindow.open(map,currentNodeMarker_2);
          infoWindows.push(infowindow);
      });
    } else {
      google.maps.event.addListener(currentNodeMarker_1, 'click', function() {
          closeAllInfoWindows();
          infowindow.open(map,currentNodeMarker_1);
          infoWindows.push(infowindow);
      });
    }
    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}

function pinSymbol(_color) {
    return {
       path: google.maps.SymbolPath.CIRCLE,
        scale: 10,
        strokeColor: _color,
        fillColor: _color,
        fillOpacity: 1
    };
}
function closeAllInfoWindows() {
    for (var i=0;i<infoWindows.length;i++) {
      infoWindows[i].close();
    }
}

function deleteMarker(id){
    $('#jsNodes #node[attr-point-id="'+id+'"]').remove();
    $('.jsSave').trigger('click');
}

function isCategoryA(category_code) {
    return category_code == 'A';
}

function isCategoryB(category_code) {
    return category_code == 'B';
}

function isCategoryCityLimit(category_code) {
    return category_code == 'city_limit';
}

function categoryName(category_code) {
    if(isCategoryA(category_code)){
        return category_a_name;
    }else if(isCategoryB(category_code)){
        return category_b_name;
    }else{
        return category_city_limit_name;
    }
}

function no_fly_zone(map, item, category) {
    color = '#f39c12';
    mile = 5; // 5 miles
    image = red_image;
    if(isCategoryB(category)) {
        color = '#2d89ef';
        mile = 1;
        image = blue_image;
    }

    var marker_latlng = new google.maps.LatLng(item[0], item[1]);
    var noFlyZone = new google.maps.Marker({
        position: marker_latlng,
        popup: true,
        icon: image,
        map: map
    });
    if(isCategoryCityLimit(category)){
        noFlyZone.setVisible(false)
    }

    text = item[2] + " (" + categoryName(category) + ")";
    if(isCategoryA(category)){
        text += "<br/><small>No Fly Zone<br/>Height Restriction Zone</small>"
    }else{
        text += "<br/><small>No Fly Zone</small>"
    }
    var infowindow = new google.maps.InfoWindow({
        content: text
    });

    if(!isCategoryCityLimit(category)){
        var options = {
          strokeColor: color,
          strokeOpacity: 0.6,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.4,
          map: map,
          center: noFlyZone.position,
          radius: mile * 1609.344
        };
        // Add the circle for this city to the map.
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }

    if(isCategoryA(category)){    // Category A
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: noFlyZone.position,
            radius: 1.5 * 1609.344
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }else if(isCategoryCityLimit(category)){
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: marker_latlng,
            radius: item[5] * 1000
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
        google.maps.event.addListener(cityCircle, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    if(!isCategoryCityLimit(category)){
        google.maps.event.addListener(noFlyZone, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}
function updateGoHome() {
    var $goHome = $("#goHome");
    $goHome.append("<span class='inner'>指示しました。</span>").children(".jsClose").addClass("disabled");
    setTimeout(function() {
      $('#goHome .inner').fadeOut();
    }, 10000);
    var _url = '/api/flight/gohome';
    $.ajax({
        type: "POST",
        url: _url,
        data: { "id": flight, "go_home": 1 },
        success: function (data) {
        },
        error: function (data) {
        }
    });
}
function updatePlanStatus() {
    var $startFlight = $("#startFlight");
    $startFlight.append("<span class='inner'>指示しました。</span>").children(".jsClose").addClass("disabled");
    setTimeout(function() {
      $('#startFlight .inner').fadeOut();
    }, 10000);
    var _url = '/api/flight/startflight';
    $.ajax({
        type: "POST",
        url: _url,
        data: { "id": flight, "plan_status": 1 },
        success: function (data) {
        },
        error: function (data) {
        }
    });
}
function _respondToMapChange(map){
    // var newInitialBounds = map.getBounds();
    // if(initialBounds == null || initialBounds !== newInitialBounds){
    //     initialBounds = newInitialBounds;
    //     var _url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds='+initialBounds.getNorthEast().lat()+','+initialBounds.getSouthWest().lat()+','+initialBounds.getSouthWest().lng()+','+initialBounds.getNorthEast().lng()+'&faa=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=7200&gliders=1&stats=1';
    //     $.ajax({
    //         url: _url,
    //         type: "GET",
    //         dataType: "JSON",
    //         success: function(res, textStatus, xhr){
    //             clearMarkers();
    //             jQuery.each(res, function(key, val) {
    //                 if(key != "full_count" && key != "version" && key != "stats"){
    //                     addFlight(val[1], val[2], val[3], map);
    //                 }
    //             });
    //         }
    //     });
    // }
    // weatherLoaded = false;
    var newInitialCenter = map.getCenter();

    if(initialCenter == null || initialCenter !== newInitialCenter){
        initialCenter = newInitialCenter;

        if(!weatherIconClicked){
            $('#weatherIcon_1').click(function(){
                loadWeather(initialCenter.lat(),initialCenter.lng());
                weatherLoaded = true;
                weatherIconClicked = true;
            });
        }else{
            if(weatherLoaded){
                if ( $("#reload_btn").hasClass("reloadHidden") ) {
                    $("#reload_btn").removeClass("reloadHidden").addClass("reloadShow");
                    weatherLoaded = false;
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }else{
                if( $("#reload_btn").hasClass("reloadShow") ) {
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }
        }

    }
}

function addFlight(lat,lng,rotate,name,speed,map) {
    var color = "#f7255c";
    var latlng = new google.maps.LatLng(lat,lng);
    var image = {
        path: "M24.8,15.2L14,7.2c0.1-1.2,0.1-4,0.1-4.5c-0.4-4.3-3.1-2.4-3.1-0.5c0,0.5-0.1,3.2-0.1,5.1   L0.2,15.4l0,2.6l11-5l0.4,8l-3.2,2.3l0,1.7l4.2-1.2l0,0l4.2,1.1l-0.1-1.7L13.5,21l0.3-8l11.1,4.8L24.8,15.2L24.8,15.2z",
        fillColor: '#f0c61d',
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 1,
        anchor: new google.maps.Point(0,0),
        size: new google.maps.Size(32, 32),
        scale: 1,
        rotation: rotate,
    };
    var aircraft = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:false,
        icon: image,
        title: name,
        zIndex: 1,
    });
    aircraft.setMap(map);
    aircrafts.push(aircraft);

    var speedConvert = (parseFloat(speed))/3600;

    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,1);
    }, 1000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,1.5);
    }, 1500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,2);
    }, 2000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,2.5);
    }, 2500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,3);
    }, 3000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,3.5);
    }, 3500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,4);
    }, 4000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,4.5);
    }, 4500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,5);
    }, 5000);
}
function moveFlightAfterHalfSecond(marker,lat,lng,speed,rotate,time){
    with (Math) {
        if (marker != undefined) {
            lat = rad(lat);
            lng = rad(lng);
            rotate = rad(rotate);
            var speedConvert = (parseFloat(speed))/3600;
            var distance = parseFloat((speedConvert*time)/radiusEarth);
            var newLat = asin(sin(lat)*cos(distance) + cos(lat)*sin(distance)*cos(rotate));
            var newLng = lng + atan2(sin(rotate)*sin(distance)*cos(lat), cos(distance)-sin(lat)*sin(newLat));
            marker.setPosition(new google.maps.LatLng( deg(newLat),deg(newLng) ));
        }
    }
}
function rad(dg) {
    return (dg* Math.PI / 180);
}

function deg(rd) {
    return (rd* 180 / Math.PI);
}
function setMapOnAll(map) {
    for (var i = 0; i < aircrafts.length; i++) {
        aircrafts[i].setMap(map);
    }
}
function deleteOldAirCraft() {
    clearMarkers();
    aircrafts = [];
}
function clearMarkers() {
    setMapOnAll(null);
}
function noFlyZoneSwicher(_this){
    var _this = $(_this);
    if(_this.hasClass('noFlyZone_on')) {
        _this.addClass('noFlyZone_off').removeClass('noFlyZone_on');
        _this.find('img').attr('src','/assets/img/off_switcher2.png');
        $('#weather-content #noFlyZoneSwicher').addClass('geoFilter_off').removeClass('geoFilter_on');
        $('#weather-content #noFlyZoneSwicher').find('img').attr('src','/assets/img/off_switcher2.png');
        setMapClearNoFlyZone(null);
        noFlyZones = [];
    }else if (_this.hasClass('noFlyZone_off')){
        _this.addClass('noFlyZone_on').removeClass('noFlyZone_off');
        _this.find('img').attr('src','/assets/img/on_switcher2.png');
        $('#weather-content #noFlyZoneSwicher').addClass('noFlyZone_on').removeClass('noFlyZone_off');
        $('#weather-content #noFlyZoneSwicher').find('img').attr('src','/assets/img/on_switcher2.png');
        drawNoFlyZone();
    }
};

/**
 * Draw exist geo filter zones
 * This function will be called when Geo Filter Switch ON
 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
 */
function geoFilterSwicher(_this){
    var _this = $(_this);
    if(_this.hasClass('geoFilter_on')) {
        _this.addClass('geoFilter_off').removeClass('geoFilter_on');
        _this.find('img').attr('src','/assets/img/off_switcher2.png');
        $('#weather-content #geoFilterSwicher').addClass('geoFilter_off').removeClass('geoFilter_on');
        $('#weather-content #geoFilterSwicher').find('img').attr('src','/assets/img/off_switcher2.png');
        setMapClearGeoFilterZone(null);
    }else if (_this.hasClass('geoFilter_off')){
        _this.addClass('geoFilter_on').removeClass('geoFilter_off');
        _this.find('img').attr('src','/assets/img/on_switcher2.png');
        $('#weather-content #geoFilterSwicher').addClass('geoFilter_on').removeClass('geoFilter_off');
        $('#weather-content #geoFilterSwicher').find('img').attr('src','/assets/img/on_switcher2.png');
        setMapClearGeoFilterZone(map);
    }
};

function drawNoFlyZone(){
    $.each(mc_category_a, function(index, item) {
        no_fly_zone(map, item, 'A')
    });

    $.each(mc_category_b, function(index, item) {
        no_fly_zone(map, item, 'B')
    });

    $.each(mc_category_city_limit, function(index, item) {
        no_fly_zone(map, item, 'city_limit');
    });
}
function drawGeoFilterZone(){
    // Hard Code Fly Area
    var flyAreaWrapperPath = [
        {lat: 26.5202597, lng: 128.0207795},
        {lat: 26.5197269, lng: 128.0210423},
        {lat: 26.5199621, lng: 128.0213937},
        {lat: 26.5199405, lng: 128.0216378},
        {lat: 26.5202837, lng: 128.0217492},
        {lat: 26.5205285, lng: 128.0216405},
        {lat: 26.5204013, lng: 128.0211885},
        {lat: 26.5202597, lng: 128.0207795}
    ];
    var flyAreaWrapper = new google.maps.Polygon({
        paths: flyAreaWrapperPath,
        strokeColor: '#052eff',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#052eff',
        fillOpacity: 0.2
    });
    geoFilterZones.push(flyAreaWrapper);
    var lineSymbol = {
        path: 'M 0,-1 0,1',
        strokeColor: '#052eff',
        strokeOpacity: 1,
        fillColor: '#052eff',
        fillOpacity: 1,
        scale: 4
    };
    var line = new google.maps.Polyline({
        path: flyAreaWrapperPath,
        strokeOpacity: 0,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '20px'
        }],
        map: map
    });
    geoFilterZones.push(line);
    flyAreaWrapper.setMap(map);
}
function setMapClearNoFlyZone(map) {
    for (var i = 0; i < noFlyZones.length; i++) {
        noFlyZones[i].setMap(map);
    }
}
function setMapClearGeoFilterZone(map) {
    for (var i = 0; i < geoFilterZones.length; i++) {
        geoFilterZones[i].setMap(map);
    }
}
function loadWeather(c_lat,c_long) {
    var _url = 'http://52.43.150.151/assets/getWeather.php?lat='+c_lat+'&lng='+c_long;
    if(!weatherLoaded){
        $.ajax({
            url: _url,
            type: "GET",
            dataType: "JSON",
            success: function(res, textStatus, xhr){
                if(xhr.status == 200) {
                    $('.popover.fade.bottom.in').css("backgroundImage","none");
                    $('.popover.fade.bottom.in').css("backgroundColor","#fff");
                    var weatherDate = res.precipitation.time.split(' ');
                    var weatherDateConvert = weatherDate[1].split(':');
                    var endPrecipitationTime = parseInt(weatherDateConvert[0])+6;
                    var wind_direction_string_jp;

                    if(endPrecipitationTime >= 24) endPrecipitationTime = '0'+parseInt(endPrecipitationTime-24);

                    switch (res.forecasts.wind_direction_string) {
                        case "east":
                            wind_direction_string_jp = "東";
                            break;
                        case "east-northeast":
                            wind_direction_string_jp = "東北東";
                            break;
                        case "northeast":
                            wind_direction_string_jp = "北東";
                            break;
                        case "north-northeast":
                            wind_direction_string_jp = "北北東";
                            break;
                        case "north":
                            wind_direction_string_jp = "北";
                            break;
                        case "north-northwest":
                            wind_direction_string_jp = "北北西";
                            break;
                        case "northwest":
                            wind_direction_string_jp = "北西";
                            break;
                        case "west-northwest":
                            wind_direction_string_jp = "西北西";
                            break;
                        case "west":
                            wind_direction_string_jp = "西";
                            break;
                        case "west-southwest":
                            wind_direction_string_jp = "西南西";
                            break;
                        case "souththwest":
                            wind_direction_string_jp = "南西";
                            break;
                        case "south-southwest":
                            wind_direction_string_jp = "南南西";
                            break;
                        case "south":
                            wind_direction_string_jp = "南";
                            break;
                        case "south-southeast":
                            wind_direction_string_jp = "南南東";
                            break;
                        case "southeast":
                            wind_direction_string_jp = "南東";
                            break;
                        case "east-southeast":
                            wind_direction_string_jp = "東南東";
                            break;
                        case "east":
                            wind_direction_string_jp = "東";
                    }

                    $('#addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                    $('#dateForecast').html(res.forecasts.time);
                    $('#nameWeather').html(res.weather.weather.name);
                    $('#humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                    $('#temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                    $('#precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                    $('#timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                    $('#windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                    $('#directStringForecast').html(wind_direction_string_jp);
                    $('#directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                    $('#weather-content #addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                    $('#weather-content #dateForecast').html(res.forecasts.time);
                    $('#weather-content #nameWeather').html(res.weather.weather.name);
                    $('#weather-content #humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                    $('#weather-content #temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                    $('#weather-content #precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                    $('#weather-content #timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                    $('#weather-content #windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                    $('#weather-content #directStringForecast').html(wind_direction_string_jp);
                    $('#weather-content #directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                    document.getElementById('weatherIcon_2').style.backgroundImage = "url('/assets/img/"+res.weather.weather.code+".png')";
                    $('#weather-content #weatherIcon_2').css("backgroundImage","url('/assets/img/"+res.weather.weather.code+".png')");

                    var windDirection = document.getElementById('windDirection');
                    windDirection.style.webkitTransform = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.mozTransform    = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.msTransform     = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.oTransform      = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.transform       = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';

                    $('#weather-content #windDirection').css("webkitTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("mozTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("msTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("oTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("transform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');

                    weatherLoaded = true;
                    $("#reload_btn").removeClass("reloadShow").addClass("reloadHidden");
                    $("#reload_btn").html('');
                    $("#weather-content #reload_btn").html('');
                }else {
                }
            }
        });
    }
}
function processAlertForRealtime(jqXHR){
    if (jqXHR.status == true) {
        currentTime = new Date().getTime();
        lastestTimeLog = new Date(jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].created_at.replace(/-/g, "/")).getTime();
        var timeDiff = (currentTime-lastestTimeLog);
        if(timeDiff > 300000){
            clearInterval(getLogInterval);
            clearFlightLogMakers(logsNodeArray_1);
            clearFlightLogMakers(logsNodeArray_2);
            var droneChart = $('#chartContainer').highcharts();
            droneChart.series[1].data.forEach(function(chart, key){
                chart.update({
                    marker: {symbol: 'url(/assets/img/blank.png)'}
                });
            });
        } else {
            // Case: Don't have any logs but plan_status = 1
            if (jqXHR.data.flight.lastFlight == null || jqXHR.data.flight.lastFlight.length == 0) {
                if (jqXHR.data.flight.plan_status == 1) {
                }
            } else {
                // Case: Plan Status !== 1 !==0
                if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status !== 1 && jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status !== 0) {
                    if (!modalAlertShowed) {
                        $('#alertModal').modal('show');
                        modalAlertShowed = true;
                    }
                }
                // Case: Plan Status == 0: Mean Landing
                else if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status == 0) {
                    var checkExistFlyingStatus;
                    loopCheckExistFlyingStatus: {
                        for (var k in jqXHR.data.flight.lastFlight) {
                            if (jqXHR.data.flight.lastFlight[k].plan_status == 1) {
                                $('#alertModal').modal('hide');
                                checkExistFlyingStatus = true;
                                break loopCheckExistFlyingStatus;
                            }
                        }
                    }
                    if (checkExistFlyingStatus !== true) {
                        if (!modalAlertShowed) $('#alertModal').modal('show');
                        clearInterval(getLogInterval);
                        timeIntervalGetLog = 10000;
                        getLogInterval = setInterval(function() {
                            modalAlertShowed = true;
                            ajaxLoadNodes(object_1);
                        }, timeIntervalGetLog);
                    } else {
                        clearInterval(getLogInterval);
                        clearFlightLogMakers(logsNodeArray_1);
                        clearFlightLogMakers(logsNodeArray_2);
                    }
                }
                // Case: Plan Status == 1: Mean Flying
                else if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status == 1) {
                    currentLogID = jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].id;
                    //  Case: Plan Status == 1: But don't have any new log
                    if (currentLogID == lastLogID && (currentTime - lastTimeGetLog) > 30000) {
                        if (!modalAlertShowed) $('#alertModal').modal('show');
                        clearInterval(getLogInterval);
                        timeIntervalGetLog = 10000;
                        getLogInterval = setInterval(function() {
                            modalAlertShowed = true;
                            ajaxLoadNodes(object_1);
                            ajaxLoadNodes(object_2);
                        }, timeIntervalGetLog);
                    }
                    //  Case: Plan Status == 1: Have new log
                    else if (currentLogID != lastLogID) {
                        $('#alertModal').modal('hide');
                        lastLogID = currentLogID;
                        lastTimeGetLog = currentTime;
                        clearInterval(getLogInterval);
                        timeIntervalGetLog = 3000;
                        getLogInterval = setInterval(function() {
                            ajaxLoadNodes(object_1);
                            ajaxLoadNodes(object_2);
                        }, timeIntervalGetLog);
                    }
                }
            }
        }
    } else if (jqXHR.status === 429) {
        clearInterval(getLogInterval);
        timeIntervalGetLog = 10000;
        getLogInterval = setInterval(function() {
            ajaxLoadNodes(object_1);
        }, timeIntervalGetLog);
    }
}

function captureImageSwitcher(_this){
    var _this = $(_this);
    if(_this.hasClass('capture_on')) {
        _this.addClass('capture_off').removeClass('capture_on');
        _this.find('img').attr('src','/assets/img/off_switcher.png');
        _this.find('input').attr('value','0');
    }else if (_this.hasClass('capture_off')){
        _this.addClass('capture_on').removeClass('capture_off');
        _this.find('img').attr('src','/assets/img/on_switcher.png');
        _this.find('input').attr('value','1');
    }
}

function rightAngleSwitcher(_this){
    var _this = $(_this);
    if(_this.hasClass('capture_on')) {
        _this.addClass('capture_off').removeClass('capture_on');
        _this.find('img').attr('src','/assets/img/off_switcher.png');
        _this.find('input').attr('value','0');
    }else if (_this.hasClass('capture_off')){
        _this.addClass('capture_on').removeClass('capture_off');
        _this.find('img').attr('src','/assets/img/on_switcher.png');
        _this.find('input').attr('value','1');
    }
}

function checkform(){
    var lat = document.getElementById('lat').value;
    var lng = document.getElementById('lgn').value;
    var height = document.getElementById('height').value;
    if(lat > 90 || lat < -90){
        $('#alertLatModal').modal('show');
    }
    else if(lng > 180 || lng < -180){
        $('#alertLongModal').modal('show');
    }
    else if(height < 0 || height > 150){
        $('#alertHeightModal').modal('show');
    }
    else{
        document.getElementById('frmEditNode').submit();
    }
}
function clearFlightLogMakers(objFlight) {
  console.log("clearFlightLogMakers~~~~~");
    var map = null;
    for (var i = 0; i < objFlight.length; i++) {
        objFlight[i].setMap(map);
    }
    objFlight = [];
}
$(document).ready(function(){
    // if (!document.getElementById('noFlyZone-checkbox').checked) {
    //     $('#noFlyZone-checkbox').bootstrapSwitch('toggleState', true, true);
    // }
    // if (!document.getElementById('geoFilterZone-checkbox').checked) {
    //     $('#geoFilterZone-checkbox').bootstrapSwitch('toggleState', true, true);
    // }
    $("[name='noFlyZone-checkbox']").bootstrapSwitch();
    $("[name='geoFilterZone-checkbox']").bootstrapSwitch();
    var weatherPopover = $("#drone_weather_toolbar a").popover({
        html : true,
        content: function() {
            return $("#weather-content").html();
        },
    });
    weatherPopover.on("hidden.bs.popover", function(e) {
        weatherIconClicked = false;
    });
    // Call API to load flights each 2s
    // setInterval(function(){
    //     bounds = map.getBounds();
    //     var _url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds='+bounds.getNorthEast().lat()+','+bounds.getSouthWest().lat()+','+bounds.getSouthWest().lng()+','+bounds.getNorthEast().lng()+'&faa=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=7200&gliders=1&stats=1';
    //     $.ajax({
    //         url: _url,
    //         type: "GET",
    //         dataType: "JSON",
    //         success: function(res, textStatus, xhr){
    //             clearMarkers();
    //             jQuery.each(res, function(key, val) {
    //                 if(key != "full_count" && key != "version" && key != "stats"){
    //                     addFlight(val[1], val[2], val[3], val[16], val[5], map);
    //                 }
    //             });
    //         }
    //     });
    // }, 10000);
});
