@extends('layouts.node')
@section('content')
<style type="text/css">
    #iw-container  .iw-title {
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 22px;
        font-weight: 400;
        padding: 10px;
        background-color: #48b5e9;
        color: white;
        margin: 1px;
        border-radius: 2px 2px 0 0; /* In accordance with the rounding of the default infowindow corners. */
    }
    .gm-style-iw {
        width: 300px !important;
        top: 20px !important;
        left: 0 !important;
        background-color: #fff;
        background-image: url("{{ URL::to('assets/img/popDrone.png') }}");
        background-repeat: no-repeat;
        background-position: 130px -50px; 
        background-size: 80% auto;
        box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
        border: 1px solid #bbb;
        border-radius: 5px !important;
        text-align: center;
    }
    #frmEditNode h4{
        margin-bottom: 5px;
        margin-top: 15px;
    }
    #frmEditNode hr{
        margin-top: 0px;
        border-top: 1px solid #25a6ef;
    }
    #frmEditNode h4.w-90p{
        text-align: left;
    }
    #frmEditNode label.col-md-4{
        text-align: left;
        padding-left: 0px;
    }
    #frmEditNode .form-group{
        text-align: left;
    }
    .labelForInput {
        top: 6px;
    }
    #height{
        border-top-left-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
    }
    #frmEditNode .input-group-addon{
        border-top-right-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
    }
    #frmEditNode .form-group.center{
        text-align: center;
    }
    .labels {
      color: white;      
      font-family: "Lucida Grande", "Arial", sans-serif;
      font-size: 14px;
      text-align: left;
      font-weight: bold;
    }
</style>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/admin/flight/list') }}"><img src="/assets/img/back.png" alt="back" /> BACK</a>
        </div>
        <input type="hidden" id="flight" value="{!! $flight->id !!}" />
        {!! Form::open(array('id'=>'jsNodes','class'=>'form-horizontal', 'files'=>true)) !!}
        <ul class="nav navbar-nav navbar-right">
            <li><a href="javascript:;" class="jsCreatNode p-10-3 unactive" class="btn btn-primary p-15"><img src="/assets/img/editPoint.png" alt="ポイント編集" /></a></li>
            <li><a href="javascript:;" class="p-10-3 jsSave" onclick="$(this).closest('form').submit();"><img src="/assets/img/save.png" alt="保存" /></a></li>
        </ul>
        @if(!empty($nodes) && count($nodes) > 0)
            @foreach($nodes as $node)
                <input type="hidden" attr-point-id="{!!  $node->id !!}" id="old_node" name="node[]" value ="{!! $node->id.','. $node->lat.','.$node->lng.','.$node->height !!}" />
            @endforeach
        @endif
        {!! Form::close() !!}
    </div>
</nav>
<!--Map -->
<div id="map"></div>
@endsection 
@section('scripts')
<script src="{{ URL::to('assets/js/markerwithlabel.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.jsCreatNode').on('click', function(){
            var _this = $(this);
            if(_this.hasClass('unactive')) {
                _this.addClass('active').removeClass('unactive');
                _this.find('img').attr('src','/assets/img/add-point.png');
            }
        });
    }); 
    var map;
    var latLngBounds;
    var gmarkers = [];
    var glocations = [];
    var marker = [];
    var total = 0;
    var id = [];
    var hight = [];
    var flight;
    var object =[];
    var infoWindows = [];
    var center;
    var c_lat = 35.65325;
    var c_long = 139.772049;
    var point;
    var polylines = [];
    var init = false;
  
    function initialize() {
        flight = $('#flight').val();
        var location = new Array();
        center = new google.maps.LatLng(c_lat, c_long);
        navigator.geolocation.getCurrentPosition(function(position) {
                var _lat = position.coords.latitude;
                var _long = position.coords.longitude;
                center = new google.maps.LatLng(_lat, _long);
            }, function() {
        });
        map = new google.maps.Map(document.getElementById('map'), {
            center: center,
            zoom: 14,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeId: 'satellite',
            disableDefaultUI: true,
        });     

        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zoomControlDiv);

        var markers = [];
        latLngBounds = new google.maps.LatLngBounds();
        $('#jsNodes #old_node').each (function () {
            var _val = $(this).val().split(",");
            markers.push(_val[1]+','+_val[2]);
            object.push(_val);
        });

        if(object.length >0 ) {
            total = -1;
            loadMarkers(markers);
        }else{
            total = 0;
            var center = 1;
            /*Get location*/
            navigator.geolocation.getCurrentPosition(function(position) {
                var _lat = position.coords.latitude;
                var _long = position.coords.longitude;
                var pos = {
                  lat: _lat,
                  lng: _long
                };
                map.setCenter(pos);
                var latlng = new google.maps.LatLng(_lat,_long);
                var center = 1;
                createNode(flight,_lat, _long, latlng, center);
                glocations.push(_lat+','+_long);
            }, function() {
                var _lat = c_lat;
                var _long = c_long;
                var latlng = new google.maps.LatLng(_lat,_long);
                createNode(flight,_lat, _long, latlng, center);
                glocations.push(_lat+','+_long);
            });
            init = true;
        }
        
        google.maps.event.addListener(map, 'click', function (e) {
            if(total < 10 && $('.jsCreatNode').hasClass('active')) {
                if(glocations.length == 0) {
                    var lat = e.latLng.lat().toFixed(6);
                    var lgn = e.latLng.lng().toFixed(6);
                    var latlng = new google.maps.LatLng(lat,lgn);  
                    total = total + 1;
                    var center = 1;
                    createNode(flight, lat, lgn, latlng, center);
                }else{
                    var lat = e.latLng.lat().toFixed(6);
                    var lgn = e.latLng.lng().toFixed(6);
                    var latlng = new google.maps.LatLng(lat,lgn);  
                    total = total + 1;
                    var center = 0;
                    createNode(flight, lat, lgn, latlng);                    

                    var length = glocations.length -1;                  
                    var endCoords = glocations[length].split(",");
                    var lat_end = endCoords[0];
                    var lgn_end = endCoords[1];

                    drawLine(lat, lgn, lat_end, lgn_end)
                }
                glocations.push(lat+','+lgn);
            }
        });
        
        map.setZoom(18);      
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    
    function ZoomControl(controlDiv, map) {  
        // Creating divs & styles for custom zoom control
        controlDiv.style.padding = '5px';

        // Set CSS for the control wrapper
        var controlWrapper = document.createElement('div');
        controlWrapper.style.borderWidth = '1px';
        controlWrapper.style.cursor = 'pointer';
        controlWrapper.style.textAlign = 'center';
        controlWrapper.style.width = '70px'; 
        controlWrapper.style.height = '150px';
        controlDiv.appendChild(controlWrapper);
        
        // Set CSS for the zoomIn
        var zoomInButton = document.createElement('div');
        zoomInButton.style.width = '50px'; 
        zoomInButton.style.height = '45px';
        /* Change this to be the .png image you want to use */
        zoomInButton.style.backgroundImage = 'url("/assets/img/icZoomOut.png")';
        controlWrapper.appendChild(zoomInButton);
          
        // Set CSS for the zoomOut
        var zoomOutButton = document.createElement('div');
        zoomOutButton.style.width = '50px'; 
        zoomOutButton.style.height = '45px';
        /* Change this to be the .png image you want to use */
        zoomOutButton.style.backgroundImage = 'url("/assets/img/icZoomin.png")';
        controlWrapper.appendChild(zoomOutButton);

        var locationButton = document.createElement('div');
        locationButton.style.width = '50px'; 
        locationButton.style.height = '60px';
        /* Change this to be the .png image you want to use */
        locationButton.style.backgroundImage = 'url("/assets/img/icLocation.png")';
        controlWrapper.appendChild(locationButton);

        // Setup the click event listener - zoomIn
        google.maps.event.addDomListener(zoomInButton, 'click', function() {
            map.setZoom(map.getZoom() + 1);
        });
          
        // Setup the click event listener - zoomOut
        google.maps.event.addDomListener(zoomOutButton, 'click', function() {
            map.setZoom(map.getZoom() - 1);
        });

        google.maps.event.addDomListener(locationButton, 'click', function() {
              map.setCenter(latLngBounds.getCenter());
              map.setZoom(18);
        });
    }

    
    function createNode(flight, _lat, _long, latlng,center) {
        var _url = '/admin/flight/'+flight+'/create?lat='+_lat+'&long='+_long;
        $.ajax({
            url: _url,
            type: "POST",
            dataType: "JSON",
            success: function(res){
                if(res.status) {
                    var str = res.data.id + ',' + res.data.lat + ','+ res.data.lng + ',' + res.data.height;
                    object.push(str.split(','));
                    var obj = str.split(',');
                    addMarker(latlng, total.toString(), map, obj,center); 
                }else {
                }
            }
        });
    }

    function loadMarkers(markers) {              
        var locations = new Array;
        var length = markers.length;
        if(length > 0){
            for (var i=0; i < length; i++){
                total = total + 1;
                locations.push(markers[i]);
                j = i + 1;
                var start = markers[i];
                var end = markers[j];

                /*Start*/
                var startCoords = start.split(",");
                var lat_start = startCoords[0];
                var lgn_start = startCoords[1];
                var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
                var obj = object[i];
                if(i== 0) {
                  center = 1;
                  addMarker(latlng_start,total.toString(),map,obj,center);
                  
                }else{
                    center = 0;
                    addMarker(latlng_start,total.toString(),map,obj,center);
                    
                }
                if(length == 1) {
                    latLngBounds = drawLine(lat_start, lgn_start, lat_start, lgn_start); 
                }
                if(j != length) {
                    /*End*/
                    var endCoords = end.split(",");
                    var lat_end = endCoords[0];
                    var lgn_end = endCoords[1];
                    latLngBounds = drawLine(lat_start, lgn_start, lat_end, lgn_end);
                }  

            }
            map.fitBounds(latLngBounds);
            gmarkers = locations;
            glocations = locations;
        }
    }

    function moveMarkerDrawLine(marker_1, marker_2, marker_3,pos_node) {
        marker_2 = marker_2.split(",");
        lat_2 = marker_2[0];
        lgn_2 = marker_2[1];

        if(marker_1 != null){
            marker_1 = marker_1.split(",");
            lat_1 = marker_1[0];
            lgn_1 = marker_1[1];
            drawLine(lat_1, lgn_1, lat_2, lgn_2,pos_node-1);         
        }
        if(marker_3 != null){
            marker_3 = marker_3.split(",");
            lat_3 = marker_3[0];
            lgn_3 = marker_3[1];
            drawLine(lat_2, lgn_2, lat_3, lgn_3,pos_node);         
        }
    }

    function drawLine(lat_start, lgn_start, lat_end, lgn_end, pos_node) {
        var startPt = new google.maps.LatLng(lat_start,lgn_start);
        var endPt = new google.maps.LatLng(lat_end,lgn_end);
        calcRoute(startPt, endPt, pos_node);
        latLngBounds.extend(startPt);
        latLngBounds.extend(endPt);
        return latLngBounds;
    }

    function calcRoute(source,destination,pos_node){
        var polyline = new google.maps.Polyline({
            path: [source, destination],
            strokeColor: '#FFF',
            strokeWeight: 2,
            strokeOpacity: 1
        });
        polyline.setMap(map);
        if(pos_node!=null){
            polylines[pos_node] =  polyline;
        }else{
            polylines.push(polyline);
        }
    }
    function addMarker(latlng,title,map,obj,center) {
        if(center == 1) {
            title = 'H';
            color = 'red';
        }else{
            color = '#2396e5';
        }        
        
        if(title == 10){
          var marker = new MarkerWithLabel({
              position: latlng,
              map: map,
              draggable: true,              
              labelContent: title,
              labelAnchor: new google.maps.Point(7, 10),
              labelClass: "labels",
              labelInBackground: false,
              icon: pinSymbol(color)
          });
        }else{
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                label: {
                    text: title,
                    color: "#FFF",
                    fontWeight: "bold",
                },
                draggable:true,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 10,
                    strokeColor: color,
                    fillColor: color,
                    fillOpacity: 1
                }
            });
        }
        var lat = latlng.lat().toFixed(6);
        var lgn = latlng.lng().toFixed(6);
        marker.setMap(map);

        var _id = obj[0];
        var _height = obj[3];
        
        $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes[]" value ="'+ lat + ',' + lgn +',' + _height + '" />');
        
        var html = '<form id="frmEditNode"  method="POST" action="/admin/flight/'+flight+'/edit/'+_id+'" >' +
                '<div class="form-group">' +
                    '<h4 class="w-90p">ポイント '+title+'<a class="jsDeleteNode" onClick="deleteMarker('+_id+');"><img src="/assets/img/icTrash.png" alt="" style="float: right;"/></a></h4>' +
                    '<hr>'+
                '</div>' +
                '<div class="form-group">' +
                    '<label class="col-md-4">経度: </label><strong>'+ lat + '</strong>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label class="col-md-4">緯度: </label><strong>' + lgn + '</strong>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label class="col-md-4 labelForInput">高度</label>' +
                    '<div class="input-group col-md-8">' +
                        '<input type="number" class="form-control" name ="height" min="0" id="height" placeholder="0" value="'+_height+'">' +
                        '<span class="input-group-addon" style="width: 0;">m</span>' +
                    '</div>' +
                '</div>' +
                '<div class="form-group center">' +
                    '<a href="javascript:;" class="jsClose"  onclick="document.getElementById(\'frmEditNode\').submit();"><img src="/assets/img/save.png" alt="キャンセル" /></a> ' +
                    '<a href="javascript:;" class="jsClose"  onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>' +
                '</div>' +
            '</form>';

        var infowindow = new google.maps.InfoWindow({
            content: html,
            maxWidth: 500
        });
        google.maps.event.addListener(infowindow, 'domready', function() {
           var iwOuter = $('.gm-style-iw');
           var iwBackground = iwOuter.prev();

           iwBackground.children(':nth-child(2)').css({'display' : 'none'});
           iwBackground.children(':nth-child(4)').css({'display' : 'none'});
        });

        google.maps.event.addListener(marker, 'click', function() {
            closeAllInfoWindows();
            infowindow.open(map,marker);
            infoWindows.push(infowindow); 
        });

        /*Move marker*/
        google.maps.event.addListener(marker, 'dragend', function() {
            
            if(title == 'H') {
                 number = 0;
                 polylines[0].setMap(null);
            }else{
                var number = parseInt(title);
                for(i= number - 1; i <= number ; i++) {
                  if(polylines[i] != null)
                      polylines[i].setMap(null);
              }
            }
            
            var lat = marker.position.lat().toFixed(6);
            var lgn = marker.position.lng().toFixed(6);
            marker_2 = lat+','+lgn;

            marker_1 = gmarkers[number - 1];
            gmarkers[title] = marker_2;
            marker_3 = gmarkers[number + 1];
            moveMarkerDrawLine(marker_1, marker_2, marker_3,number);
            $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height);
            if(title == 'H' || init)
              $('.jsSave').trigger('click');
        });
    }

    function pinSymbol(_color) {
        return {
           path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            strokeColor: _color,
            fillColor: _color,
            fillOpacity: 1
        };
    }
    function closeAllInfoWindows() {
        for (var i=0;i<infoWindows.length;i++) {
          infoWindows[i].close();
        }
    }

    function deleteMarker(id){
        $('#jsNodes #node[attr-point-id="'+id+'"]').remove();
        $('.jsSave').trigger('click');
    }

    

</script>

@endsection
