<?php
namespace App\Repositories;

use App\User;

class UserRepository extends AbstractRepository
{

    public function __construct(User $users)
    {
        $this->model         = $users;
    }

}
