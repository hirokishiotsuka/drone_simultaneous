<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\DroneRepository;
use App\Http\Requests;
use App\Drone;
use Validator;

class DroneController extends Controller
{
    public function __construct(Drone $drone, Request $request)
    {
        parent::__construct();
        $this->drone = $drone;
        $this->currentUser = auth()->user();
    }

    public function lists()
    {
        $drones = Drone::get();
        return view('drones.list', ['drones' => $drones]);
    }

    public function createDroneForm()
    {
        return view('drones.create');
    }

    public function createDroneProcess(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name'          => 'required|max:255',
            'manufacture'   => 'required|max:255',
            'serial'        => 'required|max:255'
        ];
        $messages = [
            'name.required'         => '機体名を入力してください',
            'name.max'              => '255文字以内で入力してください',
            'manufacture.required'  => '製造者名を入力してください',
            'manufacture.max'       => '255文字以内で入力してください',
            'serial.required'       => '製造番号を入力してください',
            'serial.max'            => '255文字以内で入力してください'
        ];

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->passes()) {
            Drone::create([
                'name'              => $data['name'],
                'manufacture'       => $data['manufacture'],
                'serial'            => $data['serial'],
                'insurance'         => $request->has('insurance') ? $data['insurance'] : '',
                'renovation'        => $request->has('renovation') ? $data['renovation'] : '',
                'post_on_homepage'  => $request->has('post_on_homepage') ? $data['post_on_homepage'] : '',
                'user_id'           => $this->currentUser->id,
                'status'            => 0
            ]);
            return redirect('/admin/drone');
        }

        $messagesError = $validator->errors();
        return view('drones.create', ['errors' => $messagesError]);
    }

    public function editDrone($id)
    {
        $drone = $this->drone->where('id',$id)->where('user_id',$this->currentUser->id)->first();
        return view('drones.edit', ['drone' => $drone]);
    }

    public function editDroneProcess(Request $request, $id)
    {
        $data = $request->all();

        $rules = [
            'name'          => 'required|max:255',
            'manufacture'   => 'required|max:255',
            'serial'        => 'required|max:255'
        ];
        $messages = [
            'name.required'         => '機体名を入力してください',
            'name.max'              => '255文字以内で入力してください',
            'manufacture.required'  => '製造者名を入力してください',
            'manufacture.max'       => '255文字以内で入力してください',
            'serial.required'       => '製造番号を入力してください',
            'serial.max'            => '255文字以内で入力してください'
        ];

        $validator = Validator::make($data, $rules, $messages);
        if ($validator->passes()) {

            $data['insurance']          = $request->has('insurance') ? $data['insurance'] : '';
            $data['renovation']         = $request->has('renovation') ? $data['renovation'] : '';
            $data['post_on_homepage']   = $request->has('post_on_homepage') ? $data['post_on_homepage'] : '';

            $drone = $this->drone->where('id',$id)->where('user_id',$this->currentUser->id)->first();
            if (!empty($drone)) {
                $drone->update($data);
                return redirect('/admin/drone');
            }
            return view('404');
        }

        $messagesError = $validator->errors();
        return redirect()->back()->with(['errors' => $messagesError]);
    }

    public function deleteProcess(Request $request)
    {
        try {
            $data = $request->all();
            $rules = [
                'id.*' => 'required|numeric',
            ];

            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                foreach ($data['id'] as $key => $id) {
                    $drone = $this->drone->where('id',$id)->where('user_id',$this->currentUser->id)->first();
                    if (!empty($drone)) {
                        Drone::destroy($drone->id);
                    } else return response()->error('You don\'t have permission', 404);
                }
                return response()->success($data['id']);
            } else return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
