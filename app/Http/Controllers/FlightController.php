<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Http\Middleware\authJWT;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\FlightRepository;
use App\Repositories\UserRepository;
use App\Flight;
use App\Point;
use App\HomePosition;
use App\FlightLog;
use App\GeoFilter;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

class FlightController extends Controller {

    public function __construct(FlightRepository $flights, UserRepository $users, Request $request) {
        parent::__construct();
        $this->flights = $flights;
        $this->users = $users;
        $this->currentUser = auth()->user();
        $this->currentUserAPI = $request->get('currentUser');
    }

    public function lists() {
        $flights = $this->flights->all()->sortByDesc('updated_at');
        $flights = $this->addUser($flights);
        return view('flights.lists', ['flights' => $flights]);
    }

    public function edit($id) {
        // Find Flight exist
        $flight = $this->flights->find($id);
        if ($flight !== null) {
            return view('flights.edit', ['flight' => $flight]);
        }
        return view('404');
    }

    public function proEdit(Request $request) {
        $this->validate($request, [
            'id' => 'required|numeric',
            'name' => 'required|max:255',
            'user_id' => 'required|numeric',
        ]);
        $flight = $this->flights->find($request['id']);
        if ($flight !== null && $user_id == $flight->user_id) {
            $flight = $this->flights->update(
                [
                    'name' => $request['name'],
                ], $request['id']
            );
            return view('flights.edit', ['flight' => $flight])->with('message', 'Edit succeed!');
        }
        return view('404');
    }

    public function detail($id) {
        // Find Flight exist
        $flight = $this->flights->find($id);
        if ($flight !== null) {
            $user_id = $flight->user_id;
            $points = $flight->points;
            $geofilters = $flight->geofilters;
            $geofilters = $this->convertGeoFilterArray($geofilters);
            $homeposition = Point::getHomePosition($id, $user_id);
            return view('flights.detail', ['flight' => $flight, 'nodes' => $points, 'homeposition' => $homeposition, 'geofilters' => $geofilters, 'user_id' => $user_id]);
        }
        return view('404');
    }

    public function convertGeoFilterArray($collection){
        return $collection->map(function($value){
            $pointsArray = unserialize($value->array_points);
            if(count($pointsArray) > 0) $value->array_points_convert = implode(";",$pointsArray);
            // unset($value->array_points);
            return $value;
        });
    }

    public function postDetail($id) {
        $flight = $this->flights->find($id, null, ['allPoints']);
        $is_home = !empty(Input::get('is_home')) ? 1 : 0;
        $is_home_sub = !empty(Input::get('is_home_sub')) ? 1 : 0;
        // if ($flight !== null && $this->currentUser->id == $flight->user_id) {
        if ($flight !== null) {
            /* Delete all node of flight */
            if (!empty($flight->allPoints())) {
                $flight->allPoints()->delete();
            }
            /* Create new nodes */
            $points_1 = Input::get('nodes');
            $points_2 = Input::get('nodes_sub');
            if (!empty($points_1)) {
                foreach ($points_1 as $key => $point_1) {
                    $arr = explode(",", $point_1);
                    $point_1 = new Point();
                    $point_1->lat = $arr[0];
                    $point_1->lng = $arr[1];
                    $point_1->height = $arr[2];
                    $point_1->flight_id = $id;
                    $point_1->user_id = $arr[6];
                    if ($key == 0 && $is_home) {
                        $point_1->is_home = 1;
                    }
                    $points_1 = $point_1->save();
                }
            }
            if (!empty($points_2)) {
                foreach ($points_2 as $key => $point_2) {
                    $arr = explode(",", $point_2);
                    $point_2 = new Point();
                    $point_2->lat = $arr[0];
                    $point_2->lng = $arr[1];
                    $point_2->height = $arr[2];
                    $point_2->flight_id = $id;
                    $point_2->user_id = $arr[6];
                    if ($key == 0 && $is_home_sub) {
                        $point_2->is_home = 1;
                    }
                    $points_2 = $point_2->save();
                }
            }
            $geoFilterPoints = Input::get('flyAreaPoint');
            $geoFilterZone = new GeoFilter();
            $geoFilterZone->flight_id = $id;
            $geoFilterZone->array_points = serialize($geoFilterPoints);
            $geoFilterZone->save();
            return redirect()->route('flight.detail', $id);
        }
        return view('404');
    }

    public function create() {
        $user_id = 1;
        return view('flights.create', ['user_id' => 1]);
    }

    public function proCreate(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        $user_id = 1;
        $flight = $this->flights->create([
            'user_id' => $user_id,
            'name' => $request['name'],
        ]);
        return redirect()->route('flight.detail', $flight->id);
    }

    public function delete($id) {
        $user_id = 1;
        $flight = $this->flights->find($id);
        if ($flight !== null && $user_id == $flight->user_id) {
            $this->flights->delete($id);
            $flights = $this->flights->all();
            return view('flights.lists', ['flights' => $flights])->with('message', 'Delete flight succeed!');
            ;
        }
        return view('404');
    }

    public function deleteMore(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'id.*' => 'required|numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $user_id = 1;
                foreach ($data['id'] as $key => $id) {
                    $flight = $this->flights->find($id);
                    if ($flight !== null && $user_id == $flight->user_id) {
                        $flight->allPoints()->delete();
                        $flight->logs()->delete();
                        $flight->medias()->delete();
                        $this->flights->delete($id);
                    } else
                        return response()->error('You don\'t have permission', 404);
                }
                return response()->success($data['id']);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getRealTimeList() {
        $flights = Flight::where('status', 0)->where('user_id', $this->currentUser->id)->orderBy('updated_at', 'DESC')->get();
        $flights = $this->addUser($flights);
        return view('flights.realtimes.lists', ['flights' => $flights]);
    }

    public function getRealTimeDetail($id) {
        // Find Flight exist
        $flight = $this->flights->find($id);
        if ($flight !== null) {
            $user_id = $flight->user_id;
            $points = $flight->points;
            $homeposition = Point::getHomePosition($id, $user_id);
            $geofilters = $flight->geofilters;
            $geofilters = $this->convertGeoFilterArray($geofilters);
            return view('flights.realtimes.detail', ['flight' => $flight, 'nodes' => $points, 'homeposition' => $homeposition, 'geofilters' => $geofilters, 'user_id' => $user_id]);
        }
        return view('404');
    }

    public function addUser($collection) {
        return $collection->map(function($value) {
                    $user = $this->users->find($value->user_id);
                    if ($user != null)
                        $value->user_name = $user->name;
                    return $value;
                });
    }

    // API
    public function createAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'name' => 'required|max:255',
                'user_id' => 'required|numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                // Create Flight
                $flight = $this->flights->create($data);
                // Return JSON data
                return response()->success(compact('flight'));
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function listAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'keyword' => 'string',
            ];
            if($request->has('keyword')) $data['keyword'] = strip_tags($data['keyword']);
            else $data['keyword'] = null;
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $flight = $this->flights->search(
                        [
                    'name' => $data['keyword'],
                    'user_id' => 1,
                        ], ['*'], false, 15, ['users'], 'DESC'
                );
                return response()->success(compact('flight'));
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function createNodeAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'id' => 'required|numeric',
                'nodes.*.lat' => 'numeric',
                'nodes.*.lng' => 'numeric',
                'nodes.*.height' => 'numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $flight = $this->flights->find($data['id']);
                if ($flight !== null) {
                    $this->attachNodeToFlight($flight, $data['nodes']);
                    return response()->success(compact('data'));
                }
                return response()->error($flight, 404);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detailAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'id' => 'required|numeric',
                'user_id' => 'required|numeric'
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $flight = $this->flights->find($data['id']);
                if ($flight !== null) {
                    $homeposition = Point::getHomePosition($data['id'], $data['user_id']);
                    $arrayPoint = $flight->points->toArray();
                    /**
                     * Check Home Position have Right Angle Or Not
                     */
                    if(!is_null($homeposition) && $homeposition->right_angle == 1 && $homeposition['height'] !== $arrayPoint[0]['height']){
                        if(count($arrayPoint) > 0){
                            $newPoint = Point::getHomePosition($data['id'], $data['user_id']);
                            $newPoint['height'] = $arrayPoint[0]['height'];
                        }
                        $newArrayPoint[] = $newPoint;
                    }
                    /**
                     * Foreach Point Array and Find Right Angle
                     */
                    foreach ($arrayPoint as $keyPoint => $point) {
                        if($point['right_angle'] == 1 && $keyPoint < count($arrayPoint)-1 && $point['height'] !== $arrayPoint[$keyPoint+1]['height']){
                            $newArrayPoint[] = $point;
                            $newPoint = $point;
                            $newPoint['height'] = $arrayPoint[$keyPoint+1]['height'];
                            $newArrayPoint[] = $newPoint;
                        }
                        else $newArrayPoint[] = $point;
                    }
                    $flight->homeposition = $homeposition;
                    if(!empty($newArrayPoint)){
                        unset($flight->points);
                        $flight->points = $newArrayPoint;
                    }
                    return response()->success(compact('flight'));
                }
                return response()->error($flight, 404);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // API Set Home Position
    public function setHomeAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'id' => 'required|numeric',
                'lat' => 'required|numeric',
                'lng' => 'required|numeric',
                // 'height' => 'required|numeric',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $flight = $this->flights->find($data['id']);
                // $user = JWTAuth::toUser($data['token']);
                if ($flight !== null && $flight->user_id == $this->currentUserAPI->id) {
                    $homePosition = Point::updateOrCreate([
                                'is_home' => 1,
                                'flight_id' => $data['id'],
                                'user_id' => $data['user_id'],
                                    ], [
                                'lat' => $data['lat'],
                                'lng' => $data['lng'],
                                'height' => 0,
                                'is_home' => 1,
                    ]);
                    $flight = $this->flights->find($data['id']);
                    $flight->homeposition = Point::getHomePosition($data['id'], $data['user_id']);
                    $flight->point = $flight->points;
                    unset($flight->points);
                    return response()->success(compact('flight'));
                }
                return response()->error($flight, 404);
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * API Send Logs Data Of Flight
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     * @param  Request      $request
     * @return Json         $flight
     */
    public function sendLogAPI(Request $request) {
        try {
            $data = $request->all();
            $rules = [
                'id' => 'numeric',
                'lat' => 'required|numeric',
                'lng' => 'required|numeric',
                'height' => 'required|numeric',
                'battery' => 'required|numeric',
                'flight_time' => 'required|numeric',
                'fstarted_at' => 'required|date_format:Y-m-d H:i:s',
                'user_id' => 'required|numeric',
                'plan_status' => 'required|numeric',
                // 'current_point' => 'numeric',
                'stream_key' => 'string|max:100'
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $flight = $this->flights->find($data['id']);
                // $user = JWTAuth::toUser($data['token']);
                logger($flight->user_id);
                // logger($this->currentUserAPI->id);
                if ($flight !== null) {
                    $flightLog = new FlightLog;
                    $flightLog->lat = $data['lat'];
                    $flightLog->lng = $data['lng'];
                    $flightLog->height = $data['height'];
                    $flightLog->battery = $data['battery'];
                    $flightLog->flight_time = $data['flight_time'];
                    // if($request->has('current_point')) $flightLog->current_point = $data['current_point'];
                    $flightLog->fstarted_at = $data['fstarted_at'];
                    $flightLog->user_id = $data['user_id'];
                    $flightLog->flight_id = $flight->id;
                    // Check the log is first logs
                    $firstLogs = FlightLog::where('flight_id', $data['id'])->where('fstarted_at', $data['fstarted_at'])->get();
                    // if(count($firstLogs) == 0){
                    //     $isFirstLogs = true;
                    // }else{
                    //     foreach ($firstLogs as $keyFirstLogs => $firstLog) {
                    //         if($firstLog->plan_status != 0){
                    //             $isFirstLogs = false;
                    //             break;
                    //         }
                    //         $isFirstLogs = true;
                    //     }
                    // }
                    // if($isFirstLogs){
                    //     if($flight->plan_status == 1)
                    //         $flight->plan_status = 1;
                    //     else
                    //         $flight->plan_status = 0;
                    // }else{
                    $flight->plan_status = $data['plan_status'];
                    // }
                    // End check the log is first logs?
                    $flightLog->plan_status = $data['plan_status'];
                    if($request->has('stream_key')) $flightLog->stream_key = $data['stream_key'];
                    $flightLog->save();
                    $flight->save();
                    $retval = array_merge($flightLog->toArray(),$flight->toArray());
                    logger($retval);
                    return response()->success($retval);
                } else {
                  return response()->error($flight, 404);
                }
            }
            return response()->error($validator->errors()->all(), 300);
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // API Send Data Media For Flight
    // public function uploadAPI(Request $request)
    // {
    // 	try {
    // 		$data = $request->all();
    // 		$rules = [
    // 			'id'			=> 'required|numeric',
    // 			'name'			=> 'required|string|max:255',
    // 			'lat'			=> 'required|numeric',
    // 			'lng'			=> 'required|numeric',
    // 			'height'		=> 'required|numeric',
    // 			'recorded_at'	=> 'required|date_format:Y-m-d H:i:s',
    // 		];
    // 		$validator = Validator::make($data, $rules);
    // 		if ($validator->passes()) {
    // 			$flight = $this->flights->find($data['id']);
    // 			// $user = JWTAuth::toUser($data['token']);
    // 			if ($flight != null && $flight->user_id == 1) {
    // 				return 'hiiiii';
    // 			}
    // 			return response()->error($flight, 404);
    // 		}
    // 		return response()->error($validator->errors()->all(), 300);
    // 	} catch (\Exception $e) {
    // 		return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    // 	}
    // }
    // public function deleteAPI(Request $request)
    // {
    // 	try{
    // 		$data	= $request->all();
    // 		$rules 	= [
    // 			'id'		=> 'required|numeric',
    // 		];
    // 		$validator = Validator::make($data, $rules);
    // 		if ($validator->passes()) {
    // 			$user_id = 1;
    // 			$flight = $this->flights->find($id);
    // 			$flight = $this->flights->findBy('user_id', $user_id);
    // 			if($flight !== null){
    // 				$this->flights->delete($id);
    // 				$flights = $this->flights->all();
    // 	        	return view('lists', ['flights' => $flights])->with('message', 'Delete flight succeed!');;
    // 			}
    // 			return view('404');
    // 			$flight = $this->flights->find($data['id']);
    // 			if($flight !== null){
    // 				$flight = $this->flights->getModel()->updateOrCreate(
    // 					[
    // 						'user_id' 	=> 1,
    // 					], [
    // 						'name'	=> $data['name'],
    // 				]);
    // 				return response()->success(compact('data'));
    // 			}
    // 			return response()->error($flight, 404);
    // 		}
    // 		return response()->error($validator->errors()->all(), 300);
    // 	} catch (\Exception $e) {
    // 		return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    // 	}
    // }

    public function attachNodeToFlight($model, $pointArr) {
        if (empty($pointArr))
            return null;
        $model->points()->delete();
        foreach ($pointArr as $key => $point) {
            $attachNode = new Point;
            $attachNode->lat = $point['lat'];
            $attachNode->lng = $point['lng'];
            $attachNode->height = $point['height'];
            $model->points()->save($attachNode);
        }
        return $model;
    }

    /*
     * This is action used edit node
     * @Author: Vu Nguyen
     */

    public function postEditNode($id, $user_id, $point_id, Request $request) {
        $data = $request->all();
        $rules = [
            'lat' => 'required|numeric|min:-90|max:90',
            'lgn' => 'required|numeric|min:-180|max:180',
            'height' => 'required|numeric|min:0|max:150',
            'capture' => 'required|numeric|min:0|max:1',
            'rightAngle' => 'required|numeric|min:0|max:1',
            'timeStay' => 'required|numeric|min:0|max:60',
        ];
        $validator = Validator::make($data, $rules);
        $user_id = 1;
        $flight = $this->flights->find($id);
        $flight = $this->flights->findBy('user_id', $user_id);
        if ($flight !== null) {
            $point = Point::find($point_id);
            if (!empty($point)) {
                //Check height and set default = 3 if height < 3
                if($point->is_home != 1) {
                    if($data['height'] >= 3 && $data['height'] <= 150) $point->height = $data['height'];
                    elseif($data['height'] < 3) $point->height = 3;
                }else $point->height = 0;
                if($data['capture'] >= 0 && $data['capture'] <= 1) $point->capture = $data['capture'];
                if($data['rightAngle'] >= 0 && $data['rightAngle'] <= 1) $point->right_angle = $data['rightAngle'];
                if($data['timeStay'] >= 0 && $data['timeStay'] <= 60) $point->time_stay = $data['timeStay'];
                if($data['lat'] >= -90 && $data['lat'] <= 90) $point->lat = $data['lat'];
                if($data['lgn'] >= -180 && $data['lgn'] <= 180) $point->lng = $data['lgn'];
                if ($point->save()) {
                    return redirect()->route('flight.detail', $id);
                }
            }
        }
    }

    public function postEditNodeRealTime($id, $user_id, $point_id, Request $request){
        $data = $request->all();
        $rules = [
            'lat' => 'required|numeric|min:-90|max:90',
            'lgn' => 'required|numeric|min:-180|max:180',
            'height' => 'required|numeric|min:0|max:150',
            'capture' => 'required|numeric|min:0|max:1',
            'rightAngle' => 'required|numeric|min:0|max:1',
            'timeStay' => 'required|numeric|min:0|max:60',
        ];
        $validator = Validator::make($data, $rules);
        // $user_id = 1;
        $flight = $this->flights->find($id);
        $flight = $this->flights->findBy('user_id', $user_id);
        if ($flight !== null) {
            $point = Point::find($point_id);
            if (!empty($point)) {
                if($point->is_home != 1) {
                    if($data['height'] >= 3 && $data['height'] <= 150) $point->height = $data['height'];
                    elseif($data['height'] < 3) $point->height = 3;
                }else $point->height = 0;
                if($data['capture'] >= 0 && $data['capture'] <= 1) $point->capture = $data['capture'];
                if($data['rightAngle'] >= 0 && $data['rightAngle'] <= 1) $point->right_angle = $data['rightAngle'];
                if($data['timeStay'] >= 0 && $data['timeStay'] <= 60) $point->time_stay = $data['timeStay'];
                if($data['lat'] >= -90 && $data['lat'] <= 90) $point->lat = $data['lat'];
                if($data['lgn'] >= -180 && $data['lgn'] <= 180) $point->lng = $data['lgn'];
                if ($point->save()) {
                    return redirect()->route('flight.realtimes.detail', $id);
                }
            }
        }
    }

    /*
     * This is action used edit node
     * @Author: Vu Nguyen
     */

    public function postCreateNode() {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $id = Input::get('id');
            $user_id = Input::get('user_id');
            $lat = Input::get('lat');
            $long = Input::get('long');

            $flight = $this->flights->find($id);
            if ($flight !== null) {
                $point = new Point();
                $point->flight_id = $id;
                $point->lat = $lat;
                $point->lng = $long;
                $point->height = 3;
                $point->user_id = $user_id;
                if ($point->save()) {
                    return response()->json([
                        'status' => true,
                        'data' => $point,
                        'message' => 'Create node is success'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Error'
                ]);
            }
        }
    }

}
