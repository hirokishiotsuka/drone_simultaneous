function ZoomControl(controlDiv, map) {
    // Creating divs & styles for custom zoom control
    controlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '70px';
    controlWrapper.style.height = '150px';
    controlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '50px';
    zoomInButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/assets/img/icZoomOut.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '50px';
    zoomOutButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/assets/img/icZoomin.png")';
    controlWrapper.appendChild(zoomOutButton);

    var locationButton = document.createElement('div');
    locationButton.style.width = '50px';
    locationButton.style.height = '60px';
    /* Change this to be the .png image you want to use */
    locationButton.style.backgroundImage = 'url("/assets/img/icLocation.png")';
    controlWrapper.appendChild(locationButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    google.maps.event.addDomListener(locationButton, 'click', function() {
        console.log('get Position Action');
        console.log('c_lat '+c_lat+'---c_long'+c_long);
        var l_latlng = new google.maps.LatLng(c_lat, c_long);
        map.setCenter(l_latlng);
        setHomeLocation(l_latlng);
    });
}

function setHomeLocation(c_latlng) {
    var marker = new google.maps.Marker({
        position: c_latlng,
        map: map,
        label: {
            text: 'A',
            color: "#2396e5",

        },
        draggable:false,
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            strokeColor: '#ECFCF8',
            fillColor: '#2396e5',
            fillOpacity: 1,
            strokeWeight: 5,
        }
    });
    marker.setMap(map);
}

function createNode(flight, _lat, _long, latlng, center, pos) {
    var _url = '/admin/flight/'+flight+'/create?lat='+_lat+'&long='+_long;
    $.ajax({
        url: _url,
        type: "POST",
        dataType: "JSON",
        success: function(res){
            if(res.status) {
                var str = res.data.id + ',' + res.data.lat + ','+ res.data.lng + ',' + res.data.height;
                object.push(str.split(','));
                var obj = str.split(',');
                addMarker(latlng, map, obj, center, pos);
            }else {
            }
        }
    });
}

function loadMarkers(markers) {
    var locations = new Array;
    var length = markers.length;
    if(length > 0){
        for (var i=0; i < length; i++){
            locations.push(markers[i]);
            j = i + 1;
            var start = markers[i];
            var end = markers[j];

            /*Start*/
            var startCoords = start.split(",");
            var lat_start = startCoords[0];
            var lgn_start = startCoords[1];
            var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
            var obj = object[i];
            if(!is_home) {
                addMarker(latlng_start,map,obj,center,i);
            }else{
                if(i== 0) {
                  center = 1;
                  addMarker(latlng_start,map,obj,center,i);

                }else{
                    center = 0;
                    addMarker(latlng_start,map,obj,center,i);
                }
            }

            if(length == 1) {
                latLngBounds = drawLine(lat_start, lgn_start, lat_start, lgn_start);
            }
            if(j != length) {
                /*End*/
                var endCoords = end.split(",");
                var lat_end = endCoords[0];
                var lgn_end = endCoords[1];
                latLngBounds = drawLine(lat_start, lgn_start, lat_end, lgn_end);
            }

        }
        map.fitBounds(latLngBounds);
        gmarkers = locations;
        glocations = locations;
    }
}

function moveMarkerDrawLine(marker_1, marker_2, marker_3,pos_node) {
    marker_2 = marker_2.split(",");
    lat_2 = marker_2[0];
    lgn_2 = marker_2[1];

    if(marker_1 != null){
        marker_1 = marker_1.split(",");
        lat_1 = marker_1[0];
        lgn_1 = marker_1[1];
        drawLine(lat_1, lgn_1, lat_2, lgn_2,pos_node-1);
    }
    if(marker_3 != null){
        marker_3 = marker_3.split(",");
        lat_3 = marker_3[0];
        lgn_3 = marker_3[1];
        drawLine(lat_2, lgn_2, lat_3, lgn_3,pos_node);
    }
}

function drawLine(lat_start, lgn_start, lat_end, lgn_end, pos_node) {
    var startPt = new google.maps.LatLng(lat_start,lgn_start);
    var endPt = new google.maps.LatLng(lat_end,lgn_end);
    calcRoute(startPt, endPt, pos_node);
    latLngBounds.extend(startPt);
    latLngBounds.extend(endPt);
    return latLngBounds;
}

function calcRoute(source,destination,pos_node){
    var polyline = new google.maps.Polyline({
        path: [source, destination],
        strokeColor: '#FFF',
        strokeWeight: 2,
        strokeOpacity: 1
    });
    polyline.setMap(map);
    if(pos_node!=null){
        polylines[pos_node] =  polyline;
    }else{
        polylines.push(polyline);
    }
}
function addMarker(latlng,map,obj,center,pos) {
    if(center == 1) {
        title = 'H';
        color = '#f7255c';
    }else{
        color = '#28b8fb';
    }
    var title = (++inc).toString();
    var image = {
      url: '/assets/img/icon10_v2.png',
      anchor: new google.maps.Point(16,16),
      origin: new google.maps.Point(0,0),
      size: new google.maps.Size(32, 32),
      scale: 1,
    };
    if(is_home && inc == 0)
        title = 'H';
    if(title == 10){
        var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable: false,
          icon: image,
          zIndex: 2,
          optimized: false
        });
    }else{
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            label: {
                text: title,
                color: "#FFF",
                fontWeight: "bold",
            },
            draggable:false,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 15,
                strokeColor: '#fff',
                strokeWeight: 2,
                fillColor: color,
                fillOpacity: 1
            },
            zIndex: 1,
            optimized: false
        });
    }
    var lat = latlng.lat().toFixed(7);
    var lgn = latlng.lng().toFixed(7);
    marker.setMap(map);

    var _id = obj[0];
    var _height = obj[3];

    $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes[]" value ="'+ lat + ',' + lgn +',' + _height + '" />');
    var html = '<form id="frmEditNode"  method="POST" action="/admin/flight/'+flight+'/edit/'+_id+'" >' +
            '<div class="form-group">' +
                '<h4 class="w-90p">ポイント '+title+'</h4>' +
                '<hr>'+
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-3">経度: </label><strong>'+ lat + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-3">緯度: </label><strong>' + lgn + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-3 labelForInput">高度</label>' +
                '<div class="input-group col-md-8">' +
                    '<input type="number" class="form-control" name ="height" min="0" id="height" placeholder="0" value="'+_height+'">' +
                    '<span class="input-group-addon" style="width: 0;">m</span>' +
                '</div>' +
            '</div>' +
            '<div class="form-group center">' +
                '<a href="javascript:;" class="jsClose"  onclick="document.getElementById(\'frmEditNode\').submit();"><img src="/assets/img/save.png" alt="保存" /></a> ' +
                '<a href="javascript:;" class="jsClose"  onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>' +
            '</div>' +
        '</form>';

    var infowindow = new google.maps.InfoWindow({
        content: html,
        maxWidth: 500
    });

    google.maps.event.addListener(marker, 'click', function() {
        closeAllInfoWindows();
        infowindow.open(map,marker);
        infoWindows.push(infowindow);
    });

    /*Move marker*/
    // google.maps.event.addListener(marker, 'dragend', function() {
    //     if(is_home) {
    //         title = pos;
    //     }else{
    //         title = pos;
    //     }
    //     if(title == 'H') {
    //         var number = 0;
    //         polylines[0].setMap(null);
    //     }else{
    //         var number = parseInt(pos);
    //         for(i= number - 1; i <= number ; i++) {
    //           if(polylines[i] != null)
    //               polylines[i].setMap(null);
    //       }
    //     }

    //     var lat = marker.position.lat().toFixed(7);
    //     var lgn = marker.position.lng().toFixed(7);
    //     marker_2 = lat+','+lgn;

    //     marker_1 = gmarkers[number - 1];
    //     gmarkers[title] = marker_2;
    //     marker_3 = gmarkers[number + 1];
    //     console.log(pos+'test');
    //     console.log('marker_1' + marker_1);
    //     console.log('marker_2' + marker_2);
    //     console.log('marker_3' + marker_3);
    //     console.log(title);
    //     moveMarkerDrawLine(marker_1, marker_2, marker_3, number);
    //     $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height);
    //     if(init){
    //         $('.jsSave').trigger('click');
    //     }
    // });

    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}

function ajaxLoadNodesForReplay(){
    var _url = '/api/flight/replay/'+idlogreplay;
    $.ajax({
        url: _url,
        type: "GET",
        dataType: "JSON",
        success: function(res){
          if(res.status) {
            flightInfo = res.data.flight.name;
              if(res.data.flight.lastFlight.length > 0){// && res.data.flight.plan_status == 0 && res.data.flight.lastFlight[res.data.flight.lastFlight.length-1].plan_status == 0){
                    res.data.flight.lastFlight.forEach(function(log, id, array) {
                        var newLogNodes = [];
                        if (id !== array.length - 1){
                            var nodeExist = newLogNodes.indexOf(log.lat+','+log.lng);
                            if (nodeExist > -1) {
                            }else{
                                logNodes.push(log.lat+','+log.lng);
                                newLogNodes.push(log.lat+','+log.lng);
                                timeOutLoadNode = setTimeout(function(){
                                    loadLogNodes(newLogNodes);
                                    if (currentNode == null) {
                                        if(res.data.flight.lastFlight!=null){
                                            currentNode = log;
                                            //Get Current Point From Logs
                                            var droneChart = $('#chartContainer').highcharts();
                                            droneChart.series[1].data.forEach(function(chart, key){
                                                if (chart.key == currentNode.current_point) {
                                                    chart.update({
                                                        marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                                    });
                                                } else {
                                                    chart.update({
                                                        marker: {symbol: 'url(/assets/img/blank.png)'}
                                                    });
                                                }

                                            });
                                            //End Process Point with second board
                                            var latlng_start = new google.maps.LatLng(log.lat,log.lng);
                                            addCurrentNode(latlng_start,map);
                                        }
                                    } else {
                                        if(res.data.flight.lastFlight!=null){
                                            currentNode = log;
                                            //Get Current Point From Logs
                                            var droneChart = $('#chartContainer').highcharts();
                                            droneChart.series[1].data.forEach(function(chart, key){
                                                if (chart.key == currentNode.current_point) {
                                                    chart.update({
                                                        marker: {symbol: 'url(/assets/img/drone_v3.png)'}
                                                    });
                                                } else {
                                                    chart.update({
                                                        marker: {symbol: 'url(/assets/img/blank.png)'}
                                                    });
                                                }

                                            });
                                            //End Process Point with second board
                                            currentNodeMarker.setPosition( new google.maps.LatLng( currentNode.lat,currentNode.lng ) );
                                        }
                                    }
                                }, parseInt(log.flight_time)*1000);
                                timeOutLoadNodeArray.push(timeOutLoadNode);
                            }
                        }
                    });
                }
            }
        },
    });
}

// Load Logs of Drone
function loadLogNodes(logNodes) {
    var locations = new Array;
    var length = logNodes.length;
    if(length > 0){
        for (var i=0; i < length; i++){
            locations.push(logNodes[i]);
            j = i + 1;
            var start = logNodes[i];
            var end = logNodes[j];

            /*Start*/
            var startCoords = start.split(",");
            var lat_start = startCoords[0];
            var lgn_start = startCoords[1];
            var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
            var obj = object[i];

            center = 0;
            addLogNode(latlng_start,map,obj,center,i);

        }
        map.fitBounds(latLngBounds);
        gmarkers = locations;
        glocations = locations;
    }
}

function addLogNode(latlng,map,obj,center,pos) {
    var title = (++inc).toString();
    var color = "#ffde00";

    var lat = latlng.lat().toFixed(7) - 0.000050;
    var lgn = latlng.lng().toFixed(7);

      var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable:false,
          icon: {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 5,
              strokeColor: '#fff',
              strokeWeight: 2,
              fillColor: color,
              fillOpacity: 1
          },
          zIndex: 1,
          optimized: false
      });

      var lat = latlng.lat().toFixed(7);
      var lgn = latlng.lng().toFixed(7);
      marker.setMap(map);
      logsNodeArray.push(marker);
    // var _id = obj[0];
    // var _height = obj[3];

    // $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes[]" value ="'+ lat + ',' + lgn +',' + _height + '" />');
    // var _delete = '<a class="jsDeleteNode" onClick="deleteMarker('+_id+');"><img src="/assets/img/icTrash.png" alt="" style="float: right;"/></a>';
    // if(title == 'H') {
    //     _delete = '';
    // }
    // var html = '<form id="frmEditNode"  method="POST" action="/admin/flight/'+flight+'/edit/'+_id+'" >' +
    //         '<div class="form-group">' +
    //             '<h4 class="w-90p">ポイント '+title+_delete+'</h4>' +
    //             '<hr>'+
    //         '</div>' +
    //         '<div class="form-group">' +
    //             '<label class="col-md-4">経度: </label><strong>'+ lat + '</strong>' +
    //         '</div>' +
    //         '<div class="form-group">' +
    //             '<label class="col-md-4">緯度: </label><strong>' + lgn + '</strong>' +
    //         '</div>' +
    //         '<div class="form-group">' +
    //             '<label class="col-md-4 labelForInput">高度</label>' +
    //             '<div class="input-group col-md-8">' +
    //                 '<input type="number" class="form-control" name ="height" min="0" id="height" placeholder="0" value="'+_height+'">' +
    //                 '<span class="input-group-addon" style="width: 0;">m</span>' +
    //             '</div>' +
    //         '</div>' +
    //         '<div class="form-group center">' +
    //             '<a href="javascript:;" class="jsClose"  onclick="document.getElementById(\'frmEditNode\').submit();"><img src="/assets/img/save.png" alt="キャンセル" /></a> ' +
    //             '<a href="javascript:;" class="jsClose"  onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>' +
    //         '</div>' +
    //     '</form>';

    // var infowindow = new google.maps.InfoWindow({
    //     content: html,
    //     maxWidth: 500
    // });

    // google.maps.event.addListener(marker, 'click', function() {
    //     closeAllInfoWindows();
    //     infowindow.open(map,marker);
    //     infoWindows.push(infowindow);
    // });

    /*Move marker*/
    // google.maps.event.addListener(marker, 'dragend', function() {
    //     if(is_home) {
    //         title = pos;
    //     }else{
    //         title = pos;
    //     }
    //     if(title == 'H') {
    //         var number = 0;
    //         polylines[0].setMap(null);
    //     }else{
    //         var number = parseInt(pos);
    //         for(i= number - 1; i <= number ; i++) {
    //           if(polylines[i] != null)
    //               polylines[i].setMap(null);
    //       }
    //     }

    //     var lat = marker.position.lat().toFixed(7);
    //     var lgn = marker.position.lng().toFixed(7);
    //     marker_2 = lat+','+lgn;

    //     marker_1 = gmarkers[number - 1];
    //     gmarkers[title] = marker_2;
    //     marker_3 = gmarkers[number + 1];
    //     console.log(pos+'test');
    //     console.log('marker_1' + marker_1);
    //     console.log('marker_2' + marker_2);
    //     console.log('marker_3' + marker_3);
    //     console.log(title);
    //     moveMarkerDrawLine(marker_1, marker_2, marker_3, number);
    //     $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height);
    //     if(init){
    //         $('.jsSave').trigger('click');
    //     }
    // });

    //*Hidden Background of Google API
    // google.maps.event.addListener(infowindow, 'domready', function() {
    //    var iwOuter = $('.gm-style-iw');
    //    var iwBackground = iwOuter.prev();

    //    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
    //    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    // });
}

function addCurrentNode(latlng,map) {
    var title = (++inc).toString();
    var color = "#ffde00";

    // var lat = latlng.lat().toFixed(7) - 0.000075;
    // var lng = latlng.lng().toFixed(7);
    // var latlng = new google.maps.LatLng(lat,lng);
    var image = {
      url: '/assets/img/drone_v2.png',
      anchor: new google.maps.Point(19,19),
      origin: new google.maps.Point(0,0),
      size: new google.maps.Size(40, 40),
      scale: 1,
    };
    currentNodeMarker = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:false,
        icon: image,
        zIndex: 4,
        optimized: false
    });

    currentNodeMarker.setMap(map);
    logsNodeArray.push(currentNodeMarker);

    var html = '<form id="frmCurrentNode"  method="POST" action="#" >' +
            '<div class="form-group">' +
                '<h4 class="w-90p">'+flightInfo+'</h4>' +
                '<hr>'+
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">経度: </label><strong>'+ currentNode.lat + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">緯度: </label><strong>' + currentNode.lng + '</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">高度:</label><strong>' + currentNode.height + ' m</strong>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="col-md-4">バッテリー:</label><strong>' + currentNode.battery + ' %</strong>' +
            '</div>' +
            '<div class="form-group center">' +
                '<a href="javascript:;" class="jsClose" onclick="document.getElementById(\'frmEditNode\').submit();"><img src="/assets/img/save.png" alt="保存" /></a> ' +
                '<a href="javascript:;" class="jsClose" onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>' +
            '</div>' +
        '</form>';

    var infowindow = new google.maps.InfoWindow({
        content: html,
        maxWidth: 500
    });

    google.maps.event.addListener(currentNodeMarker, 'click', function() {
        closeAllInfoWindows();
        infowindow.open(map,currentNodeMarker);
        infoWindows.push(infowindow);
    });
    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}

function pinSymbol(_color) {
    return {
       path: google.maps.SymbolPath.CIRCLE,
        scale: 10,
        strokeColor: _color,
        fillColor: _color,
        fillOpacity: 1
    };
}
function closeAllInfoWindows() {
    for (var i=0;i<infoWindows.length;i++) {
      infoWindows[i].close();
    }
}

function deleteMarker(id){
    $('#jsNodes #node[attr-point-id="'+id+'"]').remove();
    $('.jsSave').trigger('click');
}

function isCategoryA(category_code) {
    return category_code == 'A';
}

function isCategoryB(category_code) {
    return category_code == 'B';
}

function isCategoryCityLimit(category_code) {
    return category_code == 'city_limit';
}

function categoryName(category_code) {
    if(isCategoryA(category_code)){
        return category_a_name;
    }else if(isCategoryB(category_code)){
        return category_b_name;
    }else{
        return category_city_limit_name;
    }
}

function no_fly_zone(map, item, category) {
    color = '#f39c12';
    mile = 5; // 5 miles
    image = red_image;
    if(isCategoryB(category)) {
        color = '#2d89ef';
        mile = 1;
        image = blue_image;
    }

    var marker_latlng = new google.maps.LatLng(item[0], item[1]);
    var noFlyZone = new google.maps.Marker({
        position: marker_latlng,
        popup: true,
        icon: image,
        map: map
    });
    if(isCategoryCityLimit(category)){
        noFlyZone.setVisible(false)
    }

    text = item[2] + " (" + categoryName(category) + ")";
    if(isCategoryA(category)){
        text += "<br/><small>No Fly Zone<br/>Height Restriction Zone</small>"
    }else{
        text += "<br/><small>No Fly Zone</small>"
    }
    var infowindow = new google.maps.InfoWindow({
        content: text
    });

    if(!isCategoryCityLimit(category)){
        var options = {
          strokeColor: color,
          strokeOpacity: 0.6,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.4,
          map: map,
          center: noFlyZone.position,
          radius: mile * 1609.344
        };
        // Add the circle for this city to the map.
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }

    if(isCategoryA(category)){    // Category A
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: noFlyZone.position,
            radius: 1.5 * 1609.344
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }else if(isCategoryCityLimit(category)){
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: marker_latlng,
            radius: item[5] * 1000
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
        google.maps.event.addListener(cityCircle, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    if(!isCategoryCityLimit(category)){
        google.maps.event.addListener(noFlyZone, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}
function _respondToMapChange(map){
    // var newInitialBounds = map.getBounds();
    // if(initialBounds == null || initialBounds !== newInitialBounds){
    //     initialBounds = newInitialBounds;
    //     console.log(initialBounds.getNorthEast().lat()+'-'+initialBounds.getSouthWest().lat()+'----'+initialBounds.getSouthWest().lng()+'-'+initialBounds.getNorthEast().lng());
    //     var _url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds='+initialBounds.getNorthEast().lat()+','+initialBounds.getSouthWest().lat()+','+initialBounds.getSouthWest().lng()+','+initialBounds.getNorthEast().lng()+'&faa=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=7200&gliders=1&stats=1';
    //     $.ajax({
    //         url: _url,
    //         type: "GET",
    //         dataType: "JSON",
    //         success: function(res, textStatus, xhr){
    //             clearMarkers();
    //             jQuery.each(res, function(key, val) {
    //                 if(key != "full_count" && key != "version" && key != "stats"){
    //                     addFlight(val[1], val[2], val[3], map);
    //                 }
    //             });
    //         }
    //     });
    // }
    // weatherLoaded = false;
    var newInitialCenter = map.getCenter();

    if(initialCenter == null || initialCenter !== newInitialCenter){
        initialCenter = newInitialCenter;
        console.log(initialCenter.lat()+'----'+initialCenter.lng());
        
        if(!weatherIconClicked){
            $('#weatherIcon_1').click(function(){
                loadWeather(initialCenter.lat(),initialCenter.lng());
                weatherLoaded = true;
                weatherIconClicked = true;
            });
        }else{
            if(weatherLoaded){
                if ( $("#reload_btn").hasClass("reloadHidden") ) {
                    $("#reload_btn").removeClass("reloadHidden").addClass("reloadShow");
                    weatherLoaded = false;
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/reload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/reload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }else{
                if( $("#reload_btn").hasClass("reloadShow") ) {
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/reload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/reload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }
        }
        
    }
}

function addFlight(lat,lng,rotate,name,speed,map) {
    var color = "#f7255c";
    var latlng = new google.maps.LatLng(lat,lng);
    var image = {
        path: "M24.8,15.2L14,7.2c0.1-1.2,0.1-4,0.1-4.5c-0.4-4.3-3.1-2.4-3.1-0.5c0,0.5-0.1,3.2-0.1,5.1   L0.2,15.4l0,2.6l11-5l0.4,8l-3.2,2.3l0,1.7l4.2-1.2l0,0l4.2,1.1l-0.1-1.7L13.5,21l0.3-8l11.1,4.8L24.8,15.2L24.8,15.2z",
        fillColor: '#f0c61d',
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 1,
        anchor: new google.maps.Point(0,0),
        size: new google.maps.Size(32, 32),
        scale: 1,
        rotation: rotate,
    };
    var aircraft = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:false,
        icon: image,
        title: name,
        zIndex: 1,
    });
    aircraft.setMap(map);
    aircrafts.push(aircraft);

    var speedConvert = (parseFloat(speed))/3600;
    
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,1);
    }, 1000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,1.5);
    }, 1500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,2);
    }, 2000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,2.5);
    }, 2500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,3);
    }, 3000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,3.5);
    }, 3500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,4);
    }, 4000);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,4.5);
    }, 4500);
    setTimeout(function(){
        moveFlightAfterHalfSecond(aircraft,lat,lng,speed,rotate,5);
    }, 5000);
}
function moveFlightAfterHalfSecond(marker,lat,lng,speed,rotate,time){
    with (Math) {
        if (marker != undefined) {
            lat = rad(lat);
            lng = rad(lng);
            rotate = rad(rotate);
            var speedConvert = (parseFloat(speed))/3600;
            var distance = parseFloat((speedConvert*time)/radiusEarth);
            var newLat = asin(sin(lat)*cos(distance) + cos(lat)*sin(distance)*cos(rotate));
            var newLng = lng + atan2(sin(rotate)*sin(distance)*cos(lat), cos(distance)-sin(lat)*sin(newLat));
            marker.setPosition(new google.maps.LatLng( deg(newLat),deg(newLng) ));
        }
    }
}
function rad(dg) {
    return (dg* Math.PI / 180);
}

function deg(rd) {
    return (rd* 180 / Math.PI);
}
function setMapOnAll(map) {
    for (var i = 0; i < aircrafts.length; i++) {
        aircrafts[i].setMap(map);
    }
}
function deleteOldAirCraft() {
    clearMarkers();
    aircrafts = [];
}
function clearMarkers() {
    setMapOnAll(null);
}
function removeAllNodesForReplay(){
    var map = null;
    for (var i = 0; i < logsNodeArray.length; i++) {
        logsNodeArray[i].setMap(map);
    }
    logsNodeArray = [];
    currentNode = null;
}
function noFlyZoneSwicher(){
    if (!document.getElementById('noFlyZone-checkbox').checked) {
        drawNoFlyZone();
    }else{
        setMapClearNoFlyZone(null);
        noFlyZones = [];
    }
};
/**
 * Draw exist geo filter zones
 * This function will be called when Geo Filter Switch ON
 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
 */
function geoFilterSwicher(){
    if (document.getElementById('geoFilterZone-checkbox').checked) {
        setMapClearGeoFilterZone(null);
    }else{
        setMapClearGeoFilterZone(map);
    }
};
function drawNoFlyZone(){
    $.each(mc_category_a, function(index, item) {
        no_fly_zone(map, item, 'A')
    });

    $.each(mc_category_b, function(index, item) {
        no_fly_zone(map, item, 'B')
    });

    $.each(mc_category_city_limit, function(index, item) {
        no_fly_zone(map, item, 'city_limit');
    });
}
function drawGeoFilterZone(){
    // Hard Code Fly Area
    var flyAreaWrapperPath = [
        {lat: 26.5202597, lng: 128.0207795},
        {lat: 26.5197269, lng: 128.0210423},
        {lat: 26.5199621, lng: 128.0213937},
        {lat: 26.5199405, lng: 128.0216378},
        {lat: 26.5202837, lng: 128.0217492},
        {lat: 26.5205285, lng: 128.0216405},
        {lat: 26.5204013, lng: 128.0211885},
        {lat: 26.5202597, lng: 128.0207795}
    ];
    var flyAreaWrapper = new google.maps.Polygon({
        paths: flyAreaWrapperPath,
        strokeColor: '#052eff',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#052eff',
        fillOpacity: 0.2
    });
    geoFilterZones.push(flyAreaWrapper);
    var lineSymbol = {
        // path: 'M-1,0a1,1 0 1,0 2,0a1,1 0 1,0 -2,0',
        path: 'M 0,-1 0,1',
        strokeColor: '#052eff',
        strokeOpacity: 1,
        fillColor: '#052eff',
        fillOpacity: 1,
        scale: 4
    };
    var line = new google.maps.Polyline({
        path: flyAreaWrapperPath,
        strokeOpacity: 0,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '20px'
        }],
        map: map
    });
    geoFilterZones.push(line);
    flyAreaWrapper.setMap(map);
}
function setMapClearNoFlyZone(map) {
    for (var i = 0; i < noFlyZones.length; i++) {
        noFlyZones[i].setMap(map);
    }
}
function setMapClearGeoFilterZone(map) {
    for (var i = 0; i < geoFilterZones.length; i++) {
        geoFilterZones[i].setMap(map);
    }
}
function loadWeather(c_lat,c_long) {
    var _url = 'http://52.43.150.151/assets/getWeather.php?lat='+c_lat+'&lng='+c_long;
    if(!weatherLoaded){
        $.ajax({
            url: _url,
            type: "GET",
            dataType: "JSON",
            success: function(res, textStatus, xhr){
                if(xhr.status == 200) {
                    $('.popover.fade.bottom.in').css("backgroundImage","none");
                    $('.popover.fade.bottom.in').css("backgroundColor","#fff");
                    var weatherDate = res.precipitation.time.split(' ');
                    var weatherDateConvert = weatherDate[1].split(':');
                    var endPrecipitationTime = parseInt(weatherDateConvert[0])+6;
                    var wind_direction_string_jp;

                    if(endPrecipitationTime >= 24) endPrecipitationTime = '0'+parseInt(endPrecipitationTime-24);

                    switch (res.forecasts.wind_direction_string) {
                        case "east":
                            wind_direction_string_jp = "東";
                            break;
                        case "east-northeast":
                            wind_direction_string_jp = "東北東";
                            break;
                        case "northeast":
                            wind_direction_string_jp = "北東";
                            break;
                        case "north-northeast":
                            wind_direction_string_jp = "北北東";
                            break;
                        case "north":
                            wind_direction_string_jp = "北";
                            break;
                        case "north-northwest":
                            wind_direction_string_jp = "北北西";
                            break;
                        case "northwest":
                            wind_direction_string_jp = "北西";
                            break;
                        case "west-northwest":
                            wind_direction_string_jp = "西北西";
                            break;
                        case "west":
                            wind_direction_string_jp = "西";
                            break;
                        case "west-southwest":
                            wind_direction_string_jp = "西南西";
                            break;
                        case "souththwest":
                            wind_direction_string_jp = "南西";
                            break;
                        case "south-southwest":
                            wind_direction_string_jp = "南南西";
                            break;
                        case "south":
                            wind_direction_string_jp = "南";
                            break;
                        case "south-southeast":
                            wind_direction_string_jp = "南南東";
                            break;
                        case "southeast":
                            wind_direction_string_jp = "南東";
                            break;
                        case "east-southeast":
                            wind_direction_string_jp = "東南東";
                            break;    
                        case "east":
                            wind_direction_string_jp = "東";
                    } 

                    $('#addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                    $('#dateForecast').html(res.forecasts.time);
                    $('#nameWeather').html(res.weather.weather.name);
                    $('#humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                    $('#temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                    $('#precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                    $('#timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                    $('#windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                    $('#directStringForecast').html(wind_direction_string_jp);
                    $('#directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                    $('#weather-content #addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                    $('#weather-content #dateForecast').html(res.forecasts.time);
                    $('#weather-content #nameWeather').html(res.weather.weather.name);
                    $('#weather-content #humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                    $('#weather-content #temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                    $('#weather-content #precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                    $('#weather-content #timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                    $('#weather-content #windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                    $('#weather-content #directStringForecast').html(wind_direction_string_jp);
                    $('#weather-content #directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                    document.getElementById('weatherIcon_2').style.backgroundImage = "url('/assets/img/"+res.weather.weather.code+".png')";
                    $('#weather-content #weatherIcon_2').css("backgroundImage","url('/assets/img/"+res.weather.weather.code+".png')");

                    var windDirection = document.getElementById('windDirection');
                    windDirection.style.webkitTransform = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.mozTransform    = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.msTransform     = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.oTransform      = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                    windDirection.style.transform       = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';

                    $('#weather-content #windDirection').css("webkitTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("mozTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("msTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("oTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                    $('#weather-content #windDirection').css("transform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');

                    weatherLoaded = true;
                    $("#reload_btn").removeClass("reloadShow").addClass("reloadHidden");
                    $("#reload_btn").html('');
                    $("#weather-content #reload_btn").html('');
                }else {
                }
            }
        });
    }
}
function processAlertForRealtime(jqXHR){
    if (jqXHR.status == true) {
        currentTime = new Date().getTime();
        // Case: Don't have any logs but plan_status = 1
        if (jqXHR.data.flight.lastFlight == null || jqXHR.data.flight.lastFlight.length == 0) {
            if (jqXHR.data.flight.plan_status == 1) {
                console.log('Drone already fly but don\'t have any logs.');
            }
        } else {
            // Case: Plan Status !== 1 !==0
            if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status !== 1 && jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status !== 0) {
                if (!modalAlertShowed) {
                    $('#alertModal').modal('show');
                    modalAlertShowed = true;
                }
            }
            // Case: Plan Status == 0: Mean Landing
            else if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status == 0) {
                var checkExistFlyingStatus;
                loopCheckExistFlyingStatus: {
                    for (var k in jqXHR.data.flight.lastFlight) {
                        if (jqXHR.data.flight.lastFlight[k].plan_status == 1) {
                            $('#alertModal').modal('hide');
                            checkExistFlyingStatus = true;
                            break loopCheckExistFlyingStatus;
                        }
                    }
                }
                if (checkExistFlyingStatus !== true) {
                    if (!modalAlertShowed) $('#alertModal').modal('show');
                    clearInterval(getLogInterval);
                    timeIntervalGetLog = 10000;
                    getLogInterval = setInterval(function() {
                        modalAlertShowed = true;
                        ajaxLoadNodes();
                    }, timeIntervalGetLog);
                } else {
                    clearInterval(getLogInterval);
                    clearFlightLogMakers(logsNodeArray);
                }
            }
            // Case: Plan Status == 1: Mean Flying 
            else if (jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].plan_status == 1) {
                currentLogID = jqXHR.data.flight.lastFlight[jqXHR.data.flight.lastFlight.length - 1].id;
                //  Case: Plan Status == 1: But don't have any new log
                if (currentLogID == lastLogID && (currentTime - lastTimeGetLog) > 10000) {
                    if (!modalAlertShowed) $('#alertModal').modal('show');
                    clearInterval(getLogInterval);
                    timeIntervalGetLog = 10000;
                    getLogInterval = setInterval(function() {
                        modalAlertShowed = true;
                        ajaxLoadNodes();
                    }, timeIntervalGetLog);
                }
                //  Case: Plan Status == 1: Have new log
                else if (currentLogID != lastLogID) {
                    $('#alertModal').modal('hide');
                    lastLogID = currentLogID;
                    lastTimeGetLog = currentTime;
                    clearInterval(getLogInterval);
                    timeIntervalGetLog = 3000;
                    getLogInterval = setInterval(function() {
                        ajaxLoadNodes();
                    }, timeIntervalGetLog);
                }
            }
        }
    } else if (jqXHR.status === 429) {
        clearInterval(getLogInterval);
        timeIntervalGetLog = 10000;
        getLogInterval = setInterval(function() {
            ajaxLoadNodes();
        }, timeIntervalGetLog);
    }
}
function clearFlightLogMakers(objFlight) {
    var map = null;
    for (var i = 0; i < objFlight.length; i++) {
        objFlight[i].setMap(map);
    }
    objFlight = [];
}
$(document).ready(function(){
    if (!document.getElementById('noFlyZone-checkbox').checked) {
        $('#noFlyZone-checkbox').bootstrapSwitch('toggleState', true, true);
    }
    if (!document.getElementById('geoFilterZone-checkbox').checked) {
        $('#geoFilterZone-checkbox').bootstrapSwitch('toggleState', true, true);
    }
    $("[name='noFlyZone-checkbox']").bootstrapSwitch();
    $("[name='geoFilterZone-checkbox']").bootstrapSwitch();
    var weatherPopover = $("#drone_weather_toolbar a").popover({
        html : true,
        content: function() {
            return $("#weather-content").html();
        },
    });
    weatherPopover.on("hidden.bs.popover", function(e) {
        weatherIconClicked = false;
    });
    // Call API to load flights each 2s
    setInterval(function(){
        bounds = map.getBounds();
        var _url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds='+bounds.getNorthEast().lat()+','+bounds.getSouthWest().lat()+','+bounds.getSouthWest().lng()+','+bounds.getNorthEast().lng()+'&faa=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=7200&gliders=1&stats=1';
        $.ajax({
            url: _url,
            type: "GET",
            dataType: "JSON",
            success: function(res, textStatus, xhr){
                clearMarkers();
                jQuery.each(res, function(key, val) {
                    if(key != "full_count" && key != "version" && key != "stats"){
                        addFlight(val[1], val[2], val[3], val[16], val[5], map);
                    }
                });
            }
        });
    }, 5000);
});

