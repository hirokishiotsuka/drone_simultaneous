@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row">
        <div id="content" class="col-sm-12 full">
            
            <div class="row box-error">
                    
                <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
                
                    <h1>403</h1>
                    <h2>You do not have permission for this page.</h2>
                
                </div><!--/col-->
                    
            </div><!--/row-->
            
        </div><!--/content-->   
            
    </div><!--/row-->       
        
</div><!--/container-->
@endsection