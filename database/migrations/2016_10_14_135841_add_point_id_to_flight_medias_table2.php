<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointIdToFlightMediasTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flight_medias', function (Blueprint $table) {
            $table->integer('point_id')->unsigned()->nullable()->after('flight_id');
            $table->foreign('point_id')
                ->references('id')->on('points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flight_medias', function (Blueprint $table) {
            //
        });
    }
}
