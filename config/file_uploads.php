<?php

return [
	"flight" => [
		'path' => 'uploads/flight/media/',
		'sizes' => [
			'300x300',
		],
		'isCrop' => false
	]
];
