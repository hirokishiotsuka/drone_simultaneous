<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePosition extends Model
{
    protected $table = 'home_positions';

    protected $primaryKey = 'flight_id';

    public function flight(){
    	return $this->belongsTo('App\Flight', 'flight_id');
    }
}
