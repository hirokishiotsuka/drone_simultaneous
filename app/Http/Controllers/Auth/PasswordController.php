<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;


    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->subject = 'パスワードリセット';
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255',
        ];
        $messages = [
            'email.email'           => 'メールアドレスの形式が間違っています',
            'email.required'        => 'メールアドレスを入力してください。',
        ];
        $this->validate($request, $rules, $messages);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response, $request);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }
    protected function getSendResetLinkEmailSuccessResponse($response, $request)
    {
        $response = '['.$request->email.']'.trans($response);
        return redirect()->back()->with('status', $response);
    }

    public function reset(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
            'password_confirmation' => 'required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
        ];
        $messages = [
            'email.email'               => '正しいメールアドレスの形式で入力してください',
            'email.required'            => 'メールアドレスを入力してください',
            'email.max'                 => '255文字以内で入力してください',
            'password.required'         => 'パスワードを入力してください',
            'password.min'              => '8文字以上入力してください',
            'password.regex'            => 'パスワードが文字と数字を合わせて入力してくだ',
            'password_confirmation.required' => 'パスワード確認を入力してください',
            'password_confirmation.min' => '8文字以上入力してください',
            'password_confirmation.regex' => 'パスワードが文字と数字を合わせて入力してくだ',
            'password.confirmed'        => 'パスワード確認が間違っています',
        ];
        $this->validate($request, $rules, $messages);

        $credentials = $this->getResetCredentials($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);
            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }
}
