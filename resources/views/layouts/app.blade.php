<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::to('assets/ico/apple-touch-icon-57-precomposed.png') }}">
    <!-- <link rel="shortcut icon" href="{{ URL::to('assets/ico/favicon.png') }}"> -->

    <title>Drone Control Panel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ URL::to('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- page css files -->
    <link href="{{ URL::to('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/morris.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/climacons-font.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ URL::to('assets/css/style.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <script src="{{ URL::to('assets/js/jquery-2.1.0.min.js') }}"></script>
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    @yield('header')
</head>
<body id="app-layout">
    <div class="navbar" role="navigation">
        <div class="container-fluid nav-menu-admin-flight">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar">a</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{ URL::to('assets/img/logoDrone.png') }}"></a>
            </div>

            <ul class="nav navbar-nav navbar-right">

                <li><a href="#">&nbsp;</a></li>
            </ul>
        </div>
    </div>

    <div class="container-fluid content">
        <div class="row">
            @if (!Auth::guest())
            <!-- start: Main Menu -->
            <div class="sidebar col-md-2 col-sm-1 ">

                <div class="sidebar-collapse collapse">

                    <ul class="nav nav-sidebar">

                        <li id="flight_menu"><a href="{{ url('/admin/flight/list') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/home-icon.png') }}"><span class="hidden-sm text"> トップ </span>
                        </a></li>

                        <li id="realtime_menu"><a href="{{ url('/admin/realtime') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/drone.png') }}"><span class="hidden-sm text"> リアルタイムフライト </span>
                        </a></li>

                        <li id="media_menu"><a href="{{ url('/admin/media') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/media.png') }}"></i><span class="hidden-sm text"> メディア </span>
                        </a></li>

                        <li id="drone_menu"><a href="{{ url('/admin/drone') }}">
                             <img class="icon-menu" src="{{ url('/assets/img/drone-free-icon-8.png') }}"><span class="hidden-sm text"> 機体 </span>
                        </a></li>

                        <li id="logs_menu"><a href="{{ url('/admin/logs') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/ico_doc.png') }}"></i><span class="hidden-sm text"> ログ </span>
                        </a></li>

                        <li id="profile_menu"><a href="{{ url('/admin/profile') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/info.png') }}"><span class="hidden-sm text"> アカウント設定</span>
                        </a></li>

                        <!-- <li><a href="#">
                            <img class="icon-menu" src="{{ url('/assets/img/add_user.png') }}"></i><span class="hidden-sm text"> ユーザー登録 </span>
                        </a></li> -->

                        <li id="logout_menu"><a href="{{ url('/admin/logout') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/logout.png') }}"></i><span class="hidden-sm text"> ログアウト</span>
                        </a></li>

                    </ul>
                    @role('admin')
                    <div class="nav-sidebar title"><span>Admin</span></div>

                    <ul class="nav nav-sidebar">

                        <li id="home_menu"><a href="{{ url('/admin/home') }}">
                            <img class="icon-menu" src="{{ url('/assets/img/setting.png') }}"><span class="hidden-sm text"> Admin設定 </span>
                        </a></li>

                        <li id="user_menu"><a href="{{ url('/admin/user')}}">
                            <img class="icon-menu" src="{{ url('/assets/img/members.png') }}"><span class="hidden-sm text"> ユーザー一覧</span>
                        </a></li>

                    </ul>
                    @endrole

                </div>

                            </div>
            <!-- end: Main Menu -->
            @endif
            @yield('content')
        </div>
    </div>
    <!--[if IE]>

        <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
        </script>

    <![endif]-->
    <script src="{{ URL::to('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>


    <!-- page scripts -->
    <script src="{{ URL::to('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/fullcalendar.min.js') }}"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/js/excanvas.min.js"></script><![endif]-->
    <!--<script src="{{ URL::to('assets/js/jquery.flot.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.time.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.flot.spline.min.js') }}"></script>-->
    <script src="{{ URL::to('assets/js/jquery.autosize.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.placeholder.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/moment.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/daterangepicker.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/raphael.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/morris.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/uncompressed/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ URL::to('assets/js/uncompressed/gdp-data.js') }}"></script>
    <script src="{{ URL::to('assets/js/gauge.min.js') }}"></script>

    <!-- theme scripts -->
    <script src="{{ URL::to('assets/js/custom.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/core.min.js') }}"></script>

    <!-- inline scripts related to this page -->
    <!--<script src="{{ URL::to('assets/js/pages/index.js') }}"></script>-->

    <!-- end: JavaScript-->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('script')
</body>
</html>
