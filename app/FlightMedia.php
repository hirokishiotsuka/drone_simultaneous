<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightMedia extends Model
{
    protected $table = 'flight_medias';
    protected $fillable = [
    	'name',
    	'link',
    ];
    protected $appends = array('thumbnail', 'baseurl');

    public function flight(){
    	return $this->belongsTo('App\Flight', 'flight_id');
    }

    public function getThumbnailAttribute()
    {
        $fileType = explode('.', $this->name);
        $videoArray = [
            'mov',
            'mp4',
            'MOV',
            'MP4'
        ];
        if (in_array(end($fileType), $videoArray)) {
            $name = str_replace('.', '-', $this->name).'.jpg';
            return $name;
        } else {
            return $this->name;
        }
    	
    }

    public function getBaseUrlAttribute()
    {
    	return url('/');
    }
}
