@extends('layouts.node')
@section('content')       
<style>
    h3 {
        font-size: 16px;
        line-height: 16px;
        font-weight: 400;
    }
    .modal-content {
        margin-top: 50%;
    }
    .modal-header{
        background: #42c2f7; /* Old browsers */
        background: -moz-linear-gradient(bottom,  #42c2f7 0%, #2082d8 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, right top, color-stop(0%,#42c2f7), color-stop(100%,#2082d8)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(bottom,  #42c2f7 0%,#2082d8 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(bottom,  #42c2f7 0%,#2082d8 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(bottom,  #42c2f7 0%,#2082d8 100%); /* IE10+ */
        background: linear-gradient(to bottom,  #42c2f7 0%,#2082d8 100%); /* W3C */
        color: #fff;
        padding-bottom: 3px;
        padding-top: 1px;
        padding-left: 16px;
        -webkit-border-top-left-radius: 5px !important;
        -moz-border-top-left-radius: 5px !important;
        border-top-left-radius: 5px !important;
        -webkit-border-top-right-radius: 5px !important;
        -moz-border-top-right-radius: 5px !important;
        border-top-right-radius: 5px !important;
    }
    .modal-header h3{
        margin: 13px 0px;
    }
    .modal-lg{
        width: 470px !important;
    }
    .modal-footer{
        text-align: center;
        margin-top: -20px;
        border-top: none;
    }
    .modal-content  {
        -webkit-border-radius: 6px !important;
        -moz-border-radius: 6px !important;
        border-radius: 6px !important; 
    }
    .modal-body input.form-control {
        border-radius: 5px !important;
        height: 38px;
        background-color: #f8f8f8;
        font-size: 18px;
    }
    .modal-body label{
        font-weight: 300;
        padding-right: 0px;
    }
    .modal-body form{
        margin-top: 25px;
    }
    .modal-footer > a {
        margin: 0 5px;
    }
</style>
<!--Map -->
<div id="map" style="height: 100% !important;"></div>
<div class="modal fade" id="createModal" role="dialog" data-backdrop="static" 
   data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>プラン｜新規作成</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route'=>'flight.create', 'id'=>'frmCreate','class'=>'form-horizontal', 'files'=>true)) !!}
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="hf-email">タイトル :</label>
                        <div class="col-md-9">
                            <input id="name" class="form-control" type="text" name="name">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <a id="btnYes" onclick="checkValidate()" href="javascript:;"><img src="{{ URL::to('assets/img/icSave.png') }}"></a>
                <a href="{!! route('flight.list') !!}" ><img src="{{ URL::to('assets/img/icCancel.png') }}"></a>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('scripts')
<script type="text/javascript">
    $('#createModal').modal('show');
    function checkValidate() {
        var name = $('#name');
        if(!name.val()) {
            name.closest('.form-group').removeClass('has-success').addClass('has-error');
            if($('#frmCreate .col-md-9').find('.help-block').length == 0){
                $("#frmCreate .col-md-9").append( '<span class="help-block"><strong>フライト名を入力してください</strong></span>');
            }
            e.preventDefault();
        } else {
            document.getElementById('frmCreate').submit();
        }
    };
</script>

<script> 
    var map;
    var latLngBounds;
    var gmarkers = [];
    var glocations = [];
    var marker = [];
    var total = 0;
    var id = [];
    var hight = [];
    var flight;
    var object =[];
    var center;
    var c_lat = 35.65325;
    var c_long = 139.772049;

    function initialize() {
        var location = new Array();
        
        navigator.geolocation.getCurrentPosition(function(position) {
            var _lat = position.coords.latitude;
            var _long = position.coords.longitude;
            center = new google.maps.LatLng(_lat, _long);
            map = new google.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 14,
                //mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeId: 'satellite',
                disableDefaultUI: true,
            });
        }, function() {
            center = new google.maps.LatLng(c_lat, c_long);
            map = new google.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 14,
                //mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeId: 'satellite',
                disableDefaultUI: true,
            });
        });
              
    }
    google.maps.event.addDomListener(window, 'load', initialize);    
</script>
@endsection