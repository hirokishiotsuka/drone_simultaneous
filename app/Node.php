<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $table = 'nodes';

    protected $fillable = [
    	'flight_id'
    ];

    public function flights(){
    	return $this->belongsTo('App\Flight', 'flight_id');
    }
}
