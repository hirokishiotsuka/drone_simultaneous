<?php

namespace App\Helpers;

use Intervention\Image\ImageManagerStatic as Image;

class ImageHelpers
{

    /**
     * Config image
     *
     * @var array
     */
    protected $config;

    /**
     * Type config
     *
     * @var string
     */
    protected $type;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->config = config('file_uploads');
    }

    /**
     * Get config image.
     *
     * @param string $option Option
     *
     * @return string|array
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function getConfig($option = null)
    {
        return $option === null ? $this->config : (isset($this->config[$option]) ? $this->config[$option] : null);
    }

    /**
     * Set type.
     *
     * @param string $type Type config
     *
     * @return void
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Upload image.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     * @param int                                         $id   Id
     * @param string                                      $type Type to get config
     *
     * @return string
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function upload($file, $id = null, $type)
    {
        $this->setType($type);
        $config = $this->getConfig($type);
        $pathUpload = $this->createPath(sprintf('%s%s', $config['path'], $id));

        return $this->processUpload($file, $pathUpload);
    }

    /**
     * Resize Image.
     *
     * @param array  $sizes    Size to resize image
     * @param Object $image    Image
     * @param string $filename File name
     * @param string $path     Path to resize
     *
     * @return bool
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function resizeImage($sizes, $image, $filename, $path, $isCrop = false)
    {
        foreach ($sizes as $size) {
            $image->backup();
            if ($isCrop) {
                $image = $this->cropImage($image);
            }
            $constraint = null;

            if (empty($size['height']) || $size['height'] == 'x') {
                $size['height'] = 'x';
                $constraint = function($constraint) {
                    $constraint->aspectRatio();
                };
            }
            //resize image to fit given
            $image->resize($size['width'], $size['height'], $constraint);
            $image->save(sprintf('%s/%sx%s_%s', $path, $size['width'], $size['height'], $filename));
            $image->reset();
        }
        return true;
    }

    /**
     * Process upload.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file File
     * @param string                                      $path Path file
     *
     * @return string
     *
     * @throws \RuntimeException
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function processUpload($file, $path)
    {
        try {
            $image = Image::make($file);
            $filename = $this->getFileName($file);
            $isCrop = $this->config[$this->type]['isCrop'];
            if ($this->config[$this->type]['sizes']) {
                $this->resizeImage($this->getSizes(), $image, $filename, $path, $isCrop);
            }
        } catch (\Exception $ex) {
            throw new \RuntimeException('File error type');
        }
        $image->save(sprintf('%s/%s', $path, $filename));

        return $filename;
    }

    /**
     * Process upload.
     *
     * @param Object $image Image
     *
     * @return \Intervention\Image\Image
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    protected function cropImage($image)
    {
        $ratio = 4 / 3;
        if (intval($image->width() / $ratio > $image->height())) {
            // Fit the img to ratio of 4:3, based on the height
            $image->fit(intval($image->height() * $ratio), $image->height());
        } else {
            // Fit the img to ratio of 4:3, based on the width
            $image->fit($image->width(), intval($image->width() / $ratio));
        }
        
        return $image;
    }

    /**
     * Delete image.
     *
     * @param string $path     Path file
     * @param string $fileName File name
     *
     * @return bool
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function deleteImage($path, $fileName)
    {
        $success = \File::delete($path . $fileName);
        return $success;
    }

    /**
     * Delete folder image.
     *
     * @param type $path Path to delete
     *
     * @return bool
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function deleteImageFolder($path)
    {
        $success = \File::deleteDirectory($path);
        return $success;
    }

    /**
     * Create path to save document.
     *
     * @param string $paths Path save
     *
     * @return mixed
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    protected function createPath($paths)
    {
        if (!is_array($paths)) {
            if (!\File::exists($paths)) {
                \File::makeDirectory($paths, $mode = 0777, true, true);
            }
        } else {
            foreach ($paths as $path) {
                if (!\File::exists($path)) {
                    \File::makeDirectory($path, $mode = 0777, true, true);
                }
            }
        }
        return $paths;
    }

    /**
     * Return size(width and height) of image type upload.
     *
     * @return array|boolean
     * @author Viet Hung <hung.pv@neo-lab.vn>
     */
    public function getSizes()
    {
        foreach ($this->config[$this->type]['sizes'] as $size) {
            $def = explode('x', $size);
            if (count($def) >= 2) {
                $sizes[] = array('width' => $def[0], 'height' => $def[1]);
            }
        }
        return $sizes;
    }

    /**
     * Create file name.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file File
     *
     * @return string
     * @author Viet Hung <hung.pv@neo-lab.vn>
     * @version Quynh Nguyen Add String General
     */
    public function getFileName($file)
    {
        $stringGeneral = md5(uniqid($file->getClientOriginalExtension(), true));
        $fileNameOri = preg_replace("/[^a-zA-Z0-9_\-.]/", "", $file->getClientOriginalName());
        $fileName = substr($stringGeneral, 0, 16) . '-' . $fileNameOri;
        return $fileName;
    }

    /**
     * Get image by type.
     *
     * @param string $fileName File name
     * @param string $type     Type
     * @param string $size     Size image
     *
     * @return string
     */
    public function getImage($fileName, $id, $type, $size = '')
    {
        $format = empty($size) ? '%s%s/%s%s' : '%s%s/%s_%s';
        if (!empty($fileName) && (\File::exists(sprintf($format, $this->getConfig($type)['path'], $id, $size, $fileName)))) {
            return asset(sprintf($format, $this->getConfig($type)['path'], $id, $size, $fileName));
        } else {
            return asset($this->config['no_image']);
        }
    }

    /**
     * Upload More Images
     *
     * @param string $fileArr   File Lists
     * @param string $id        ID
     * @param string $type      Type
     *
     * @return string
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     */
    public function uploadMore($fileArr, $id = null, $type)
    {
        try {
            $this->setType($type);
            $config = $this->getConfig($type);
            $pathUpload = $this->createPath(sprintf('%s%s', $config['path'], $id));
            foreach ($fileArr as $key => $file) {
                $fileResponse[$key]['filename'] = $this->processUpload($file, $pathUpload);
                $fileResponse[$key]['path'] = $config['path'].$id;
                $fileResponse[$key]['ext'] = $file->getClientMimeType();
            }
            return $fileResponse;
        } catch (\Exception $e) {
            return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}