<?php
namespace App\Repositories;

use App\Drone;

class DroneRepository extends AbstractRepository
{

    public function __construct(Drone $drone)
    {
        $this->model         = $drone;
    }

}
