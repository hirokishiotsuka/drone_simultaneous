<?php

namespace App\Http\Controllers;

use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\UserRepository;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function __construct (UserRepository $users)
    {
    	parent::__construct();
    	$this->users = $users;
    	$this->currentUser = auth()->user();
    }

    public function edit (Request $request, $id)
    {	
        $data = $request->all();
        if(isset($data['name'])){
            $data['name'] = ltrim($data['name']);
        }
    	$rules = [
    		'name'		=> 'string|max:255',
    		'email'		=> 'email|max:255|unique:users',
    		'password'	=> 'string|min:8|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
    		'telephone'	=> 'numeric',
    	];
    	$messages = [
            'name.max'          => '255文字以内で入力してください',
    		'email.email' 		=> 'メールアドレスの形式が間違っています',
    		'email.unique'		=> '電子メールは既に受け取られました。',
    		'telephone.numeric'	=> '数字を入力してください',
    		'password.min'		=> 'パスワードが間違っています',
            'password.regex'    => 'パスワードが文字と数字を合わせて入力してくだ',
    	];
    	$validator = Validator::make($data, $rules, $messages);
    	if ($validator->passes()) {
    		if ($id == $this->currentUser->id) {
                if($request->has('password')) $data['password'] = bcrypt($data['password']);
	    		$user = $this->users->find($id);
	    		$user_response = $this->users->update($data, $id);
	    		return $user_response;
	    	}
	    	return view('404');
    	}
    	return response()->error($validator->errors()->all(), 300);
    }

    public function detail ()
    {
		$user = $this->users->find($this->currentUser->id);
		return view('users.detail', ['user' => $user]);
    }
}
