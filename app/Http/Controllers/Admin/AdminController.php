<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;
use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\UserRepository;
use App\User;
use App\Company;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
	public function __construct(User $users)
	{
		parent::__construct();
	    $this->users = $users;
	    $this->currentUser = auth()->user();
	    $this->middleware('role:admin');
	}

	/**
	 * Get All User In One Company
	 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
	 * @return Collection 	$users
	 */
	public function listUser()
	{
		$users = $this->users->addSelect(['id','name','email','created_at','updated_at','company_id'])->where('company_id', $this->currentUser->company_id)->get();
		return view('users.lists', ['users' => $users]);
	}

	/**
	 * View Detail Of User
	 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
	 * @param  Int 			$id
	 * @return User Infor 	$user
	 */
	public function editUser($id)
	{
		$user = $this->users->where('id', $id)->where('company_id', $this->currentUser->company_id)->first();
		return view('users.edit', ['user' => $user]);
	}

	/**
	 * Process Edit Information of User
	 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
	 * @param  Request 		$request
	 * @param  Int 			$id
	 * @return Collection 	$user_response
	 */
	public function processEditUSer(Request $request, $id)
	{
		$data = $request->all();
		if(isset($data['name'])){
            $data['name'] = ltrim($data['name']);
        }
        $rules = [
    		'name'		=> 'string|max:255',
    		'email'		=> 'email|max:255|unique:users',
    		'password'	=> 'string|min:8|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
    		'telephone'	=> 'numeric',
    	];
    	$messages = [
            'name.max'          => '255文字以内で入力してください',
    		'email.email' 		=> 'メールアドレスの形式が間違っています',
    		'email.unique'		=> '電子メールは既に受け取られました。',
    		'telephone.numeric'	=> '数字を入力してください',
    		'password.min'		=> 'パスワードが間違っています',
            'password.regex'    => 'パスワードが文字と数字を合わせて入力してくだ',
    	];
    	$validator = Validator::make($data, $rules, $messages);
    	if ($validator->passes()) {
    		$user = $this->users->where('id', $id)->first();
    		if ($user !== null && $user->company_id == $this->currentUser->company_id) {
                if($request->has('password')) $data['password'] = bcrypt($data['password']);
	    		$user->update($data);
	    		// print_r($user_response);
	    		return $user;
	    	}
	    	return view('404');
    	}
    	return response()->error($validator->errors()->all(), 300);
	}

	public function createUserForm()
	{
		$company = Company::addSelect('id', 'name', 'address')->where('id', $this->currentUser->company_id)->first();
		return view('users.create', ['company' => $company]);
	}

	/**
	 * Function Create New User For Company
	 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
	 * @param  Request    $request
	 * @return Collection $users 		Users List
	 */
	public function createUserProcess(Request $request)
	{
		$data = $request->all();
		$company = Company::addSelect('id', 'name', 'address')->where('id', $this->currentUser->company_id)->first();
		$rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'tel' => 'required|numeric',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
            'password_confirmation' => 'required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/',
        ];
        $messages = [
            'name.required'             => '名前を入力してください',
            'name.max'                  => '255文字以内で入力してください',
            'email.email'               => '正しいメールアドレスの形式で入力してください',
            'email.unique'              => '電子メールは既に受け取られました',
            'email.required'            => 'メールアドレスを入力してください',
            'email.max'                 => '255文字以内で入力してください',
            'tel.required'              => '電話番号を入力してください',
            'tel.numeric'               => '数字を入力してください',
            'password.required'         => 'パスワードを入力してください',
            'password.min'              => '8文字以上入力してください',
            'password.regex'            => 'パスワードが文字と数字を合わせて入力してくだ',
            'password_confirmation.required' => 'パスワード確認を入力してください',
            'password_confirmation.min' => '8文字以上入力してください',
            'password_confirmation.regex' => 'パスワードが文字と数字を合わせて入力してくだ',
            'password.confirmed'        => 'パスワード確認が間違っています',
        ];
        $validator = Validator::make($data, $rules, $messages);
        $validator = Validator::make($data, $rules, $messages);
    	if ($validator->passes()) {
    		User::create([
	            'name' => $data['name'],
	            'email' => $data['email'],
	            'telephone' => $data['tel'],
	            'password' => bcrypt($data['password']),
	            'company_id' => $company->id,
	        ]);
	        $users = $this->users->addSelect(['id','name','email','created_at','updated_at','company_id'])->where('company_id', $this->currentUser->company_id)->get();
			return view('users.lists', ['users' => $users]);
    	}
    	$messagesError = $validator->errors();
    	return view('users.create', ['company' => $company, 'errors' => $messagesError]);
	}

	public function deletesUserProcess(Request $request)
	{
		try {
			$data = $request->all();
			$rules = [
	            'id.*' => 'required|numeric',
	        ];
	        $validator = Validator::make($data, $rules);
	        if ($validator->passes()) {
	        	foreach ($data['id'] as $key => $id) {
	        		$user = $this->users->addSelect('id','company_id')->where('id', $id)->first();
	        		if($user !== null && $user->company_id == $this->currentUser->company_id){
	        			$test = User::destroy($user->id);
	        		} else return response()->error('You don\'t have permission', 404);
	        	}
	        	return response()->success($data['id']);
	        } else return response()->error($validator->errors()->all(), 300);
		} catch (\Exception $e) {
			return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
