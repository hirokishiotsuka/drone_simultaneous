<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home', function () {
    return redirect('admin/home');
});

/**
 * API's Routes
 */
Route::group(['prefix' => 'api', 'middleware' => ['throttle:5000']], function(){
	Route::post('register', 'APIController@register');
	Route::post('login', 'APIController@login');
	Route::post('logout', 'APIController@logout');
	Route::post('flight/create', 'FlightController@createAPI'); // Create New Flight
	Route::post('flight/list', 'FlightController@listAPI'); // Get Flight List With Keyword
	Route::post('flight/node/create', 'FlightController@createNodeAPI'); // Create Point
	Route::post('flight/node/home', 'FlightController@setHomeAPI'); // Set Home Position
	Route::post('flight/log', 'FlightController@sendLogAPI'); // Send Log of Flight
	Route::post('flight/delete', 'FlightController@deleteAPI'); // Delete Flight
	Route::post('flight/point/upload', 'HelperController@uploadAtPointAPI'); // Upload Media Each Point
	Route::post('flight/media', 'HelperController@mediaAPI');
	Route::post('flight/media/deletes', 'API\FlightController@deleteAllMedias'); // Detele All Medias of Flight
	Route::post('flight/edit', 'API\FlightController@edit'); // Edit Info of Flight
	Route::get('flight/log/{id}', 'API\FlightController@getLogs')->where('id', '[0-9]+'); // Get Logs flight
	Route::get('flight/replay/{id}', 'API\FlightController@getLogsReplay')->where('id', '[0-9]+'); // Get Logs flight
	Route::post('flight/detail', 'FlightController@detailAPI'); // Get Detail Flight
	Route::get('flight/datelist/{id}', 'API\FlightController@dateList')->where('id', '[0-9]+'); // Get Date List
	Route::post('flight/deletes', 'API\FlightController@deleteMore');
	Route::post('flight/datelist/deletes/{id}', 'API\FlightController@deleteDates')->where('id', '[0-9]+'); // Delete Date Log of Flight
  	Route::post('flight/gohome', 'API\FlightController@goHome');
  	Route::post('flight/startflight', 'API\FlightController@startFlight');
  	Route::post('media/deletes', 'API\FlightController@deleteMedias'); // Detele Media

	Route::group(['middleware' => 'jwt-auth'], function () {
        Route::post('flight/upload', 'HelperController@uploadAPI'); // Upload Media for Flight
    });
	// Route::post('node/create', 'NodeController@createNodeAPI');
});

Route::group(['prefix' => 'admin'], function(){
	Route::auth();

	Route::group( ['middleware' => 'auth'], function()
	{
		Route::get('home', [
		    'as' => 'admin.home', 'uses' => 'FlightController@lists'
		]);

		/**
		 * Log's Routes
		 */
		Route::get('logs/delete/{id}', 'FlightLogController@delete');
		Route::post('logs/deletes', array('as'=>'logs.deletes','uses'=>'FlightLogController@deleteMore'));
		Route::get('logs', array('as'=>'logs', 'uses'=>'FlightLogController@index'));
		Route::get('log-replay/{id}', 'FlightLogController@replay')->where('id', '[0-9]+');
		Route::get('logs/{id}', 'FlightLogController@datelist')->where('id', '[0-9]+');

		/**
		 * Flight's Routes
		 */
		Route::get('flight/list', array('as'=>'flight.list', 'uses'=>'FlightController@lists'));

		Route::get('flight/edit/{id}', 'FlightController@edit')->where('id', '[0-9]+');

		Route::post('flight/proEdit', 'FlightController@proEdit');
		Route::get('flight/{id}', array('as'=>'flight.detail', 'uses'=>'FlightController@detail'))->where(['id' => '[0-9]+', 'user_id' => '[0-9]+']);
		Route::post('flight/{id}', array('as'=>'flight.postDetail', 'uses'=>'FlightController@postDetail'))->where('id', '[0-9]+');

		Route::get('flight/create', 'FlightController@create');
		Route::post('flight/proCreate', array('as'=>'flight.create', 'uses'=>'FlightController@proCreate'));

		Route::post('flight/deletes', 'FlightController@deleteMore');
		Route::get('flight/delete/{id}', 'FlightController@delete');

		Route::post('flight/{id}/{user_id}/edit/{node_id}', 'FlightController@postEditNode')->where('id', '[0-9]+')->where('node_id', '[0-9]+');
		Route::post('flight/{id}/{user_id}/realtime-edit/{node_id}', 'FlightController@postEditNodeRealTime')->where('id', '[0-9]+')->where('node_id', '[0-9]+');
		Route::post('flight/createNode', 'FlightController@postCreateNode');

		Route::get('realtime', 'FlightController@getRealTimeList');
		Route::get('realtime/{id}', array('as'=>'flight.realtimes.detail', 'uses'=>'FlightController@getRealTimeDetail'))->where('id', '[0-9]+');

		/**
		 * User's Routes
		 */
		Route::get('profile', 'UserController@detail');
		Route::post('user/edit/{id}', array('as' => 'user.edit', 'uses'=>'UserController@edit'))->where('id', '[0-9]+');

		/**
		 * Admin Panel
		 */
		Route::get('user', 'Admin\AdminController@listUser');
		Route::get('user/{id}', 'Admin\AdminController@editUser')->where('id', '[0-9]+');
		Route::post('user/{id}', ['as' => 'admin.user.edit', 'uses' => 'Admin\AdminController@processEditUSer'])->where('id', '[0-9]+');
		Route::get('user/create', 'Admin\AdminController@createUserForm');
		Route::post('user/create', 'Admin\AdminController@createUserProcess');
		Route::post('user/deletes', 'Admin\AdminController@deletesUserProcess');

		/**
		 * Media's Routes
		 */
		Route::get('media', 'MediaController@listFlight');
		Route::get('media/{id}', 'MediaController@datelist')->where('id', '[0-9]+');
		Route::get('log-media/{id}', 'MediaController@listMedia')->where('id', '[0-9]+');

        /**
         * Drone body
         */
        Route::get('drone', 'DroneController@lists');
        Route::get('drone/create', 'DroneController@createDroneForm');
        Route::post('drone/create', 'DroneController@createDroneProcess');
        Route::get('drone/{id}', 'DroneController@editDrone')->where('id', '[0-9]+');
        Route::post('drone/{id}', 'DroneController@editDroneProcess')->where('id', '[0-9]+');
        Route::post('drone/deletes', 'DroneController@deleteProcess');

	});
});
