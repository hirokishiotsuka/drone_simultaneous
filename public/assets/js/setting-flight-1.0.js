function ZoomControl(controlDiv, map) {
    // Creating divs & styles for custom zoom control
    controlDiv.style.padding = '5px';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.style.borderWidth = '1px';
    controlWrapper.style.cursor = 'pointer';
    controlWrapper.style.textAlign = 'center';
    controlWrapper.style.width = '70px';
    controlWrapper.style.height = '150px';
    controlDiv.appendChild(controlWrapper);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.style.width = '50px';
    zoomInButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomInButton.style.backgroundImage = 'url("/assets/img/icZoomOut.png")';
    controlWrapper.appendChild(zoomInButton);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.style.width = '50px';
    zoomOutButton.style.height = '45px';
    /* Change this to be the .png image you want to use */
    zoomOutButton.style.backgroundImage = 'url("/assets/img/icZoomin.png")';
    controlWrapper.appendChild(zoomOutButton);

    var locationButton = document.createElement('div');
    locationButton.style.width = '50px';
    locationButton.style.height = '60px';
    /* Change this to be the .png image you want to use */
    locationButton.style.backgroundImage = 'url("/assets/img/icLocation.png")';
    controlWrapper.appendChild(locationButton);

    // Setup the click event listener - zoomIn
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
        map.setZoom(map.getZoom() + 1);
    });

    // Setup the click event listener - zoomOut
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
        map.setZoom(map.getZoom() - 1);
    });

    google.maps.event.addDomListener(locationButton, 'click', function() {
        var l_latlng = new google.maps.LatLng(c_lat, c_long);
        map.setCenter(l_latlng);
        setHomeLocation(l_latlng);
    });
}

function setHomeLocation(c_latlng) {
    var marker = new google.maps.Marker({
        position: c_latlng,
        map: map,
        label: {
            text: 'A',
            color: "#2396e5",

        },
        draggable:false,
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            strokeColor: '#ECFCF8',
            fillColor: '#2396e5',
            fillOpacity: 1,
            strokeWeight: 5,
        }
    });
    marker.setMap(map);
}

function createNode(flight, _lat, _long, latlng, center, pos, is_subuser, user_id) {
    var subuser = is_subuser || false;
    var _user_id = user_id || 1;
    var _url = '/admin/flight/createNode?id=' + flight + '&user_id=' + user_id + '&lat=' + _lat + '&long=' + _long;
    $.ajax({
        url: _url,
        type: "POST",
        dataType: "JSON",
        success: function(res){
            if(res.status) {
              var str = res.data.id + ',' + res.data.lat + ','+ res.data.lng + ',' + res.data.height+'.0000000,0,0,0,'+ res.data.user_id;
              subuser ? object_2.push(str.split(',')) : object_1.push(str.split(','));
              var obj = str.split(',');
              var type = 'news';
              addMarker(latlng, map, obj, center, pos, type, subuser, _user_id);
            } else {
            }
        }
    });
}

function loadMarkers(markers, object, is_subuser) {
    var locations = new Array;
    var length = markers.length;
    var _is_subuser = is_subuser || false;
    if(length > 0){
        if(is_home && length >= 21 ) {
            length = 21;
        }else if(length > 20) {
            length = 20;
        }
        for (var i=0; i < length; i++){
            locations.push(markers[i]);
            j = i + 1;
            var start = markers[i];
            var end = markers[j];

            /*Start*/
            var startCoords = start.split(",");
            var lat_start = startCoords[0];
            var lgn_start = startCoords[1];
            var latlng_start = new google.maps.LatLng(lat_start,lgn_start);
            var obj = object[i];
            if(!is_home) {
                addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);
            }else{
                if(i== 0) {
                  center = 1;
                  addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);

                }else{
                    center = 0;
                    addMarker(latlng_start,map,obj,center,i, type, _is_subuser, user_id);
                }
            }

            if(length == 1) {
                latLngBounds = drawLine(lat_start, lgn_start, lat_start, lgn_start);
            }
            if(j != length) {
                /*End*/
                var endCoords = end.split(",");
                var lat_end = endCoords[0];
                var lgn_end = endCoords[1];
                latLngBounds = drawLine(lat_start, lgn_start, lat_end, lgn_end);
            }

        }
        map.fitBounds(latLngBounds);
        if (_is_subuser) {
          gmarkers_2 = locations;
          glocations_2 = locations;
        } else {
          gmarkers_1 = locations;
          glocations_1 = locations;
        }
    }
}

function moveMarkerDrawLine(marker_1, marker_2, marker_3,pos_node) {
    marker_2 = marker_2.split(",");
    lat_2 = marker_2[0];
    lgn_2 = marker_2[1];

    if(marker_1 != null){
        marker_1 = marker_1.split(",");
        lat_1 = marker_1[0];
        lgn_1 = marker_1[1];
        drawLine(lat_1, lgn_1, lat_2, lgn_2,pos_node-1);
    }
    if(marker_3 != null){
        marker_3 = marker_3.split(",");
        lat_3 = marker_3[0];
        lgn_3 = marker_3[1];
        drawLine(lat_2, lgn_2, lat_3, lgn_3,pos_node);
    }
}

function drawLine(lat_start, lgn_start, lat_end, lgn_end, pos_node) {
    var startPt = new google.maps.LatLng(lat_start,lgn_start);
    var endPt = new google.maps.LatLng(lat_end,lgn_end);
    calcRoute(startPt, endPt, pos_node);
    latLngBounds.extend(startPt);
    latLngBounds.extend(endPt);
    return latLngBounds;
}
function calcRoute(source,destination,pos_node){
    var polyline = new google.maps.Polyline({
        path: [source, destination],
        strokeColor: '#FFF',
        strokeWeight: 2,
        strokeOpacity: 1
    });
    polyline.setMap(map);
    if(pos_node!=null){
        polylines[pos_node] =  polyline;
    }else{
        polylines.push(polyline);
    }
}
function addMarker(latlng, map, obj, center, pos, type, subuser, user_id) {
  var _subuser = subuser || false;
  var _user_id = user_id || 1;
  var operator = '';
  if (_subuser) {
    if(center == 1) {
        title = 'H';
        color = '#f7255c';
        operator = 'drone-admin2';
    }else{
        title = (++inc_2).toString();
        color = '#00ff84';
        operator = '';
    }
  } else {
    if(center == 1) {
        title = 'H';
        color = '#f7255c';
        operator = 'drone-admin1';
        ++inc_1;
    }else{
        title = (++inc_1).toString();
        color = '#28b8fb';
        operator = '';
    }
  }
  // var title = '';
  // if(center != 1) {
  //   if (_subuser) {
  //     title = (++inc_2).toString();
  //   } else {
  //     title = (++inc_1).toString();
  //   }
  // } else {
  //   title = 'H';
  // }
  var image = {
      url: '/assets/img/icon10_v2.png',
      anchor: new google.maps.Point(16,16),
      origin: new google.maps.Point(0,0),
      size: new google.maps.Size(32, 32),
      scale: 1,
  };
  // if(is_home && (inc_1 == 0 || inc_2 == 0)) {
  //     title = 'H';
  // }
  if(title == 10){
      var marker = new google.maps.Marker({
        position:  latlng,
        map: map,
        draggable: true,
        icon: image,
      });
  }else{
      var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          label: {
              text: title,
              color: "#FFF",
              fontWeight: "bold",
          },
          draggable:true,
          icon: {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 15,
              strokeColor: '#fff',
              strokeWeight: 2,
              fillColor: color,
              fillOpacity: 1
          }
      });
  }
  var lat = latlng.lat().toFixed(7);
  var lgn = latlng.lng().toFixed(7);
  marker.setMap(map);

  if(is_home && inc_1 == 0) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        label: {
            text: operator + "\n",
            color: "#FFF",
            fontWeight: "bold"
        },
        icon: {
          url: "http://maps.gstatic.com/mapfiles/markers2/arrowtransparent.png",
          size: new google.maps.Size(50, 50)
        },
        draggable:true,
        zIndex: 1,
        optimized: false
    });
    marker.setMap(map);
    // $('#map').append("<span style='position:relative; left:calc(" + _homeLeft + " - 2rem); top:calc(" + _homeTop + " - 2rem); color: #fff; text-align: center; text-shadow: 0 0 3px #000; font-weight: bold; font-size: 14px;'>" + operator + "</span>");
  }
  if(is_home && inc_2 == 0) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        label: {
            text: operator + "\n",
            color: "#FFF",
            fontWeight: "bold"
        },
        icon: {
          url: "http://maps.gstatic.com/mapfiles/markers2/arrowtransparent.png",
          size: new google.maps.Size(50, 50)
        },
        draggable:true,
        zIndex: 1,
        optimized: false
    });
    marker.setMap(map);
    // $('#map').append("<span style='position:relative; left:calc(" + _homeLeft + " - 2rem); top:calc(" + _homeTop + " - 2rem); color: #fff; text-align: center; text-shadow: 0 0 3px #000; font-weight: bold; font-size: 14px;'>" + operator + "</span>");
  }

  var _id = obj[0];
  var _height = obj[3];
  var _capture = obj[4];
  var _rightAngle = obj[5];
  var _timeStay = obj[6];
  var _user_id = obj[7];
  if(_capture == 0){
      var _capture_convert = "off";
  }else var _capture_convert = "on";
  if(_rightAngle == 0){
      var _rightAngle_convert = "off";
  }else var _rightAngle_convert = "on";

  if (_subuser) {
    $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes_sub[]" value ="'+ lat + ',' + lgn +',' + _height + ','+_capture+','+_rightAngle+','+_timeStay+','+_user_id+'" />');
  } else {
    $('#jsNodes').append('<input type="hidden" attr-point-id="' +_id +'" id="node" name="nodes[]" value ="'+ lat + ',' + lgn +',' + _height + ','+_capture+','+_rightAngle+','+_timeStay+','+_user_id+'" />');
  }
  var _delete = '<a class="jsDeleteNode" onClick="deleteMarker('+_id+');"><img src="/assets/img/icTrash.png" alt="" style="float: right;"/></a>';
  if(title == 'H') {
      _delete = '';
  }

  var container = document.createElement("FORM");
  container.action = '/admin/flight/' + flight + '/' + _user_id + '/edit/' + _id;
  container.method = "POST";
  container.setAttribute("id", "frmEditNode");

  var titleNode = document.createElement("DIV");
  titleNode.setAttribute("class", "form-group");
  titleNode.innerHTML = '<h4 class="w-90p">ポイント ' + title + _delete + '</h4>' + '<hr>';

  var latGroup = document.createElement("DIV");
  latGroup.setAttribute("class", "form-group");
  latGroup.innerHTML =   '<label class="col-md-3 labelForInput">経度: </label>' +
                          '<div class="input-group col-md-8">' +
                              '<input type="number" step="0.0000001" class="height-form-control" name ="lat" min="-90" max="90" id="lat" placeholder="0" value="'+lat+'">' +
                              '<span class="input-group-addon" style="width: 0;">°</span>' +
                          '</div>';

  var lngGroup = document.createElement("DIV");
  lngGroup.setAttribute("class", "form-group");
  lngGroup.innerHTML =    '<label class="col-md-3 labelForInput">緯度: </label>' +
                          '<div class="input-group col-md-8">' +
                              '<input type="number" step="0.0000001" class="height-form-control" name ="lgn" min="-180" max="180" id="lgn" placeholder="0" value="'+lgn+'">' +
                              '<span class="input-group-addon" style="width: 0;">°</span>' +
                          '</div>';

  var heightGroup = document.createElement("DIV");
  heightGroup.setAttribute("class", "form-group");
  heightGroup.innerHTML = '<label class="col-md-3 labelForInput">高度</label>' +
                          '<div class="input-group col-md-8">' +
                              '<input type="number" class="height-form-control" name ="height" min="0" max="150" id="height" placeholder="0" value="'+_height+'">' +
                              '<span class="input-group-addon" style="width: 0;">m</span>' +
                          '</div>';

  var timeStayGroup = document.createElement("DIV");
  timeStayGroup.setAttribute("class", "form-group");
  timeStayGroup.innerHTML = '<label class="col-md-3 labelForInput">滞空</label>' +
                          '<div class="input-group col-md-8">' +
                              '<input type="number" class="height-form-control" name ="timeStay" min="0" max="60" id="timeStay" placeholder="0" value="'+_timeStay+'">' +
                              '<span class="input-group-addon" style="width: 0;">s</span>' +
                          '</div>';

  var captureGroup = document.createElement("DIV");
  captureGroup.setAttribute("class", "form-group");
  captureGroup.innerHTML = '<label class="col-md-3">撮影: </label><strong class="onOffSwitcher">ON <span class="wrapCaptureImage capture_'+_capture_convert+'" onclick="captureImageSwitcher(this)"><img src="/assets/img/'+_capture_convert+'_switcher.png" class="captureImageSwitcher"/><input type="hidden" name="capture" value="'+_capture+'"/></span> OFF</strong>';

  var rightAngleGroup = document.createElement("DIV");
  rightAngleGroup.setAttribute("class", "form-group");
  rightAngleGroup.innerHTML = '<label class="col-md-3">直角<img class="icon_right_angle" src="/assets/img/arrow.png">: </label><strong class="onOffSwitcher">ON <span class="wrapCaptureImage capture_'+_rightAngle_convert+'" onclick="rightAngleSwitcher(this)"><img src="/assets/img/'+_rightAngle_convert+'_switcher.png" class="captureImageSwitcher"/><input type="hidden" name="rightAngle" value="'+_rightAngle+'"/></span> OFF</strong>';

  var buttonGroup = document.createElement("DIV");
  buttonGroup.setAttribute("class", "form-group center");
  buttonGroup.innerHTML = '<a href="javascript:;" class="jsClose"  onclick="return checkform();"><img src="/assets/img/save.png" alt="キャンセル" /></a> ' + '<a href="javascript:;" class="jsClose"  onclick="closeAllInfoWindows();"><img src="/assets/img/cancel.png" alt="キャンセル" /></a>';

  container.appendChild(titleNode);
  container.appendChild(latGroup);
  container.appendChild(lngGroup);
  container.appendChild(heightGroup);
  container.appendChild(timeStayGroup);
  container.appendChild(captureGroup);
  container.appendChild(rightAngleGroup);
  container.appendChild(buttonGroup);

  var infowindow = new google.maps.InfoWindow({
      content: container,
      maxWidth: 500
  });

  google.maps.event.addListener(marker, 'click', function() {
      closeAllInfoWindows();
      infowindow.open(map,marker);
      infoWindows.push(infowindow);
  });

  /*Move marker*/
  google.maps.event.addListener(marker, 'dragend', function() {
      var infoNodeUpdate = $('#jsNodes #node[attr-point-id="'+_id+'"]').attr("value");

      if(is_home) {
          title = pos;
      }else{
          title = pos;
      }
      if(title == 'H') {
          var number = 0;
          polylines[0].setMap(null);
      }else{
          var number = parseInt(pos);
          for(i= number - 1; i <= number ; i++) {
            if(polylines[i] != null)
                polylines[i].setMap(null);
        }
      }

      var lat = marker.position.lat().toFixed(7);
      var lgn = marker.position.lng().toFixed(7);

      var container = document.createElement("FORM");
      container.action = '/admin/flight/'+flight+ '/' + _user_id + '/edit/'+_id;
      container.method = "POST";
      container.setAttribute("id", "frmEditNode");
      var latGroup = document.createElement("DIV");
      latGroup.setAttribute("class", "form-group");
      latGroup.innerHTML = '<label class="col-md-3 labelForInput">経度: </label>' +
                           '<div class="input-group col-md-8">' +
                               '<input type="number" step="0.0000001" class="height-form-control" name ="lat" min="-90" max="90" id="lat" placeholder="0" value="'+lat+'">' +
                               '<span class="input-group-addon" style="width: 0;">°</span>' +
                           '</div>';

      var lngGroup = document.createElement("DIV");
      lngGroup.setAttribute("class", "form-group");
      lngGroup.innerHTML = '<label class="col-md-3 labelForInput">緯度: </label>' +
                           '<div class="input-group col-md-8">' +
                               '<input type="number" step="0.0000001" class="height-form-control" name ="lgn" min="-180" max="180" id="lgn" placeholder="0" value="'+lgn+'">' +
                               '<span class="input-group-addon" style="width: 0;">°</span>' +
                           '</div>';

      container.appendChild(titleNode);
      container.appendChild(latGroup);
      container.appendChild(lngGroup);
      container.appendChild(heightGroup);
      container.appendChild(timeStayGroup);
      container.appendChild(captureGroup);
      container.appendChild(rightAngleGroup);
      container.appendChild(buttonGroup);

      infowindow.setContent(container);

      marker_2 = lat+','+lgn;

      if (_subuser) {
        marker_1 = gmarkers_2[number - 1];
        gmarkers_2[title] = marker_2;
        marker_3 = gmarkers_2[number + 1];
      } else {
        marker_1 = gmarkers_1[number - 1];
        gmarkers_1[title] = marker_2;
        marker_3 = gmarkers_1[number + 1];
      }
      moveMarkerDrawLine(marker_1, marker_2, marker_3, number);
      $('#jsNodes #node[attr-point-id="'+_id+'"]').val(lat+',' + lgn +',' +_height+','+_capture+','+_rightAngle+','+_timeStay+','+_user_id);
      if(init || type == 'news'){
          $('.jsSave').trigger('click');
      }
  });

  //*Hidden Background of Google API
  google.maps.event.addListener(infowindow, 'domready', function() {
     var iwOuter = $('.gm-style-iw');
     var iwBackground = iwOuter.prev();

     iwBackground.children(':nth-child(2)').css({'display' : 'none'});
     iwBackground.children(':nth-child(4)').css({'display' : 'none'});
  });
}

function pinSymbol(_color) {
    return {
       path: google.maps.SymbolPath.CIRCLE,
        scale: 10,
        strokeColor: _color,
        fillColor: _color,
        fillOpacity: 1
    };
}
function closeAllInfoWindows() {
    for (var i=0;i<infoWindows.length;i++) {
      infoWindows[i].close();
    }
}

function deleteMarker(id){
    $('#jsNodes #node[attr-point-id="'+id+'"]').remove();
    $('.jsSave').trigger('click');
}

function isCategoryA(category_code) {
    return category_code == 'A';
}

function isCategoryB(category_code) {
    return category_code == 'B';
}

function isCategoryCityLimit(category_code) {
    return category_code == 'city_limit';
}

function categoryName(category_code) {
    if(isCategoryA(category_code)){
        return category_a_name;
    }else if(isCategoryB(category_code)){
        return category_b_name;
    }else{
        return category_city_limit_name;
    }
}

function no_fly_zone(map, item, category) {
    color = '#f39c12';
    mile = 5; // 5 miles
    image = red_image;
    if(isCategoryB(category)) {
        color = '#2d89ef';
        mile = 1;
        image = blue_image;
    }

    var marker_latlng = new google.maps.LatLng(item[0], item[1]);
    var noFlyZone = new google.maps.Marker({
        position: marker_latlng,
        popup: true,
        icon: image,
        map: map
    });
    if(isCategoryCityLimit(category)){
        noFlyZone.setVisible(false)
    }

    text = item[2] + " (" + categoryName(category) + ")";
    if(isCategoryA(category)){
        text += "<br/><small>No Fly Zone<br/>Height Restriction Zone</small>"
    }else{
        text += "<br/><small>No Fly Zone</small>"
    }
    var infowindow = new google.maps.InfoWindow({
        content: text
    });

    if(!isCategoryCityLimit(category)){
        var options = {
          strokeColor: color,
          strokeOpacity: 0.6,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.4,
          map: map,
          center: noFlyZone.position,
          radius: mile * 1609.344
        };
        // Add the circle for this city to the map.
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }

    if(isCategoryA(category)){    // Category A
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: noFlyZone.position,
            radius: 1.5 * 1609.344
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
    }else if(isCategoryCityLimit(category)){
        var options = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: map,
            center: marker_latlng,
            radius: item[5] * 1000
        };
        cityCircle = new google.maps.Circle(options);
        noFlyZones.push(cityCircle);
        noFlyZones.push(noFlyZone);
        google.maps.event.addListener(cityCircle, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    if(!isCategoryCityLimit(category)){
        google.maps.event.addListener(noFlyZone, 'click', function() {
            if(opened_info){ opened_info.close(); }
            opened_info = infowindow;
            infowindow.open(map,noFlyZone);
        });
    }
    //*Hidden Background of Google API
    google.maps.event.addListener(infowindow, 'domready', function() {
       var iwOuter = $('.gm-style-iw');
       var iwBackground = iwOuter.prev();

       iwBackground.children(':nth-child(2)').css({'display' : 'none'});
       iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    });
}
function noFlyZoneSwicher(_this){
    var _this = $(_this);
    if(_this.hasClass('noFlyZone_on')) {
        _this.addClass('noFlyZone_off').removeClass('noFlyZone_on');
        _this.find('img').attr('src','/assets/img/off_switcher2.png');
        $('#weather-content #noFlyZoneSwicher').addClass('geoFilter_off').removeClass('geoFilter_on');
        $('#weather-content #noFlyZoneSwicher').find('img').attr('src','/assets/img/off_switcher2.png');
        setMapClearNoFlyZone(null);
        noFlyZones = [];
    }else if (_this.hasClass('noFlyZone_off')){
        _this.addClass('noFlyZone_on').removeClass('noFlyZone_off');
        _this.find('img').attr('src','/assets/img/on_switcher2.png');
        $('#weather-content #noFlyZoneSwicher').addClass('noFlyZone_on').removeClass('noFlyZone_off');
        $('#weather-content #noFlyZoneSwicher').find('img').attr('src','/assets/img/on_switcher2.png');
        drawNoFlyZone();
    }
}
/**
 * Draw exist geo filter zones and show tools draw new geo filter zone
 * This function will be called when Geo Filter Switch ON
 * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
 */
function geoFilterSwicher(_this){
    var _this = $(_this);
    if(_this.hasClass('geoFilter_on')) {
        _this.addClass('geoFilter_off').removeClass('geoFilter_on');
        _this.find('img').attr('src','/assets/img/off_switcher2.png');
        $('#weather-content #geoFilterSwicher').addClass('geoFilter_off').removeClass('geoFilter_on');
        $('#weather-content #geoFilterSwicher').find('img').attr('src','/assets/img/off_switcher2.png');
        $('.jsDrawGeoFilter').addClass('not-allowed');
        setMapClearGeoFilterZone(null);
    }else if (_this.hasClass('geoFilter_off')){
        _this.addClass('geoFilter_on').removeClass('geoFilter_off');
        _this.find('img').attr('src','/assets/img/on_switcher2.png');
        $('#weather-content #geoFilterSwicher').addClass('geoFilter_on').removeClass('geoFilter_off');
        $('#weather-content #geoFilterSwicher').find('img').attr('src','/assets/img/on_switcher2.png');
        $('.jsDrawGeoFilter').removeClass('not-allowed');
        $('.jsDrawGeoFilter').on('click', function(){
            var _this = $(this);
            if(_this.hasClass('unactive')) {
                _this.addClass('active').removeClass('unactive');
                _this.find('img').attr('src','/assets/img/geofiliter_gray.png');
                if(flyAreaWrapper==undefined){
                    drawingManager = new google.maps.drawing.DrawingManager({
                        drawingMode: google.maps.drawing.OverlayType.POLYGON,
                        drawingControl: false,
                        drawingControlOptions: {
                            position: google.maps.ControlPosition.BOTTOM_CENTER,
                            drawingModes: [google.maps.drawing.OverlayType.POLYGON]
                        },
                        polygonOptions: {
                            strokeColor: '#052eff',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#052eff',
                            fillOpacity: 0.2,
                            draggable: true,
                            editable: true
                        }
                    });
                    drawingManager.setMap(map);
                    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(flyAreaPolygon) {
                        flyAreaWrapper = flyAreaPolygon;
                        polygonBounds = flyAreaWrapper.getPath();
                        var pointsArray = []; //list of polyline points
                        for (var i =0; i < polygonBounds.getLength(); i++) {
                            var xy = polygonBounds.getAt(i); //LatLang for a polyline
                            var item = { "lat":xy.lat(), "lng":xy.lng()};
                            $('#jsNodes').append('<input type="hidden" id="flyAreaWrapper" name="flyAreaPoint[]" value ="'+xy.lat().toFixed(7)+','+xy.lng().toFixed(7)+'" />');
                            pointsArray.push(item);
                        }

                        var polygon = {"points" : pointsArray};
                        geoFilterZones.push(flyAreaWrapper);
                        if(_this.hasClass('active')) {
                            _this.addClass('unactive').removeClass('active');
                            _this.find('img').attr('src','/assets/img/geofiliter_blue.png');
                            drawingManager.setMap(null);
                        }
                        google.maps.event.addListener(flyAreaWrapper, 'dragend', function() {
                            $( "#jsNodes #flyAreaWrapper" ).remove();
                            polygonBounds = flyAreaWrapper.getPath();
                            var pointsArray = []; //list of polyline points
                            for (var i =0; i < polygonBounds.getLength(); i++) {
                                var xy = polygonBounds.getAt(i); //LatLang for a polyline
                                var item = { "lat":xy.lat(), "lng":xy.lng()};
                                $('#jsNodes').append('<input type="hidden" id="flyAreaWrapper" name="flyAreaPoint[]" value ="'+xy.lat().toFixed(7)+','+xy.lng().toFixed(7)+'" />');
                                pointsArray.push(item);
                            }
                        }.bind(this));

                        google.maps.event.addListener(flyAreaWrapper, 'click', function(e) {
                            var max = 20;
                            if(!is_home || init) {
                                max = 19;
                            }
                            if (is_subuser) {
                              if(inc_2 - 1 == max) return;
                            } else {
                              if(inc_1 - 1 == max) return;
                            }
                            if(!is_subuser && inc_1 <= max && $('.jsCreatNode').hasClass('active')) {
                                if(glocations_1.length == 0) {
                                    var lat = e.latLng.lat().toFixed(7);
                                    var lgn = e.latLng.lng().toFixed(7);
                                    var latlng = new google.maps.LatLng(lat,lgn);
                                    if(is_home)  {
                                        var center = 1;
                                    }else{
                                        var center = 0;
                                    }
                                    createNode(flight, lat, lgn, latlng, center, inc_1, is_subuser, user_id);
                                }else{
                                    var _ok =  false;
                                    if(is_home && glocations_1.length < 11 ) {
                                        _ok = true;
                                    }else if(glocations_1.length < 10) {
                                        _ok = true;
                                    }
                                    if(_ok) {
                                        var lat = e.latLng.lat().toFixed(7);
                                        var lgn = e.latLng.lng().toFixed(7);
                                        var latlng = new google.maps.LatLng(lat,lgn);
                                        var center = 0;
                                        createNode(flight, lat, lgn, latlng, center, inc_1, is_subuser, user_id);

                                        length_1 = glocations_1.length -1;
                                        var endCoords = glocations_1[length].split(",");
                                        var lat_end = endCoords[0];
                                        var lgn_end = endCoords[1];

                                        drawLine(lat, lgn, lat_end, lgn_end)
                                    }
                                }
                                glocations_1.push(lat+','+lgn);
                            } else if(is_subuser && inc_2 <= max && $('.jsCreatNode').hasClass('active')) {
                                user_id = 2;
                                if(glocations_2.length == 0) {
                                    var lat = e.latLng.lat().toFixed(7);
                                    var lgn = e.latLng.lng().toFixed(7);
                                    var latlng = new google.maps.LatLng(lat,lgn);
                                    if(is_home) {
                                        var center = 1;
                                    }else{
                                        var center = 0;
                                    }
                                    createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);
                                }else{
                                    var _ok =  false;
                                    if(is_home && glocations_2.length < 21 ) {
                                        _ok = true;
                                    }else if(glocations_2.length < 20) {
                                        _ok = true;
                                    }
                                    if(_ok) {
                                        var lat = e.latLng.lat().toFixed(7);
                                        var lgn = e.latLng.lng().toFixed(7);
                                        var latlng = new google.maps.LatLng(lat,lgn);
                                        var center = 0;
                                        createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);

                                        var length_2 = glocations_2.length -1;
                                        var endCoords = glocations_2[length].split(",");
                                        var lat_end = endCoords[0];
                                        var lgn_end = endCoords[1];

                                        drawLine(lat, lgn, lat_end, lgn_end)
                                    }

                                }
                                glocations_2.push(lat+','+lgn);
                            }
                        }.bind(this));
                    });
                }else{
                    flyAreaWrapper.setMap(map);
                }
            }else if(_this.hasClass('active')) {
                _this.addClass('unactive').removeClass('active');
                _this.find('img').attr('src','/assets/img/geofiliter_blue.png');
                drawingManager.setMap(null);
            }
        });
        geoFilterZones.forEach(function(object, key){
            google.maps.event.addListener(object, 'click', function (e) {
                var max = 20;
                if(!is_home || init) {
                    max = 19;
                }
                if (is_subuser) {
                  if(inc_2 - 1 == max) return;
                } else {
                  if(inc_1 - 1 == max) return;
                }
                if(!is_subuser && inc_1 <= max && $('.jsCreatNode').hasClass('active')) {
                    if(glocations_1.length == 0) {
                        var lat = e.latLng.lat().toFixed(7);
                        var lgn = e.latLng.lng().toFixed(7);
                        var latlng = new google.maps.LatLng(lat,lgn);
                        if(is_home)  {
                            var center = 1;
                        }else{
                            var center = 0;
                        }
                        createNode(flight, lat, lgn, latlng, center, inc_1, is_subuser, user_id);
                    }else{
                        var _ok =  false;
                        if(is_home && glocations_1.length < 11 ) {
                            _ok = true;
                        }else if(glocations_1.length < 10) {
                            _ok = true;
                        }
                        if(_ok) {
                            var lat = e.latLng.lat().toFixed(7);
                            var lgn = e.latLng.lng().toFixed(7);
                            var latlng = new google.maps.LatLng(lat,lgn);
                            var center = 0;
                            createNode(flight, lat, lgn, latlng, center, inc_1, is_subuser, user_id);

                            var length_1 = glocations_1.length -1;
                            var endCoords = glocations_1[length].split(",");
                            var lat_end = endCoords[0];
                            var lgn_end = endCoords[1];

                            drawLine(lat, lgn, lat_end, lgn_end)
                        }
                    }
                    glocations_1.push(lat+','+lgn);
                } else if(is_subuser && inc_2 <= max && $('.jsCreatNode').hasClass('active')) {
                  user_id = 2;
                  if(glocations_2.length == 0) {
                      var lat = e.latLng.lat().toFixed(7);
                      var lgn = e.latLng.lng().toFixed(7);
                      var latlng = new google.maps.LatLng(lat,lgn);
                      if(is_home)  {
                          var center = 1;
                      }else{
                          var center = 0;
                      }
                      createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);
                  }else{
                      var _ok =  false;
                      if(is_home && glocations_2.length < 11 ) {
                          _ok = true;
                      }else if(glocations_2.length < 10) {
                          _ok = true;
                      }
                      if(_ok) {
                          var lat = e.latLng.lat().toFixed(7);
                          var lgn = e.latLng.lng().toFixed(7);
                          var latlng = new google.maps.LatLng(lat,lgn);
                          var center = 0;
                          createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);

                          var length_2 = glocations_2.length -1;
                          var endCoords = glocations_2[length].split(",");
                          var lat_end = endCoords[0];
                          var lgn_end = endCoords[1];

                          drawLine(lat, lgn, lat_end, lgn_end)
                      }
                  }
                  glocations_2.push(lat+','+lgn);
                }
            });
        });
        setMapClearGeoFilterZone(map);
    }
}

function drawNoFlyZone(){
    $.each(mc_category_a, function(index, item) {
        no_fly_zone(map, item, 'A')
    });

    $.each(mc_category_b, function(index, item) {
        no_fly_zone(map, item, 'B')
    });

    $.each(mc_category_city_limit, function(index, item) {
        no_fly_zone(map, item, 'city_limit');
    });
}

function drawGeoFilterZone(event){

    // if(flyAreaWrapper==null){
    //     flyAreaWrapper = new google.maps.Polygon({
    //         strokeColor: '#052eff',
    //         strokeOpacity: 0.8,
    //         strokeWeight: 0,
    //         fillColor: '#052eff',
    //         fillOpacity: 0.2
    //     });
    //     flyAreaWrapper.setMap(map);
    // }
    // var flyAreaWrapperPath = lineFlyAreaWrapper.getPath();
    // flyAreaWrapper.setPath(flyAreaWrapperPath);
    // flyAreaWrapperPath.push(event.latLng);
}
function setMapClearNoFlyZone(map) {
    for (var i = 0; i < noFlyZones.length; i++) {
        noFlyZones[i].setMap(map);
    }
}
function setMapClearGeoFilterZone(map) {
    for (var i = 0; i < geoFilterZones.length; i++) {
        geoFilterZones[i].setMap(map);
    }
}
function _respondToMapChange(map){
    // weatherLoaded = false;
    var newInitialCenter = map.getCenter();

    if(initialCenter == null || initialCenter !== newInitialCenter){
        initialCenter = newInitialCenter;
        if(!weatherIconClicked){
            $('#weatherIcon_1').click(function(){
                loadWeather(initialCenter.lat(),initialCenter.lng());
                weatherLoaded = true;
                weatherIconClicked = true;
            });
        }else{
            if(weatherLoaded){
                if ( $("#reload_btn").hasClass("reloadHidden") ) {
                    $("#reload_btn").removeClass("reloadHidden").addClass("reloadShow");
                    weatherLoaded = false;
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }else{
                if( $("#reload_btn").hasClass("reloadShow") ) {
                    document.getElementById("reload_btn").innerHTML = '<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');"/>';
                    $("#weather-content #reload_btn").html('<img src="/assets/img/btnReload.png" onclick="loadWeather('+initialCenter.lat()+','+initialCenter.lng()+');/>');
                }
            }
        }
    }
}
function loadWeather(c_lat,c_long) {

    var _url = 'http://52.43.150.151/assets/getWeather.php?lat='+c_lat+'&lng='+c_long;
        if(!weatherLoaded){
            $.ajax({
                url: _url,
                type: "GET",
                dataType: "JSON",
                success: function(res, textStatus, xhr){
                    if(xhr.status == 200) {
                        $('.popover.fade.bottom.in').css("backgroundImage","none");
                        $('.popover.fade.bottom.in').css("backgroundColor","#fff");
                        var weatherDate = res.precipitation.time.split(' ');
                        var weatherDateConvert = weatherDate[1].split(':');
                        var endPrecipitationTime = parseInt(weatherDateConvert[0])+6;
                        var wind_direction_string_jp;

                        if(endPrecipitationTime >= 24) endPrecipitationTime = '0'+parseInt(endPrecipitationTime-24);

                        switch (res.forecasts.wind_direction_string) {
                            case "east":
                                wind_direction_string_jp = "東";
                                break;
                            case "east-northeast":
                                wind_direction_string_jp = "東北東";
                                break;
                            case "northeast":
                                wind_direction_string_jp = "北東";
                                break;
                            case "north-northeast":
                                wind_direction_string_jp = "北北東";
                                break;
                            case "north":
                                wind_direction_string_jp = "北";
                                break;
                            case "north-northwest":
                                wind_direction_string_jp = "北北西";
                                break;
                            case "northwest":
                                wind_direction_string_jp = "北西";
                                break;
                            case "west-northwest":
                                wind_direction_string_jp = "西北西";
                                break;
                            case "west":
                                wind_direction_string_jp = "西";
                                break;
                            case "west-southwest":
                                wind_direction_string_jp = "西南西";
                                break;
                            case "souththwest":
                                wind_direction_string_jp = "南西";
                                break;
                            case "south-southwest":
                                wind_direction_string_jp = "南南西";
                                break;
                            case "south":
                                wind_direction_string_jp = "南";
                                break;
                            case "south-southeast":
                                wind_direction_string_jp = "南南東";
                                break;
                            case "southeast":
                                wind_direction_string_jp = "南東";
                                break;
                            case "east-southeast":
                                wind_direction_string_jp = "東南東";
                                break;
                            case "east":
                                wind_direction_string_jp = "東";
                        }

                        $('#addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                        $('#dateForecast').html(res.forecasts.time);
                        $('#nameWeather').html(res.weather.weather.name);
                        $('#humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                        $('#temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                        $('#precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                        $('#timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                        $('#windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                        $('#directStringForecast').html(wind_direction_string_jp);
                        $('#directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                        $('#weather-content #addressWeather').html(res.area.weather_info.prefecture+' '+res.area.weather_info.primary.name);
                        $('#weather-content #dateForecast').html(res.forecasts.time);
                        $('#weather-content #nameWeather').html(res.weather.weather.name);
                        $('#weather-content #humidityForecast').html(res.forecasts.rh_1d5maboveground.toFixed(1));
                        $('#weather-content #temperatureWeather').html(res.forecasts.tmp_1d5maboveground.toFixed(1));
                        $('#weather-content #precipitationWeather').html(res.precipitation.probability_of_precipitation.toFixed(1));
                        $('#weather-content #timeWeather').html(weatherDateConvert[0]+'-'+endPrecipitationTime);
                        $('#weather-content #windVelocityForecast').html(res.forecasts.wind_velocity.toFixed(1));
                        $('#weather-content #directStringForecast').html(wind_direction_string_jp);
                        $('#weather-content #directAngleForecast').html(res.forecasts.wind_direction_angle.toFixed(1));

                        document.getElementById('weatherIcon_2').style.backgroundImage = "url('/assets/img/"+res.weather.weather.code+".png')";
                        $('#weather-content #weatherIcon_2').css("backgroundImage","url('/assets/img/"+res.weather.weather.code+".png')");

                        var windDirection = document.getElementById('windDirection');
                        windDirection.style.webkitTransform = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                        windDirection.style.mozTransform    = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                        windDirection.style.msTransform     = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                        windDirection.style.oTransform      = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';
                        windDirection.style.transform       = 'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)';

                        $('#weather-content #windDirection').css("webkitTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                        $('#weather-content #windDirection').css("mozTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                        $('#weather-content #windDirection').css("msTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                        $('#weather-content #windDirection').css("oTransform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');
                        $('#weather-content #windDirection').css("transform",'rotate('+res.forecasts.wind_direction_angle.toFixed(1)+'deg)');

                        weatherLoaded = true;
                        $("#reload_btn").removeClass("reloadShow").addClass("reloadHidden");
                        $("#reload_btn").html('');
                        $("#weather-content #reload_btn").html('');
                    }else {
                    }
                }
            });
        }
}

function captureImageSwitcher(_this){
    var _this = $(_this);
    if(_this.hasClass('capture_on')) {
        _this.addClass('capture_off').removeClass('capture_on');
        _this.find('img').attr('src','/assets/img/off_switcher.png');
        _this.find('input').attr('value','0');
    }else if (_this.hasClass('capture_off')){
        _this.addClass('capture_on').removeClass('capture_off');
        _this.find('img').attr('src','/assets/img/on_switcher.png');
        _this.find('input').attr('value','1');
    }
}

function rightAngleSwitcher(_this){
    var _this = $(_this);
    if(_this.hasClass('capture_on')) {
        _this.addClass('capture_off').removeClass('capture_on');
        _this.find('img').attr('src','/assets/img/off_switcher.png');
        _this.find('input').attr('value','0');
    }else if (_this.hasClass('capture_off')){
        _this.addClass('capture_on').removeClass('capture_off');
        _this.find('img').attr('src','/assets/img/on_switcher.png');
        _this.find('input').attr('value','1');
    }
}

function checkform(){
    var lat = document.getElementById('lat').value;
    var lng = document.getElementById('lgn').value;
    var height = document.getElementById('height').value;
    var timeStay = document.getElementById('timeStay').value;
    if(lat > 90 || lat < -90){
        $('#alertLatModal').modal('show');
    }
    else if(lng > 180 || lng < -180){
        $('#alertLongModal').modal('show');
    }
    else if(height < 0 || height > 150){
        $('#alertHeightModal').modal('show');
    }
    else if(timeStay < 0 || timeStay > 60){
        $('#alertTimeStayModal').modal('show');
    }
    else{
        document.getElementById('frmEditNode').submit();
    }
}

$(document).ready(function(){
    var weatherPopover = $("#drone_weather_toolbar a").popover({
        html : true,
        content: function() {
            return $("#weather-content").html();
        },
    });
    weatherPopover.on("hidden.bs.popover", function(e) {
        weatherIconClicked = false;
    });
});
