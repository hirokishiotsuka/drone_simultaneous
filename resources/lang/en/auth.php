<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'メールアドレスまたはパスワードが間違っています。',
    'throttle' => '規定回数以上の試行が失敗しました。数秒後、もう一回御入力してください。',

];
