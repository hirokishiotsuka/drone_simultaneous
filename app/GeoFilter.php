<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeoFilter extends Model
{
    protected $table = 'geo_filters';

    protected $fillable = [
    	'id', 
    	'flight_id', 
    	'array_points'
    ];

    public function flight(){
    	return $this->belongsTo('App\Flight', 'flight_id');
    }
}
