<?php

namespace App\Http\Controllers;

use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\FlightMediaRepository;
use App\FlightMedia;
use App\Repositories\FlightRepository;
use App\Flight;
use App\FlightLog;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Repositories\FlightLogRepository;

class MediaController extends Controller
{
    public function __construct(FlightMediaRepository $flightmedias, FlightRepository $flights, FlightLogRepository $flightlogs)
    {
    	parent::__construct();
    	$this->flights = $flights;
    	$this->medias = $flightmedias;
    	$this->currentUser = auth()->user();
        $this->flightlogs = $flightlogs;
    }

    public function listFlight()
    {
    	$flights = $this->flights->findBy('user_id', $this->currentUser->id);
        if($flights !== null) $flights = $this->flights->findBy('user_id', $this->currentUser->id)->all()->sortByDesc('updated_at');
        else $flights = collect();
    	return view('medias.lists', ['flights' => $flights]);
    }

    /**
     * Get Media List Of One Flight Plan
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     * @param  [Int]            $id     ID Of Log
     * @return [Collection]     $media  Medias Of Log
     */
    public function listMedia($id)
    {
        $log = $this->flightlogs->find($id);
        if($log !== null) {
            $flight = $this->flights->find($log->flight_id);
            if ($flight !== null) {
                $firstLog = FlightLog::where('flight_id', $log->flight_id)->where('fstarted_at', $log->fstarted_at)->orderBy('created_at', 'asc')->first();
                $lastLog = FlightLog::where('flight_id', $log->flight_id)->where('fstarted_at', $log->fstarted_at)->orderBy('created_at', 'desc')->first();
                // dd($lastLog);
                $medias = FlightMedia::where('flight_id', $log->flight_id)->where('recorded_at','>=', $firstLog->created_at)->where('recorded_at','<=', $lastLog->created_at)->get();
                return view('medias.medias', [
                    'medias' => $medias,
                    'flight' => $flight,
                ]);
            }
             return view('404');
        }
        return view('404');
    }

    /**
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     * @param  [Flight ID] $id
     * @return [Datelist]
     */
    public function datelist($id)
    {
        $flight = $this->flights->find($id);
        if($this->currentUser->id == $flight->user_id && $flight !== null)
        {
            $logs = FlightLog::addSelect(\DB::raw('id, flight_id,fstarted_at, SUM(flight_time) AS total_flight_time, COUNT(id) AS total_sendlog'))->where('flight_id', $id)->groupBy('fstarted_at')->orderBy('fstarted_at', 'DESC')->get();
            $logs = $this->addStatus($logs);
            $logs->flightName = $flight->name;
            return view('medias.datelist', ['logs' => $logs]);
        }
        return view('404');
    }

    /**
     * Function helper detect status of Flight
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     * @param  [Collection of datelist]
     * @return [Status of flight plan]
     */
    public function addStatus($collection){
        return $collection->map(function($value) {
            $getLogs = FlightLog::addSelect('plan_status')->where('flight_id', $value->flight_id)->where('fstarted_at', $value->fstarted_at)->get();
            if(count($getLogs) == 0){
                $value->status = '-';
                return $value;
            } else {
                foreach ($getLogs as $keyLogs => $Log) {
                    if($Log->plan_status != 0){
                        $allLogIsZero = false;
                        break;
                    }
                    $allLogIsZero = true;
                }
                if($allLogIsZero){
                    $value->status = '異常'; //Fail
                    return $value;
                }else{
                    if($getLogs->last()->plan_status == 0){
                        $value->status = '正常'; //Success
                        return $value;
                    }else{
                        $value->status = '異常'; //Fail
                        return $value;
                    }
                }
            }
        });
    }
}
