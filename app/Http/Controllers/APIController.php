<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\FlightRepository;
use App\Flight;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Hash;

class APIController extends Controller
{
    public function login(Request $request)
    {
    	try {
	    	$data = $request->all();
	    	if (!$token = JWTAuth::attempt($data)) {
	    		return response()->error(['Wrong Email Or Password'], 300);
	    	}
	    	$user = JWTAuth::toUser($token);
	    	return response()->success(compact('user','token'));
    	} catch(\Exception $e) {
    		return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    	}
    }

    public function register(Request $request)
    {
    	try {
	    	$data = $request->all();
	    	$rules = [
	    		'name' => 'required|max:255',
	            'email' => 'required|email|max:255|unique:users',
	            'password' => 'required|min:8',
	            'telephone' => 'numeric',
	    	];
	    	$validator = Validator::make($data, $rules);
    		if ($validator->passes()) {
    			$data['password'] = Hash::make($data['password']);
				$user = User::create($data);
				return response()->success(compact('user'));
    		}
            return response()->error($validator->errors()->all(), 300);
    	} catch(\Exception $e) {
    		return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    	}
    }

    public function logout(Request $request){
        try {
            $data = $request->all();
            $rules = [
                'token' => 'required',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $user = JWTAuth::toUser($data['token']);
                if ($user != null) {
                    JWTAuth::invalidate();
                    return response()->success('Logged Out');
                }
                return response()->error(null, 404);
            }
        } catch (\Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->error(['Token is Invalid'], 300);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->error(['Token is Expired'], 300);
            }else{
                return response()->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }
}
