@extends('layouts.app')

@section('content')
<!-- start: Content -->
<style type="text/css">
    #DataTables_Table_0_wrapper .col-lg-6:nth-child(1){
        display: none;
        width: 0px;
    }
    #DataTables_Table_0_wrapper .col-lg-6:nth-child(2){
        width: 100%;
    }
    #add-delete-btn{
        float: right;
    }
    #add-delete-btn a{
        float: right;
        margin-left: 10px;
    }
</style>
<div class="col-md-10 col-sm-11 main">
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <!-- Modal Add New Flight-->
            <!-- <div class="modal fade" id="addNewFlight" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="{{ URL::to('/admin/flight/proCreate') }}" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Flight</h4>
                            </div>
                            <div class="modal-body">
                                @include('auth.partials.errors')
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Name of Flight">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-default btn-success"> Submit</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> -->
            <!-- End Modal -->
            <div class="panel-heading" data-original-title>
                <h2><span class="break"></span>メディア一覧</h2>
                <div class="panel-actions">

                </div>
                <div class="row">
                <div class="pull-right">
                    <!-- <a type="button" data-toggle="modal" data-target="#addNewFlight" class="btn btn-success">Add New Flight</a> -->
                </div>
                </div>
            </div>
            <div class="panel-body">
                @include('auth.partials.errors')
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <div class="col-lg-6" id="add-delete-btn">
                        <a class="jsDeleteAllComfirm" ><img src="{{ URL::to('assets/img/removeFlight.png') }}"></a>
                    </div>
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th>プラン名</th>
                        <th style="width: 200px; text-align: center;">更新日時</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($flights as $flight)
                    <tr>
                        <td class="a-center">
                            <div class="custom-checkbox">
                                <input name="id[]" value="{{ $flight->id }}" id="cb{{  $flight->id }}" type="checkbox" class="tableflat jsCheckbox">
                                <label for="cb{{ $flight->id }}">&nbsp;</label>
                          </div>
                        </td>
                        <td><a href="{{ URL::to('/admin/media/'.$flight->id) }}">
                            <div class="title-flight">
                                {{ $flight->name }}
                            </div>
                        </a></td>
                        <td>{{ $flight->updated_at }}</td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div><!--/col-->

</div><!--/row-->



</div>
<!-- end: Content -->
<!-- page scripts -->
<!-- start: JavaScript-->
<!--[if !IE]>-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="{{ URL::to('assets/js/jquery.confirm.js') }}"></script>

<!--<![endif]-->

<!--[if IE]>

    <script src="{{ URL::to('assets/js/jquery-1.11.0.min.js') }}"></script>

<![endif]-->

<!--[if !IE]>-->

    <script type="text/javascript">

    </script>

<!--<![endif]-->

<!--[if IE]>

    <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    </script>

<![endif]-->
<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" aria-hidden="true" data-dismiss="modal" href="#">×</a>
                <h3>Delete</h3>
            </div>
            <div class="modal-body">
                <p>You are about to delete.</p>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a id="btnYes" class="btn btn-sm btn-success">Yes</a>
                <a aria-hidden="true" data-dismiss="modal" href="#" class="btn btn-sm btn-danger">No</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    $('#deleteModal').on('show', function() {
        var id = $(this).data('id'),
            removeBtn = $(this).find('.danger');
    });

    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#deleteModal').data('id', id).modal('show');
    });

    $('#btnYes').click(function() {
        // Handle deletion
        var id = $('#deleteModal').data('id');
        var callDeleteAction = $.get( "{{ URL::to('/admin/flight/delete') }}/"+id, function() {
            // alert( "success" );
        })
        .done(function() {
            alert( "Flight has been deleted #"+id+"!" );
            location.reload();
            // BootstrapDialog.alert('I want banana!');
        })
        .fail(function() {
            alert( "error" );
        })

        $('[data-id='+id+']').remove();
        $('#deleteModal').modal('hide');
    });
    $(".jsCheckboxAll").on('click',function(){
        if($(this).is(':checked')) {
            $('.jsCheckbox').each(function(){
                $(this).attr('checked','checked');
                $(this).prop('checked', true);
            });
        } else {
            $('.jsCheckbox').each(function(){
                $(this).removeAttr('checked');
                $(this).prop('checked', false);
            });
        }
    });

    $(".jsCheckbox").on('click',function(){
        if($(".jsCheckbox").length == $(".jsCheckbox:checked").length){
            $(".jsCheckboxAll").attr("checked","checked");
            $('.jsCheckboxAll').prop('checked', true);
        }else{
            $(".jsCheckboxAll").removeAttr("checked");
            $('.jsCheckboxAll').prop('checked', false);
        }
    });
    /*---Delete all item---*/
    $(".jsDeleteAllComfirm").confirm({
        title:"消します",
        text: "このフライトのメディアを一括削除します。よろしいですか？",
        confirm: function(button) {
            var checboxes = new Array();
            $(".jsCheckbox:checked").each(function() {
               checboxes.push($(this).val());
            });
            var data = {
                id: checboxes
            };
            $.ajax({
                url: '{{ URL::to('admin/flight/deteles') }}',
                type: "POST",
                dataType: "JSON",
                data: id = data,
                success: function(res){
                    if(res.status) {
                        checboxes.forEach(function(val) {
                            $('.jsCheckbox[value="'+val+'"]').closest('tr').remove();
                        });
                        $('.jsMessage .alert').addClass('alert-success').html(res.message).show();
                    }else {
                        $('.jsMessage .alert').addClass('alert-danger').html(res.message).show();
                    }
                }
            });
            button.fadeOut(2000).fadeIn(2000);
        },
        cancel: function(button) {
            button.fadeOut(2000).fadeIn(2000);
        },
        confirmButton: "はい",
        cancelButton: "いいえ"
    });
</script>
<script src="//datatables.net/download/build/nightly/jquery.dataTables.js"></script>
<!-- inline scripts related to this page -->
<script src="{{ URL::to('assets/js/pages/table.js') }}"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        // $('#DataTables_Table_0_filter label').html('<input type="text" aria-controls="DataTables_Table_0" placeholder="キーワード"><img src="{{ URL::to('assets/img/btSearch.png') }}">');
        $("#DataTables_Table_0_filter").html('<label><input id="DataTables_Table_0_filter_input" type="text" aria-controls="DataTables_Table_0" placeholder="キーワード"></label><img src="{{ URL::to('assets/img/btSearch.png') }}">');
        var table = $('.datatable').DataTable();
        $('#DataTables_Table_0_filter_input').keyup( function() {
            $('.datatable').dataTable().fnFilter(this.value);
        } );
        $('.prev a').text('PREV');
        $('.next a').text('NEXT');
    });
</script>
@endsection
