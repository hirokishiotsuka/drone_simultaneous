@extends('layouts.node')
@section('content')
@if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari'))
  <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/alt/video-js-cdn.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/video.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/3.0.2/videojs-contrib-hls.js"></script>
@else
  <link href="http://vjs.zencdn.net/4.2.0/video-js.css" rel="stylesheet">
  <script src="http://vjs.zencdn.net/4.2.0/video.js"></script>
@endif
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">

    // Distance between 2 points
    var rad = function(x) {
        return x * Math.PI / 180;
    };
    var getDistance = function(lat1, lng1, lat2, lng2) {
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = rad(lat2 - lat1);
        var dLong = rad(lng2 - lng1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d/100; // returns the distance in meter
    };
    // Build data for chart
    var dataChart = [];

            @if(!empty($nodes) && count($nodes) > 0)

    var tmp = 0;

    var latStart = {{ $nodes[0]->lat }};
    var lngStart = {{ $nodes[0]->lng }};
    @if (!empty($homeposition))
            latStart = {{ $homeposition->lat }}
            lngStart = {{ $homeposition->lng }}
        @endif

        @foreach($nodes as $key => $node)
    {
        @if (isset($nodes[$key-1]))
        tmp +=getDistance({{ $nodes[$key-1]->lat }}, {{ $nodes[$key-1]->lng }}, {{ $node->lat }}, {{ $node->lng }});
    @else
            tmp = getDistance(latStart, lngStart, {{ $node->lat }}, {{ $node->lng }});
    @endif
            dataChart[{{ $key }}] = tmp;
    }
    @endforeach
@endif

$(function () {
        $('#chartContainer').highcharts({
            chart: {
                backgroundColor:'rgba(44, 44, 44, 0.6)',
                marginRight: 40,
                marginTop: 40,
                width: null
            },
            title: {
                text: ''
            },
            xAxis: {
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    style: {
                        color: '#fff'
                    },
                    text: 'Height'
                },
                labels: {
                    style: {
                        color: '#fff'
                    },
                    formatter: function () {
                        return this.value + 'm';
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [
                {
                    type: 'line',
                    name: 'Drone',
                    pointWidth: 1,
                    data: [
                            @if(!empty($homeposition))
                        {
                            y: {{$homeposition->height}}
                        },
                            @endif
                            @if(!empty($nodes) && count($nodes) > 0)
                            @foreach($nodes as $key => $node)
                        {
                            x: dataChart[{{ $key }}],
                            y: {{$node->height}}
                        },
                        @endforeach
                        @endif
                    ]
                },
                {
                    type: 'scatter',
                    name: 'Drone',
                    pointWidth: 1,
                    data: [
                            @if(!empty($homeposition))
                        {
                            y: {{$homeposition->height}},
                            marker: {symbol: 'url(/assets/img/icon00.png)'}
                        },
                            @endif
                            @if(!empty($nodes) && count($nodes) > 0)
                            @foreach($nodes as $key => $node)
                        {
                            x: dataChart[{{ $key }}],
                            y: {{$node->height}},
                            marker: {symbol: 'url(/assets/img/icon{{$key+1}}.png)'}
                        },
                        @endforeach
                        @endif
                    ]
                }
            ]
        });
    });

</script>
<style type="text/css">
    #iw-container  .iw-title {
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 22px;
        font-weight: 400;
        padding: 10px;
        background-color: #48b5e9;
        color: white;
        margin: 1px;
        border-radius: 2px 2px 0 0; /* In accordance with the rounding of the default infowindow corners. */
    }
    .gm-style-iw {
        min-width: 250px !important;
        top: 20px !important;
        left: 0 !important;
        background-color: #fff;
        background-image: url("{{ URL::to('assets/img/popDrone.png') }}");
        background-repeat: no-repeat;
        background-position: 130px -50px;
        background-size: 80% auto;
        box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
        border: 1px solid #bbb;
        border-radius: 5px !important;
        text-align: center;
        min-height: 60px;
    }
    .gm-style-iw #frmEditNode{
        width: 250px !important;
    }
    #frmCurrentNode {
        min-width: 260px !important;
    }
    #frmEditNode label.col-md-3{
        text-align: left;
        padding-left: 5px;
        font-weight: 300;
        padding-right: 10px;
    }
    #frmCurrentNode h4, #frmEditNode h4{
        margin-bottom: 5px;
        margin-top: 15px;
    }
    #frmCurrentNode hr, #frmEditNode hr{
        margin-top: 0px;
        border-top: 1px solid #25a6ef;
    }
    #frmCurrentNode h4.w-90p, #frmEditNode h4.w-90p{
        text-align: left;
    }
    #frmCurrentNode label.col-md-4,#frmEditNode label.col-md-4{
        text-align: left;
        padding-left: 0px;
    }
    #frmCurrentNode .form-group, #frmEditNode .form-group{
        text-align: left;
    }
    .labelForInput {
        top: 6px;
    }
    .input-group #lat, .input-group #lgn, #height{
        border-top-left-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
    }
    #frmEditNode .input-group-addon{
        border-top-right-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
    }
    #height{
        border-top-left-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
    }
    #frmCurrentNode .input-group-addon, #frmEditNode .input-group-addon{
        border-top-right-radius: 5px !important;
        border-bottom-right-radius: 5px !important;
    }
    #frmCurrentNode .form-group.center, #frmEditNode .form-group.center{
        text-align: center;
    }
    .height-form-control {
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        color: #555;
        display: block;
        font-size: 14px;
        height: 34px;
        line-height: 1.42857;
        padding: 6px 12px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 100%;
    }
    .labels {
      color: white;
      font-family: "Lucida Grande", "Arial", sans-serif;
      font-size: 14px;
      text-align: left;
      font-weight: bold;
    }
    .right-area {
      position: absolute;
      right: 25px;
      top: 75px;
    }
    #player {
      position: relative;
    }
    #live-stream-player{
      display: none;
    }
    .button-wrapper {
      position: absolute;
      right: 2px;
    }
    #startFlight {
      background: transparent none repeat scroll 0 0;
      font-size: 12px;
      z-index: 0;
      vertical-align: middle;
      display: table-cell;
    }
    #startFlight .inner {
      position: absolute;
      font-size: 1rem;
      color: #f00;
      font-weight: bold;
      margin: .8rem .3rem;
      background: #fff;
      padding: .4rem .7rem;
      text-align: center;
      border: 1px solid;
    }
    #startFlight .inner::before {
      content: '';
      position: absolute;
      top: -12px;
      left: 0;
      right: 0;
      width: 5px;
      border: 5px solid transparent;
      border-bottom: 7px solid #f00;
      margin: auto;
    }
    #startFlight .jsClose {
      display: block;
      position: relative;
    }
    #startFlight .jsClose.disabled {
      pointer-events: none;
      opacity: .6;
      user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
    }
    #goHome {
      background: transparent none repeat scroll 0 0;
      font-size: 12px;
      z-index: 0;
      vertical-align: middle;
      display: table-cell;
      padding-right: 15px;
      padding-bottom: 4px;
    }
    #goHome .inner {
      position: absolute;
      font-size: 1rem;
      color: #f00;
      font-weight: bold;
      margin: .8rem .3rem;
      background: #fff;
      padding: .4rem .7rem;
      text-align: center;
      border: 1px solid;
    }
    #goHome .inner::before {
      content: '';
      position: absolute;
      top: -12px;
      left: 0;
      right: 0;
      width: 5px;
      border: 5px solid transparent;
      border-bottom: 7px solid #f00;
      margin: auto;
    }
    #goHome .jsClose {
      display: block;
      position: relative;
    }
    #goHome .jsClose.disabled {
      pointer-events: none;
      opacity: .6;
      user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
    }
    #map #drone_weather_toolbar{
        display: block;
    }
    .navbar.navbar-inverse {
        position: absolute;
        width: 100%;
        z-index: 1000;
    }
    #chartContainer{
        display: none;
        background: transparent none repeat scroll 0 0;
        font-size: 12px;
        position: absolute;
        left: 15px;
        bottom: 15px;
        z-index: 0;
    }
    #drone_weather_toolbar {
        background: transparent none repeat scroll 0 0;
        font-size: 12px;
        vertical-align: middle;
        display: table-cell;
        z-index: 0;
    }
    #drone_weather_toolbar .arrow{
        left: 88% !important;
    }
    .smooth-tab.attach-right {
        border-radius: 1.4em 0 0 1.4em / 1em 0 0 1em;
    }
    .smooth-tab {
        background-color: transparent;
        border: 0 solid;
        font-size: 1.1em;
        min-height: 1.6em;
        min-width: 1.6em;
    }
    #drone_weather_toolbar h4{
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 5px;
    }
    #drone_weather_toolbar hr{
        border-top: 1px solid #25a6ef;
        margin: 0px;
    }
    #drone_weather_toolbar > ul {
        margin-bottom: 3px;
    }
    #drone_weather_toolbar .form-group{
        margin-bottom: 0;
    }
    #drone_weatherIcon{
        clear: left;
        float: left;
        height: 4em;
        margin-bottom: 2px;
    }
    #drone_weather_toolbar ul li > a.dropdown-toggle {
        background: rgba(0, 0, 0, 0.45) none repeat scroll 0 0;
        display: inline-block;
        line-height: 0;
        padding: 0;
    }
    #drone_weather_toolbar a {
      outline: 0;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;

    }
    #drone_weather_toolbar a #weatherIcon_wrapper{
        width: auto;
        height: 45px;
    }
    #drone_weather_toolbar #weatherIcon_1{
        cursor: pointer;
    }
    #drone_weather_toolbar #weatherIcon_2{
        height: 70px;
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar .popover.fade.bottom.in{
        left: -223px !important;
        width: 300px !important;
        border-radius: 5px !important;
        background-repeat: no-repeat;
        background-position: center;
        background-image: url(/assets/img/weatherLoading.gif);
        background-size: 25px 25px;
        background-color: #eee;
    }
    #drone_weather_toolbar fieldset .col-sm-12, #drone_weather_toolbar fieldset .col-sm-5, #drone_weather_toolbar fieldset .col-sm-4, #drone_weather_toolbar fieldset .col-sm-7{
        padding-left: 0px;
    }
    #drone_weather_toolbar .boxWeather .col-md-8{
        padding-left: 5px;
    }
    #drone_weather_toolbar fieldset .col-sm-12, #drone_weather_toolbar fieldset .col-sm-5, #drone_weather_toolbar fieldset .col-sm-7{
        padding-right: 0px;
    }
    #drone_weather_toolbar .boxWeather.form-group {
        margin-left: 0;
    }
    #drone_weather_toolbar .backgroundDirection{
        width: 86px;
        height: 86px;
        background-image: url(/assets/img/weatherDirectionBG.png);
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar #windDirection{
        width: 86px;
        height: 86px;
        background-image: url(/assets/img/windDirection.png);
        background-repeat: no-repeat;
        background-position: center;
    }
    #drone_weather_toolbar label{
        font-weight: 300;
        padding-left: 0px;
        padding-right: 0px;
    }
    #drone_weather_toolbar .form-group.weatherTitle{
        margin-bottom: 0px;
    }
    #drone_weather_toolbar .dateWeather{
        font-size: 11px;
    }
    #drone_weather_toolbar .nameWeather{
        font-weight: 600;
    }
    #drone_weather_toolbar .temperatureInfo{
        font-size: 20px;
    }
    #drone_weather_toolbar .temperatureInfo #temperatureWeather{
        font-size: 38px;
    }
    #drone_weather_toolbar .kccs_api{
        height: 75px;
    }
    #drone_weather_toolbar .kccs_api #reload_btn{
        margin-top: 5px;
        text-align: center;
    }
    #drone_weather_toolbar .kccs_api #reload_btn img{
        cursor: pointer;
    }
    #drone_weather_toolbar .kccs_api.form-group a img {
        margin-top: 10px;
        margin-bottom: 5px;
    }
    #app-layout .popover{
        width: 300px;
        transform : translateY(0px) !important;
        transition-duration: 0ms;
        transition-property: transform !important;
    }
    #app-layout .popover-tip{
        transition: -webkit-transform 0ms ease-in !important;
    }
    #drone_weather_toolbar .colon{
        float: right;
    }
    #jsNodes .bootstrap-switch{
        border-radius: 20px !important;
        border: 2px solid #fff !important;
        margin: 0 5px !important;
    }
    #jsNodes .bootstrap-switch.bootstrap-switch-inverse.bootstrap-switch-on .bootstrap-switch-label,#jsNodes .bootstrap-switch.bootstrap-switch-off .bootstrap-switch-label, #jsNodes .bootstrap-switch.bootstrap-switch-inverse.bootstrap-switch-off .bootstrap-switch-label, #jsNodes .bootstrap-switch.bootstrap-switch-on .bootstrap-switch-label {
        border-radius: 40% !important;
    }
    #jsNodes .bootstrap-switch .bootstrap-switch-handle-off, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on, #jsNodes .bootstrap-switch .bootstrap-switch-label{
        padding: 1px 12px !important;
    }
    #jsNodes .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default, #jsNodes .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-primary, #jsNodes .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary{
        background-color: rgba(0, 0, 0, 0) !important;
    }
    #jsNodes ./*noFlyZone-switcher{
        line-height: 1;
        margin-top: 15px;
        margin-right: 20px;
        color: #fff;
        font-weight: 600;
    }*/
    #jsNodes .noFlyZone-switcher label.title{
        font-weight: 300;
        margin-right: 5px;
    }
    #jsNodes .geoFilter-switcher label.title{
        margin-right: 5px;
        font-size: 15px;
    }
    /*#jsNodes .geoFilter-switcher {
        line-height: 1;
        color: #fff;
        font-weight: 600;
        margin-right: 20px;
        margin-top: 15px;
    }*/
    .vjs-default-skin .vjs-big-play-button {
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
    }
    strong.onOffSwitcher{
        /*color: #2e9ce5;*/
    }
    strong.onOffSwitcher span:hover{
        cursor: pointer;
    }
    .icon_right_angle{
        width: 13px;
        height: auto;
    }
    .highcharts-legend{display: none;}
    .highcharts-contextbutton {display: none}
    .highcharts-credits{display: none}
    .highcharts-series.highcharts-series-1.highcharts-line-series.highcharts-color-1{display: none}
    .form-group.geoFilter-switcher, .form-group.noFlyZone-switcher {
        font-size: 13px;
    }
    .form-group.noFlyZone-switcher {
        margin-top: 10px;
    }
    .geoFilter-switcher .onOffSwitcher, .noFlyZone-switcher .onOffSwitcher{
        float: right;
    }
    .fieldset-switcher{
        margin-bottom: 20px;
    }
</style>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/admin/realtime') }}"><img src="/assets/img/back.png" alt="back" /> BACK</a>
        </div>
        <input type="hidden" id="flight" value="{!! $flight->id !!}" />
        <input type="hidden" id="user_id" value="{!! $user_id !!}" />
        {!! Form::open(array('route' => array('flight.postDetail', $flight->id), 'id'=>'jsNodes', 'class'=>'form-horizontal', 'files'=>true)) !!}
        <ul class="nav navbar-nav navbar-right">
            <li><a href="javascript:;" class="p-10-3 jsLive active"><img src="/assets/img/live_off.png" alt="保存" /></a></li>
            <li id="startFlight"><a href="#" class="p-10-3" onclick="updatePlanStatus();"><img src="/assets/img/btnStart.png" alt="Start" /></a></li>
            <li id="goHome"><a href="#" class="p-10-3" onclick="updateGoHome();"><img src="/assets/img/btnGoHome.png" alt="強制帰還" /></a></li>
            <li id="drone_weather_toolbar"><a href="javascript:;" class="p-10-3" id="drone_weather_icon" tabindex="0" role="button" data-placement="bottom"><img src="/assets/img/informationicon.png" id="weatherIcon_1"/></a></li>
        </ul>
        @if(!empty($homeposition) && $user_id == "1")
            <input type="hidden" id="is_home" name="is_home" value ="1" />
            <input type="hidden" attr-home="{!! $homeposition->is_home ? '1' : '0' !!}" attr-point-id="{!! $homeposition->id !!}" attr-user-id="{!! $user_id !!}" id="old_node" name="node[]" value ="{!! $homeposition->id.','. $homeposition->lat.','.$homeposition->lng.','.$homeposition->height.','.$homeposition->capture.','.$homeposition->right_angle.','.$homeposition->time_stay.','.$user_id !!}" />
        @elseif(!empty($homeposition))
            <input type="hidden" id="is_home_sub" name="is_home_sub" value ="1" />
            <input type="hidden" attr-home="{!! $homeposition->is_home ? '1' : '0' !!}" attr-point-id="{!! $homeposition->id !!}" attr-user-id="{!! $user_id !!}" id="old_node_sub" name="node_sub[]" value ="{!! $homeposition->id.','. $homeposition->lat.','.$homeposition->lng.','.$homeposition->height.','.$homeposition->capture.','.$homeposition->right_angle.','.$homeposition->time_stay.','.$user_id !!}" />
        @endif

        @if(!empty($nodes) && count($nodes) > 0)
          @foreach($nodes as $node)
            @if($node->user_id == 1)
              <input type="hidden" attr-home="{!! $node->is_home ? '1' : '0' !!}" attr-point-id="{!! $node->id !!}" attr-user-id="{!! $user_id !!}" id="old_node" name="node[]" value ="{!! $node->id.','. $node->lat.','.$node->lng.','.$node->height.','.$node->capture.','.$node->right_angle.','.$node->time_stay.','.$node->user_id !!}" />
            @else
              <input type="hidden" attr-home="{!! $node->is_home ? '1' : '0' !!}" attr-point-id="{!! $node->id !!}" attr-user-id="{!! $user_id !!}" id="old_node_sub" name="node_sub[]" value ="{!! $node->id.','. $node->lat.','.$node->lng.','.$node->height.','.$node->capture.','.$node->right_angle.','.$node->time_stay.','.$node->user_id !!}" />
            @endif
          @endforeach
        @endif

        @if(!empty($geofilters) && count($geofilters) > 0)
            @foreach($geofilters as $geofilter)
                <input type="hidden" attr-geofilter-id="{!! $geofilter->id !!}" id="geo_filters" name="geo_filters[]" value ="{!! $geofilter->id.'-'. $geofilter->array_points_convert !!}" />
            @endforeach
        @endif
        {!! Form::close() !!}
    </div>
</nav>
<!--Map -->

<div id="map"></div>
  <div class="right-area">

    <!-- <video id="live-stream-player" class="video-js vjs-default-skin" controls autoplay
 preload="auto" width="450" height="250" data-setup='{}'>
        @if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari'))
            <source src="http://52.35.45.9/hls/0000.m3u8" type="application/x-mpegURL">
        @else
            <source src="hls/0000.m3u8" type='application/x-mpegURL'>
            <source src="rtmp://52.35.45.9/src/0000" type='rtmp/mp4'>
        @endif
      <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
      </p>
    </video> -->

  <div class="button-wrapper bootstrap">
      <!-- <input type="text" style="width:200px;display:none;" value="XdlmoLAbbiQ" id="live_view_key">
      <input type="button" value="URL 更新" id="change_live_view_key" style="display:none;"> -->
      <div id="drone_weather_toolbar" class="smooth-tab attach-right dark">
        <a id="drone_weather_icon"
           tabindex="0"
           role="button"
           data-placement="bottom">
           <div id="weatherIcon_wrapper"><div id="weatherIcon_1">&nbsp;</div></div>
        </a>
      </div>
  </div>
  <div id="weather-content" class="hidden">
      <fieldset class="col-sm-12 form-horizontal fieldset-switcher">
          <div class="form-group geoFilter-switcher">
              <label class="col-md-5">
              ジオフィルター
              </label>
              <strong class="onOffSwitcher">ON <span id="geoFilterSwicher" class="wrapSwitcher geoFilter_off" onclick="geoFilterSwicher(this)"><img src="/assets/img/off_switcher2.png" class="geoFilterSwicher"/></span> OFF</strong>
          </div>
          <div class="form-group noFlyZone-switcher">
              <label class="col-md-5">
              飛行禁止区域
              </label>
              <strong class="onOffSwitcher">ON <span id="noFlyZoneSwicher" class="wrapSwitcher noFlyZone_off" onclick="noFlyZoneSwicher(this)"><img src="/assets/img/off_switcher2.png" class="noFlyZoneSwicher"/></span> OFF</strong>
          </div>
      </fieldset>
      <fieldset class="col-sm-12 form-horizontal">
          <div class="form-group weatherTitle">
              <h4 class="w-90p"><span id="addressWeather"></span>気象情報 </h4>
              <hr>
          </div>
          <div class="dateWeather form-group">
              <span id="dateForecast"></span> 時点の気象情報
          </div>
          <div class="iconWeather form-group">
              <div class="col-sm-5" id="weatherIcon_2">

              </div>
              <div class="col-sm-7 temperatureInfo">
                  <span id="temperatureWeather"></span>°C
              </div>
          </div>
          <div class="nameWeather form-group">
              <span id="nameWeather"></span>
          </div>
          <div class="boxWeather form-group">
              <div class="col-sm-7">
                  <div class="form-group">
                      <label class="col-md-4">湿度 <span class="colon">:</span></label>
                      <div class="col-md-8"><span id="humidityForecast"></span>%</div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">降水確率 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="precipitationWeather"></span>%
                          (<span id="timeWeather"></span>)
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">風速 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="windVelocityForecast"></span>m/s
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4">風向 <span class="colon">:</span></label>
                      <div class="col-md-8">
                          <span id="directStringForecast"></span> (<span id="directAngleForecast"></span>°)
                      </div>
                  </div>
              </div>
              <div class="col-sm-5 backgroundDirection">
                  <div id="windDirection">

                  </div>
              </div>
          </div>
          <div class="kccs_api form-group">
              <a href="http://www.energy-cloud.jp"><img src="/assets/img/logo_kccs.png" title="KCCS API サービス"></a>
              <hr>
              <div id="reload_btn" class="reloadHidden"></div>
          </div>
      </fieldset>
  </div>
</div>
<div id="chartContainer" style="min-width: 500px; height: 250px; margin: 0 auto"></div>
<div class="modal fade" id="alertModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                ドローンからの応答が途切れました。
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alertLatModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                -90から90までの値を指定してください
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alertLongModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                -180から180までの値を指定してください
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alertHeightModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                0から150までの値を指定してください
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alertTimeStayModal" role="dialog" data-backdrop="static"
   data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3></h3>
            </div>
            <div class="modal-body">
                0から60までの値を指定してください
            </div>
            <div class="modal-footer">
                <a id="btnYes" data-dismiss="modal"><img src="{{ URL::to('assets/img/close.png') }}"></a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

<script src="{{ URL::to('assets/js/mc_category_a.js') }}"></script>
<script src="{{ URL::to('assets/js/mc_category_b.js') }}"></script>
<script src="{{ URL::to('assets/js/mc_category_city_limit.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $player = document.getElementById("live-stream-player");
        $('.jsLive').on('click', function(){
            var _this = $(this);
            if(_this.hasClass('active')) {
                _this.addClass('unactive').removeClass('active');
                _this.find('img').attr('src','/assets/img/live.png');
                $player.style.display = "block";
            }else{
                _this.addClass('active').removeClass('unactive');
                _this.find('img').attr('src','/assets/img/live_off.png');
                $player.style.display = "none";
            }
        });
    });
    var map;
    var latLngBounds;
    var markers_1 = [];
    var markers_2 = [];
    var gmarkers_1 = [];
    var gmarkers_2 = [];
    var glocations_1 = [];
    var glocations_2 = [];
    var marker = [];
    var marker_1 = [];
    var marker_2 = [];
    var id = [];
    var hight = [];
    var flight;
    var object_1 =[];
    var object_2 =[];
    var length_1 =[];
    var length_2 =[];
    var user_id = 1;
    var infoWindows = [];
    var center;
    var c_lat = 35.65325;
    var c_long = 139.772049;
    var point;
    var type = '';
    var polylines = [];
    var init = false;
    var is_home = false;
    var logNodes_1 = [];
    var logNodes_2 = [];
    var currentNode_1;
    var currentNode_2;
    var currentNodeMarker_1;
    var currentNodeMarker_2;
    var flightInfo;

    var red_image = '/assets/img/pin-red.png';
    var blue_image = '/assets/img/pin-blue.png';
    var category_a_name = 'Category A';
    var category_b_name = 'Category B';
    var category_city_limit_name = 'No Fly';
    var opened_info = null;
    var inc_1 = 0;
    var inc_2 = 0;

    var initialBounds;
    var aircrafts = [];
    var noFlyZones = [];
    var geoFilterZones = [];
    var logsNodeArray_1 = [];
    var logsNodeArray_2 = [];
    var weatherLoaded = false;
    var initialCenter;
    var weatherIconClicked = false;

    var radiusEarth=6372.7976;
    var lastTimeGetLog = new Date().getTime();
    var lastLogID;
    var timeIntervalGetLog = 3000;
    var getLogInterval;
    var modalAlertShowed = false;
    var stream_key;
    var goHomeCalled = false;

    function initialize() {
        // flight = $('#flight').val();
        var f = document.getElementById('flight');
        flight = f.value;
        var u = document.getElementById('user_id');
        user_id = typeof u !== 'undefined' ? u.value : "1";
        var location = new Array();
        center = new google.maps.LatLng(c_lat, c_long);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center:center,
            tilt: 0,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeId: 'satellite',
            disableDefaultUI: true,
        });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                c_lat = position.coords.latitude;
                c_long = position.coords.longitude;
                center = new google.maps.LatLng(c_lat, c_long);
                if($('#jsNodes #old_node').length == 0){
                    // map.setCenter(new google.maps.LatLng(c_lat, c_long));
                }
                // setHomeLocation(center);
            }, function(error) {
            });
        }else {
            center = new google.maps.LatLng(c_lat, c_long);
            setHomeLocation(center);
        }


        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zoomControlDiv);

        latLngBounds = new google.maps.LatLngBounds();
        $('#jsNodes #old_node').each(function() {
            var _val_1 = $(this).val().split(",");
            markers_1.push(_val_1[1]+','+_val_1[2]);
            object_1.push(_val_1);
            user_id = $(this).attr('attr-user-id');
            if($(this).attr('attr-home') ==　1) {
                is_home = true;
                inc = -1;
            }
        });
        $('#jsNodes #old_node_sub').each(function() {
            var _val_2 = $(this).val().split(",");
            markers_2.push(_val_2[1]+','+_val_2[2]);
            object_2.push(_val_2);
            user_id = $(this).attr('attr-user-id');
            if($(this).attr('attr-home') ==　1) {
                is_home = true;
                inc = -1;
            }
        });

        var is_sub = false;
        if(object_1.length > 0 ) {
            loadMarkers(markers_1, object_1, is_sub);
        }else{
            init = true;
        }
        if(object_2.length > 0 ) {
            is_sub = true;
            loadMarkers(markers_2, object_2, is_sub);
        }else{
            init = true;
        }

        // ajaxLoadNodes();
        getLogInterval = setInterval(function(){
            // if($('.jsLive').hasClass('active'))
            if(object_1.length > 0 ) {
              // console.log("detail.blade.phpにて、MAINのajaxLoadNodesを呼びます。");
              is_sub = false;
              ajaxLoadNodes(object_1, is_sub);
            }
            if(object_2.length > 0 ) {
              // console.log("detail.blade.phpにて、SUBのajaxLoadNodesを呼びます。");
              is_sub = true;
              ajaxLoadNodes(object_2, is_sub);
            }
        }, timeIntervalGetLog);

        google.maps.event.addListener(map, 'click', function (e) {
            var is_subuser = $('#subuser').hasClass('active') ? true : false;
            var max = 20;
            if(!is_home || init) {
                max = 19;
            }
            if (is_subuser) {
              if(inc_2 - 1 == max) return;
            } else {
              if(inc_1 - 1 == max) return;
            }
            if(!is_subuser && inc_1  <= max && $('.jsCreatNode').hasClass('active')) {
              if(glocations.length == 0) {
                  var lat = e.latLng.lat().toFixed(7);
                  var lgn = e.latLng.lng().toFixed(7);
                  var latlng = new google.maps.LatLng(lat,lgn);
                  if(is_home)  {
                      var center = 1;
                  }else{
                      var center = 0;
                  }
                  console.log("F1にまだポイントが打たれていない時 at detail.blade.php");
                  createNode(flight, lat, lgn, latlng, center, inc, is_subuser);
              }else{
                var _ok =  false;
                if(is_home && glocations.length < 21 ) {
                    _ok = true;
                }else if(glocations.length < 20) {
                    _ok = true;
                }
                if(_ok) {
                  var lat = e.latLng.lat().toFixed(7);
                  var lgn = e.latLng.lng().toFixed(7);
                  var latlng = new google.maps.LatLng(lat,lgn);
                  var center = 0;
                  console.log("F1にいくつかポイントが打たれているが、まだいける時 at detail.blade.php");
                  createNode(flight, lat, lgn, latlng, center, inc, is_subuser);

                  var length = glocations.length -1;
                  var endCoords = glocations[length].split(",");
                  var lat_end = endCoords[0];
                  var lgn_end = endCoords[1];

                  drawLine(lat, lgn, lat_end, lgn_end);
                }
              }
              glocations_1.push(lat+','+lgn);
          } else if (is_subuser && inc_2 <= max && $('.jsCreatNode').hasClass('active')) {
            user_id = 2;
            if(glocations_2.length == 0) {
                var lat = e.latLng.lat().toFixed(7);
                var lgn = e.latLng.lng().toFixed(7);
                var latlng = new google.maps.LatLng(lat,lgn);
                if(is_home) {
                    var center = 1;
                }else{
                    var center = 0;
                }
                console.log("GREEN: F1にまだポイントが打たれていない時 at detail.blade.php");
                createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);
            }else{
                var _ok =  false;
                if(is_home && glocations_2.length < 21 ) {
                    _ok = true;
                }else if(glocations_2.length < 20) {
                    _ok = true;
                }
                if(_ok) {
                    var lat = e.latLng.lat().toFixed(7);
                    var lgn = e.latLng.lng().toFixed(7);
                    var latlng = new google.maps.LatLng(lat,lgn);
                    var center = 0;
                    console.log("GREEN: F1にいくつかポイントが打たれているが、まだいける時 at detail.blade.php");
                    createNode(flight, lat, lgn, latlng, center, inc_2, is_subuser, user_id);

                    var length = glocations_2.length -1;
                    var endCoords = glocations_2[length].split(",");
                    var lat_end = endCoords[0];
                    var lgn_end = endCoords[1];

                    drawLine(lat, lgn, lat_end, lgn_end)
                }

            }
            glocations_2.push(lat+','+lgn);
          }
        });

        map.setZoom(18);

        // google.maps.event.addListener(map, 'bounds_changed', scheduleDelayedCallback(map));
        google.maps.event.addListener(map, 'idle', function() {
            if (this.isMapDragging) {
                this.idleSkipped = true;
                return;
            }
            this.idleSkipped = false;
            this._respondToMapChange(map);
        }.bind(this));
        google.maps.event.addListener(map, 'dragstart', function() {
            this.isMapDragging = true;
        }.bind(this));
        google.maps.event.addListener(map, 'dragend', function() {
            this.isMapDragging = false;
            if (this.idleSkipped == true) {
                this.idleSkipped = false;
            }
        }.bind(this));
        google.maps.event.addListener(map, 'bounds_changed', function() {
            this.idleSkipped = false;
        }.bind(this));

        /**
         * Draw exist geo filter zone
         */
        $('#jsNodes #geo_filters').each (function () {
            var flyAreaWrapperPath = [];
            var _val = $(this).val().split("-");
            var arrayPointsOfFilterZone = _val[1].split(";");
            arrayPointsOfFilterZone.forEach(function (value, key){
                coordinate = value.split(',');
                flyAreaWrapperPath.push({lat: parseFloat(coordinate[0]), lng: parseFloat(coordinate[1])});
            });
            var oldFlyAreaWrapper = new google.maps.Polygon({
                paths: flyAreaWrapperPath,
                strokeColor: '#052eff',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#052eff',
                fillOpacity: 0.2
            });
            oldFlyAreaWrapper.setMap(map);
            geoFilterZones.push(oldFlyAreaWrapper);
        });
        setMapClearGeoFilterZone(null);
    }
    // google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script src="{{ URL::to('assets/js/realtime-flight-1.0.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwv_DIo4nEWCHNnqpDm5f5-VGkmxjYLN8&libraries=drawing,geometry&callback=initialize"
    async defer></script>
@endsection
