<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->users();
        $this->roles();
    }

    public function users() {
        DB::table('companies')->insert([
            'name'      => 'Drone Company',
            'address'   => '#242 Street',
            'created_at' => new DateTime,
        ]);
        $company = DB::table('companies')
            ->where('id', 1)
            ->select('id')->first();
        DB::table('users')->insert([
            'name' => 'SAdmin',
            'email' => 'drone@gmail.com',
            'password' => bcrypt('123123'),
            'created_at' => new DateTime,
            'company_id' => $company->id,
        ]);
    }

    public function roles() {
        $owner = DB::table('roles')->insert([
            'name'          => 'owner',
            'display_name'  => 'Project Owner',
            'description'   => 'User is the owner of the given project',
            'created_at' => new DateTime
        ]);
        $admin = DB::table('roles')->insert([
            'name'          => 'admin',
            'display_name'  => 'User Administrator',
            'description'   => 'User is allowed to manage and edit other users',
            'created_at' => new DateTime
        ]);
        $user = DB::table('users')->where('id', 1)->first();
    }
}
