<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = 'points';

    protected $fillable = [
    	'flight_id',
    	'user_id',
    	'lat',
    	'lng',
    	'height',
    	'seq_num',
    	'status',
    	'is_home'
    ];

    public function flights(){
    	return $this->belongsTo('App\Flight', 'flight_id');
    }

    public static function getHomePosition($flight_id, $user_id) {
        return self::where('flight_id', $flight_id)->where('user_id', $user_id)->where('is_home', 1)->first();
    }
}
