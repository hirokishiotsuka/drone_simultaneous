@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container-fluid content">
    <div class="row">
        <div id="content" class="col-sm-12 full">
            <div class="row">
                <div class="resetpassword-box">
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                            <div class="form-group buttonbox">
                                <div class="col-md-6 col-md-offset-3">
                                    <center><a href="{{ url('/admin/login') }}">ログインページヘ</a></center>
                                </div>
                            </div>
                        @else
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/password/email') }}">
                            {{ csrf_field() }}
                            <fieldset class="col-sm-12">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-3 control-label">メールアドレス</label>

                                    <div class="col-md-8">
                                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group buttonbox">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary col-md-12">パスワードリセット
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <div class="metabox">
                        <a href="{{ url('/admin/password/reset') }}">パスワードを忘れた方はコチラ</a>
                        <a href="{{ url('/admin/register') }}">新規登録</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
