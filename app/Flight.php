<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
	protected $table = 'flights';

    protected $fillable = [
    	'name',
    	'user_id',
        'status',
        'plan_status',
        'created_at',
        'updated_at',
    ];

    public function points(){
    	return $this->hasMany('App\Point', 'flight_id')->where('is_home',0);
    }
    public function allPoints(){
        return $this->hasMany('App\Point', 'flight_id');
    }

    public function users(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function logs(){
        return $this->hasMany('App\FlightLog', 'flight_id');
    }

    public function geofilters(){
        return $this->hasMany('App\GeoFilter', 'flight_id');
    }

    public function logs_date(){
        return $this->hasMany('App\FlightLog', 'flight_id')->where('fstarted_at', '2016-08-27 14:19:50');
    }

    public function medias(){
        return $this->hasMany('App\FlightMedia', 'flight_id');
    }

    public static function getFlightByID($id, $user_id) {
        return self::where('id',$id)->where('user_id',$user_id)->first();
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y/m/d H:i');
    }

    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y/m/d H:i');
    }
}
