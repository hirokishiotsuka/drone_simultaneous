<?php 
/**
 * @author Phong Hunterist <publicvnteam@gmail.com>
 */
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use DB;

abstract class AbstractRepository {

    protected $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'), $with = [], $orderBy = 'DESC', $perPage = 0) {
        if ($perPage > 0)
            return $this->make($with)->orderBy('created_at', $orderBy)->paginate($perPage, $columns);    
            return $this->make($with)->orderBy('created_at', $orderBy)->get($columns);
    }

    /**
     * @param  string $value
     * @param  string $key
     * @return array
     */
    public function lists($value, $key = null) {
        $lists = $this->model->lists($value, $key);
        if(is_array($lists)) {
            return $lists;
        }
        return $lists->all();
    }

    public function make($with = [])
    {
        return $this->model->with($with);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 20, $columns = array('*')) {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        // dd($this->model);
        return $this->model->create($data);
    }

    /**
     * Insert more records into table
     * @param  array  $data mixed
     */
    public function createMore(array $data) {
        return DB::table($this->model->getTable())
            ->insert($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id") {
        if (!($model = $this->model->where($attribute, $id)->first())) {
            return false;
        }
        $model->update($data);
        return $model;
    }

    /**
     * @param  array  $data
     * @param  $id
     * @return mixed
     */
    public function updateRich(array $data, $id) {
        if (!($model = $this->model->find($id))) {
            return false;
        }

        $model->fill($data)->save();
        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'), $with = []) {
        return $this->make($with)->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findAllBy($attribute, $value, $columns = array('*'), $perPage = 0) {
        if ($perPage > 0)
            return $this->model->where($attribute, '=', $value)->paginate($perPage, $columns);
        return $this->model->where($attribute, '=', $value)->get($columns);
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $columns
     * @param bool  $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, $columns = ['*'], $or = false, $perPage = 0)
    {
        $model = $this->model;

        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = (! $or)
                    ? $model->where($value)
                    : $model->orWhere($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = (! $or)
                        ? $model->where($field, $operator, $search)
                        : $model->orWhere($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = (! $or)
                        ? $model->where($field, '=', $search)
                        : $model->orWhere($field, '=', $search);
                }
            } else {
                $model = (! $or)
                    ? $model->where($field, '=', $value)
                    : $model->orWhere($field, '=', $value);
            }
        }
        if ($perPage > 0)
            return $model->paginate($perPage, $columns); 
        return $model->get($columns);
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $columns
     * @param bool  $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     *
     * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
     */
    public function search($where, $columns = ['*'], $or = false, $perPage = 0, $with = null, $sort = 'ASC')
    {
        $model = $this->model;
        foreach ($where as $key => $value) {
            if($key == 'user_id' && $value != null){
                $model = $model->Where($key, '=', $value);
            }
            $model  = (! $or)
                    ? $model->where($key, 'LIKE', "%$value%")
                    : $model->orWhere($key, 'LIKE', "%$value%");  
        }
        if($with != null)
            $model->with($with);
        $model->orderBy('updated_at',$sort);
        if ($perPage > 0)
            return $model->paginate($perPage, $columns); 
        return $model->get($columns);
    }
    
    
     /**
     * @param $attribute
     * @param $value
      *@param $with  relationship
     * @param array $columns
     * @return mixed
     */
    public function findAllWith($attribute,$value, $with = [], $group_by=null, $columns = array('*'), $perPage = 0) {
        $model = $this->make($with)->where($attribute, '=', $value);
        if($group_by) {
             $model = $model->groupBy($group_by);
        }
        if ($perPage > 0)
            return $model->paginate($perPage, $columns);
        return $model->get($columns);
    }
}
