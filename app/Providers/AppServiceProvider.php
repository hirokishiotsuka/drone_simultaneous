<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * File Input Validate
         *
         * @author Quynh Nguyen <quynh.nn@neo-lab.vn>
         */
        Validator::extend('validate_files', function($attribute, $value, $parameters, $validator) {
            $mime = $value->getMimeType();
            $mimeArray = [
                'video/quicktime',
                'video/mp4',
                'image/png',
                'image/jpeg',
                'image/jpg',
            ];
            if (in_array($mime, $mimeArray)) {
                return true;
            }
            return false;

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function validateRequired($attribute, $value)
    {
        if (is_null($value)) {
            return false;
        } elseif (is_string($value) && trim($value) === '') {
            return false;
        } elseif ((is_array($value) || $value instanceof Countable) && count($value) < 1) {
            return false;
        } elseif ($value instanceof File) {
            return (string) $value->getPath() != '';
        }
        return true;
    }
}
